/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#include "render.h"

void
loadTextureAndFreePrevious(Texture *texture, void *pixels,
                           unsigned int w, unsigned int h,
                           GLenum color_format,
                           GLenum wrap_mode)
{
    if (texture->id != 0)
        freeTexture(texture);

    texture->width  = w;
    texture->height = h;

    glGenTextures(1, &texture->id);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap_mode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap_mode);

    glTexImage2D(GL_TEXTURE_2D, 0, color_format,
                 texture->width, texture->height,
                 0, color_format, GL_UNSIGNED_BYTE,
                 pixels);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void
freeTexture(Texture *texture)
{
    if (texture->id != 0)
    {
        glDeleteTextures(1, &texture->id);
        texture->id = 0;
    }

    texture->width	= 0;
    texture->height	= 0;
}

int
loadTextureFromFile(Texture *texture, const char *path)
{
    lockGlobalMallocMutex();

	SDL_Surface *surface = IMG_Load(path);

    if (!surface)
    {
        DEBUG_PRINTF("IMG_Load() failure: %s\n", IMG_GetError());
        unlockGlobalMallocMutex();
        return 1;
    }

    if (surface->format->BytesPerPixel != 4)
    {
        SDL_Surface *csurface = SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_RGBA8888, 0);
        SDL_FreeSurface(surface);
        surface = csurface;
    }

    glGenTextures(1, &texture->id);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);

    texture->width  = surface->w;
    texture->height = surface->h;

    SDL_FreeSurface(surface);

    unlockGlobalMallocMutex();

    return 0;
}

int
loadTextureFromText(Texture *texture, const char *text,
    TTF_Font *font, SDL_Color color, unsigned int wrap_width)
{
    if (texture->id != 0)
        freeTexture(texture);

    SDL_Surface *text_surface;

    lockGlobalMallocMutex();
    if (wrap_width > 0)
        text_surface = TTF_RenderText_Blended_Wrapped(font, text, color, (int)wrap_width);
    else
        text_surface = TTF_RenderText_Blended(font, text, color);

    if (!text_surface)
    {
        DEBUG_PRINTF("Text surface no work");
        unlockGlobalMallocMutex();
        return 1;
    }

    glGenTextures(1, &texture->id);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture->id);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, text_surface->w, text_surface->h,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, text_surface->pixels);
    glBindTexture(GL_TEXTURE_2D, 0);

    texture->width  = text_surface->w;
    texture->height = text_surface->h;

    SDL_FreeSurface(text_surface);
    unlockGlobalMallocMutex();

    return 0;
}

int
loadTextureFromTextOnAnyThread(RenderThread *render_thread, Texture *texture,
    const char *text, TTF_Font *font, SDL_Color color, unsigned int wrap_width)
{
    SDL_Surface *text_surface;

    lockGlobalMallocMutex();
    if (wrap_width > 0)
        text_surface = TTF_RenderText_Blended_Wrapped(font, text, color, wrap_width);
    else
        text_surface = TTF_RenderText_Blended(font, text, color);
    unlockGlobalMallocMutex();

    if (!text_surface)
    {
        DEBUG_PRINTF("TTF_RenderText_x() error (text: %s): %s\n", text, TTF_GetError());
        return 1;
    }

    texture->width  = text_surface->w;
    texture->height = text_surface->h;

    RenderThread_enqueueTextureJob(render_thread, texture, text_surface);

    return 0;
}

int
loadTextureFromFileOnAnyThread(RenderThread *render_thread,
    Texture *texture, const char *path)
{
    lockGlobalMallocMutex();
	SDL_Surface *surface = IMG_Load(path);
    unlockGlobalMallocMutex();

    if (!surface)
    {
        DEBUG_PRINTF("IMG_Load() error (path :%s): %s\n", path, IMG_GetError());
        return 1;
    }

    if (surface->format->BytesPerPixel != 4)
    {
        SDL_Surface *csurface = SDL_ConvertSurfaceFormat(surface, SDL_PIXELFORMAT_RGBA8888, 0);
        SDL_FreeSurface(surface);
        surface = csurface;
        DEBUG_PRINTF("Note: loadTextureFromFile() converting surface format for file %s.\n", path);
    }

    texture->width  = surface->w;
    texture->height = surface->h;
    RenderThread_enqueueTextureJob(render_thread, texture, surface);
    return 0;
}

void
Animation_create(Animation *animation, AnimationFrame *frames, uint num_frames)
{
    lockGlobalMallocMutex();
	animation->frames = (AnimationFrame *)malloc(num_frames * sizeof(AnimationFrame));
    unlockGlobalMallocMutex();
	animation->num_frames = num_frames;
	memcpy(animation->frames, frames, num_frames * sizeof(AnimationFrame));

    animation->total_duration = 0;

    for (uint i = 0; i < num_frames; ++i)
    {
        animation->total_duration += animation->frames[i].duration;
        if (animation->frames[i].scale.x == 0.0f)
            animation->frames[i].scale.x = 1.0f;
        if (animation->frames[i].scale.y == 0.0f)
            animation->frames[i].scale.y = 1.0f;
    }
}

void
Animation_free(Animation *animation)
{
    lockGlobalMallocMutex();
	free(animation->frames);
    unlockGlobalMallocMutex();
    animation->num_frames = 0;
}

void
Animation_createFromSheet(Animation *animation, Texture *texture,
    uint frame_width, uint frame_height, float frame_duration,
    uint start_x, uint start_y, uint num_frames, uint sprites_in_row)
{
	#ifdef _DEBUG
	if(sprites_in_row > num_frames)
		DEBUG_PRINTF("SPRITES_IN_ROW > NUM_FRAMES");
	#endif

    AnimationFrame *frames = (AnimationFrame*)malloc(num_frames * sizeof(AnimationFrame));

	uint num_rows = num_frames / sprites_in_row;

    if (num_frames % sprites_in_row > 0) ++num_rows;

    uint row;
	for (uint i = 0; i < num_frames; ++i)
	{
        row = i / sprites_in_row;

		frames[i].texture   = texture;
		frames[i].clip[0]   = start_x + i * frame_width - (row * sprites_in_row % sprites_in_row * frame_width);
		frames[i].clip[1]   = start_y + (i / sprites_in_row) * frame_height;
		frames[i].clip[2]   = frame_width;
		frames[i].clip[3]   = frame_height;
		frames[i].duration  = frame_duration;
		frames[i].flip      = SPRITE_FLIP_NONE;
		frames[i].offset    = {0, 0};
		frames[i].angle     = 0.0f;
		frames[i].scale.x   = 1.0f;
		frames[i].scale.y   = 1.0f;
	}

    Animation_create(animation, frames, num_frames);
    free(frames);
}

void
Animation_createFromSinglePicture(Animation *animation, Texture *texture,
    uint frame_width, uint frame_height, float frame_duration,
    uint start_x, uint start_y, uint num_frames)
{
    AnimationFrame *frames = (AnimationFrame*)malloc(num_frames * sizeof(AnimationFrame));

	for (uint i = 0; i < num_frames; ++i)
	{
		frames[i].texture   = texture;
		frames[i].clip[0]   = 0;
		frames[i].clip[1]   = 0;
		frames[i].clip[2]   = frame_width;
		frames[i].clip[3]   = frame_height;
		frames[i].duration  = frame_duration;
		frames[i].flip      = SPRITE_FLIP_NONE;
		frames[i].offset    = {0, 0};
		frames[i].angle     = 0.0f;
		frames[i].scale.x   = 1.0f;
		frames[i].scale.y   = 1.0f;
	}

    Animation_create(animation, frames, num_frames);
}

void
AnimatedObject_update(AnimatedObject *object, double delta)
{
    if (object->state != ANIMATION_STATE_PLAYING)
        return;

    object->time_passed += (float)delta;

    if (object->time_passed >= object->animation->frames[object->frame].duration * (1.0f / object->speed))
    {
        if (object->frame == object->animation->num_frames-1)
        {
            if (object->loop)
            {
                object->frame = 0;
                object->time_passed = object->time_passed - object->animation->frames[object->frame].duration * (1.0f / object->speed);
            }
        }
        else
        {
            ++object->frame;
            object->time_passed = object->time_passed - object->animation->frames[object->frame].duration * (1.0f / object->speed);
        }
    }
}

void
AnimatedObject_draw(AnimatedObject *object, SpriteBatch *batch, int x, int y)
{
    if (!object->animation || object->state == ANIMATION_STATE_STOPPED)
        return;

    SpriteBatch_drawSprite_FlipScale(batch,
        object->animation->frames[object->frame].texture,
        x + object->animation->frames[object->frame].offset.x,
        y + object->animation->frames[object->frame].offset.y,
        object->animation->frames[object->frame].clip,
        object->animation->frames[object->frame].flip,
        object->animation->frames[object->frame].scale.x,
        object->animation->frames[object->frame].scale.y);
}

void
TextSprite_init(TextSprite *textsprite)
{
    AnimationFrame* frame = (AnimationFrame*)calloc(1,sizeof(AnimationFrame));
    textsprite->animation.frames = frame;
    textsprite->animation.num_frames = 1;
    frame->texture = &textsprite->texture;
    frame->duration = 2;
    textsprite->animation.total_duration = frame->duration;
    frame->flip = SPRITE_FLIP_NONE;
    frame->offset = { 0, 0 };
    frame->angle = 0.0f;
    frame->clip[0] = 0;
    frame->clip[1] = 0;
    frame->scale.x = 1.0f;
    frame->scale.y = 1.0f;
}

void
TextSprite_setText(RenderThread *thread, TextSprite *textsprite, const char* text, TTF_Font *font, SDL_Color color)
{
    loadTextureFromTextOnAnyThread(thread, &textsprite->texture, text, font, color, 0);
    textsprite->animation.frames->clip[2] = textsprite->texture.width;
    textsprite->animation.frames->clip[3] = textsprite->texture.height;
}

void
TextSprite_draw(SpriteBatch *batch, Texture *texture, int x, int y, int widthscale, int heightscale)
{
    SpriteBatch_drawSprite_Scale(batch, texture, x, y, 0, widthscale, heightscale);
}


#define FLIP_ANIMATED_OBJECT()\
    switch (flip)\
    {\
        case SPRITE_FLIP_NONE:\
        {\
            actual_flip = frame->flip;\
        }\
            break;\
        case SPRITE_FLIP_HORIZONTAL:\
        {\
            switch (frame->flip)\
            {\
                case SPRITE_FLIP_NONE:\
                    actual_flip = SPRITE_FLIP_HORIZONTAL;\
                    break;\
                case SPRITE_FLIP_HORIZONTAL:\
                    actual_flip = SPRITE_FLIP_NONE;\
                    break;\
                case SPRITE_FLIP_VERTICAL:\
                    actual_flip = SPRITE_FLIP_BOTH;\
                    break;\
                case SPRITE_FLIP_BOTH:\
                    actual_flip = SPRITE_FLIP_VERTICAL;\
                    break;\
                default:\
                    actual_flip = SPRITE_FLIP_NONE;\
                    break;\
            }\
        }\
            break;\
        case SPRITE_FLIP_VERTICAL:\
        {\
            switch (frame->flip)\
            {\
                case SPRITE_FLIP_NONE:\
                    actual_flip = SPRITE_FLIP_VERTICAL;\
                    break;\
                case SPRITE_FLIP_HORIZONTAL:\
                    actual_flip = SPRITE_FLIP_BOTH;\
                    break;\
                case SPRITE_FLIP_VERTICAL:\
                    actual_flip = SPRITE_FLIP_NONE;\
                    break;\
                case SPRITE_FLIP_BOTH:\
                    actual_flip = SPRITE_FLIP_HORIZONTAL;\
                    break;\
                default:\
                    actual_flip = SPRITE_FLIP_NONE;\
                    break;\
            }\
        }\
            break;\
        case SPRITE_FLIP_BOTH:\
        {\
            switch (frame->flip)\
            {\
                case SPRITE_FLIP_NONE:\
                    actual_flip = SPRITE_FLIP_BOTH;\
                    break;\
                case SPRITE_FLIP_HORIZONTAL:\
                    actual_flip = SPRITE_FLIP_VERTICAL;\
                    break;\
                case SPRITE_FLIP_VERTICAL:\
                    actual_flip = SPRITE_FLIP_HORIZONTAL;\
                    break;\
                case SPRITE_FLIP_BOTH:\
                    actual_flip = SPRITE_FLIP_NONE;\
                    break;\
                default:\
                    actual_flip = SPRITE_FLIP_NONE;\
                    break;\
            }\
        }\
            break;\
        default:\
            actual_flip = SPRITE_FLIP_NONE;\
            break;\
    }


void
AnimatedObject_draw_Flip(AnimatedObject *object, SpriteBatch *batch,
    int x, int y, SpriteFlip flip)
{
    if (!object->animation || object->state == ANIMATION_STATE_STOPPED)
        return;

    AnimationFrame *frame = &object->animation->frames[object->frame];
    SpriteFlip actual_flip;

    FLIP_ANIMATED_OBJECT();

    SpriteBatch_drawSprite_FlipScaleRotate(batch,
        object->animation->frames[object->frame].texture,
        x + object->animation->frames[object->frame].offset.x,
        y + object->animation->frames[object->frame].offset.y,
        object->animation->frames[object->frame].clip,
        actual_flip,
        object->animation->frames[object->frame].scale.x,
        object->animation->frames[object->frame].scale.y,
        object->animation->frames[object->frame].angle);
}

void
AnimatedObject_draw_Scale(AnimatedObject *object, SpriteBatch *batch, int x, int y,
    float scale_x, float scale_y)
{
    if (!object->animation || object->state == ANIMATION_STATE_STOPPED)
        return;

    SpriteBatch_drawSprite_FlipScaleRotate(batch,
        object->animation->frames[object->frame].texture,
        x + object->animation->frames[object->frame].offset.x,
        y + object->animation->frames[object->frame].offset.y,
        object->animation->frames[object->frame].clip,
        object->animation->frames[object->frame].flip,
        scale_x * object->animation->frames[object->frame].scale.x,
        scale_y * object->animation->frames[object->frame].scale.y,
        object->animation->frames[object->frame].angle);
}

void
AnimatedObject_draw_Rotate(AnimatedObject *object, SpriteBatch *batch, int x, int y, float angle)
{

    if (!object->animation || object->state == ANIMATION_STATE_STOPPED)
        return;

    SpriteBatch_drawSprite_FlipScaleRotate(batch,
        object->animation->frames[object->frame].texture,
        x + object->animation->frames[object->frame].offset.x,
        y + object->animation->frames[object->frame].offset.y,
        object->animation->frames[object->frame].clip,
        object->animation->frames[object->frame].flip,
        object->animation->frames[object->frame].scale.x,
        object->animation->frames[object->frame].scale.y,
        object->animation->frames[object->frame].angle + angle);
}

void
AnimatedObject_draw_FlipScale(AnimatedObject *object, SpriteBatch *batch,
    int x, int y, SpriteFlip flip, float scale_x, float scale_y)
{
    if (!object->animation || object->state == ANIMATION_STATE_STOPPED)
        return;

    AnimationFrame *frame = &object->animation->frames[object->frame];
    SpriteFlip actual_flip;

    FLIP_ANIMATED_OBJECT();

    SpriteBatch_drawSprite_FlipScaleRotate(batch,
        object->animation->frames[object->frame].texture,
        x + object->animation->frames[object->frame].offset.x,
        y + object->animation->frames[object->frame].offset.y,
        object->animation->frames[object->frame].clip,
        actual_flip,
        scale_x * object->animation->frames[object->frame].scale.x,
        scale_y * object->animation->frames[object->frame].scale.y,
        object->animation->frames[object->frame].angle);
}

void
AnimatedObject_draw_FlipScaleRotate(AnimatedObject *object, SpriteBatch *batch,
    int x, int y, SpriteFlip flip, float scale_x, float scale_y, float angle)
{
    if (!object->animation || object->state == ANIMATION_STATE_STOPPED)
        return;

    AnimationFrame *frame = &object->animation->frames[object->frame];
    SpriteFlip actual_flip;

    FLIP_ANIMATED_OBJECT();

    SpriteBatch_drawSprite_FlipScaleRotate(batch,
        object->animation->frames[object->frame].texture,
        x + object->animation->frames[object->frame].offset.x,
        y + object->animation->frames[object->frame].offset.y,
        object->animation->frames[object->frame].clip,
        actual_flip,
        scale_x * object->animation->frames[object->frame].scale.x,
        scale_y * object->animation->frames[object->frame].scale.y,
        object->animation->frames[object->frame].angle + angle);
}
void
AnimatedObject_draw_ClipPercentage(AnimatedObject *object, SpriteBatch *batch, int x, int y, float percent_x, float percent_y)
{
    if (!object->animation || object->state == ANIMATION_STATE_STOPPED)
        return;
    uint clip[4];
    clip[0] = object->animation->frames[object->frame].clip[0];
    clip[1] = object->animation->frames[object->frame].clip[1];
    clip[2] = (uint)((float)object->animation->frames[object->frame].clip[2] * percent_x);
    clip[3] = (uint)((float)object->animation->frames[object->frame].clip[3] * percent_y);
    SpriteBatch_drawSprite_FlipScale(batch,
        object->animation->frames[object->frame].texture,
        x + object->animation->frames[object->frame].offset.x,
        y + object->animation->frames[object->frame].offset.y,
        clip,
        object->animation->frames[object->frame].flip,
        object->animation->frames[object->frame].scale.x,
        object->animation->frames[object->frame].scale.y);
}

void
TextSpriteContainer_init(TextSpriteContainer *container)
{
    for (TextSprite *textsprite = &container->textsprites[0]; textsprite < &container->textsprites[MAX_TEXT_EFFECTS]; ++textsprite)
    {
        textsprite->texture.id = 0;
        textsprite->texture.height = 0;
        textsprite->texture.width = 0;
        textsprite->prev = textsprite - 1;
        textsprite->next = textsprite + 1;
        textsprite->numberef = VisualEffectType_create(&textsprite->animation, 0);
        TextSprite_init(textsprite);
    }

    container->textsprites[0].prev = 0;
    container->textsprites[MAX_TEXT_EFFECTS - 1].next = 0;

    container->free_textsprites = &container->textsprites[0];
    container->active_textsprites = 0;
}

TextSprite*
TextSprite_reserve(TextSpriteContainer *container, RenderThread *thread, const char* text, TTF_Font *font, SDL_Color color)
{
    TextSprite *textsprite = container->free_textsprites;

    if (!textsprite)
    {
        return 0;
    }
    //remove from free list
    container->free_textsprites = textsprite->next;

    if (container->free_textsprites) container->free_textsprites->prev = 0;

    // add to active list
    if (!container->active_textsprites)
    {
        container->active_textsprites = textsprite;
        textsprite->prev = 0;
    }
    else
    {
        TextSprite *tail = container->active_textsprites;

        while (tail->next)
            tail = tail->next;

        textsprite->prev = tail;
        tail->next = textsprite;
    }

    textsprite->next = 0;

    TextSprite_setText(thread, textsprite, text, font, color);

    textsprite->duration = textsprite->animation.total_duration * 1.0f;
    textsprite->time_passed = 0;
    return textsprite;
}

void
TextSprite_release(TextSpriteContainer *container, double dt)
{
    TextSprite *e = container->active_textsprites;
    TextSprite *e_next;

    while (e)
    {
        e_next = e->next;

        e->time_passed += (float)dt;

        if (e->time_passed >= e->duration)
        {
            //remove from the active list
            if (e->prev) e->prev->next = e->next;
            else container->active_textsprites = e->next;

            if (e->next) e->next->prev = e->prev;

            //add to free list
            if (!container->free_textsprites)
            {
                container->free_textsprites = e;
                e->next = 0;
                e->prev = 0;
            }
            else
            {
                e->next = container->free_textsprites;
                container->free_textsprites->prev = e;
                e->prev = 0;
                container->free_textsprites = e;
            }
        }

    e = e_next;
    }

}

void
VisualEffectContainer_init(VisualEffectContainer *container)
{
    for (VisualEffect *effect = &container->effects[0];
        effect < &container->effects[MAX_VISUAL_EFFECTS];
        ++effect)
    {
        effect->aobject = AnimatedObject_create(0, 0, 0, 1.0f);
        effect->prev = effect - 1;
        effect->next = effect + 1;
    }

    container->effects[0].prev = 0;
    container->effects[MAX_VISUAL_EFFECTS-1].next = 0;

    container->free_effects = &container->effects[0];

    container->active_effects = 0;
    container->num_queued = 0;

    pthread_mutexattr_t mtx_attr;
    pthread_mutexattr_init(&mtx_attr);
    pthread_mutexattr_settype(&mtx_attr, PTHREAD_MUTEX_RECURSIVE);

    pthread_mutex_init(&container->mutex, &mtx_attr);
    pthread_mutex_init(&container->queue_mutex, &mtx_attr);

    pthread_mutexattr_destroy(&mtx_attr);
}

int
playVisualEffect(VisualEffectContainer *container,
    VisualEffectType *type,
    void* update_args,
    SpriteFlip flip, float rot,
    float scale_x, float scale_y,
    float speed,
    int start_x, int start_y,
    int end_x, int end_y)
{
    pthread_mutex_lock(&container->mutex);

    VisualEffect *effect = container->free_effects;

    if (!effect)
    {
        pthread_mutex_unlock(&container->mutex);
        return 1;
    }

    /* Remove from free list */
    container->free_effects = effect->next;

    if (container->free_effects) container->free_effects->prev = 0;

    /* Add to active list */
    if (!container->active_effects)
    {
        container->active_effects = effect;
        effect->prev = 0;
    }
    else
    {
        VisualEffect *tail = container->active_effects;

        while (tail->next)
            tail = tail->next;

        effect->prev = tail;
        tail->next   = effect;
    }

    effect->next = 0;

    effect->type        = type;
    effect->rot         = rot;
    effect->scale.x     = scale_x;
    effect->scale.y     = scale_y;
    effect->start_pos.x = start_x;
    effect->start_pos.y = start_y;
    effect->end_pos.x   = end_x;
    effect->end_pos.y   = end_y;
    effect->pos.x       = start_x;
    effect->pos.y       = start_y;
    effect->update_args = update_args;
    effect->duration    = type->animation->total_duration * (1.0f / speed);
    effect->time_passed = 0;

    AnimatedObject_playAnimation(&effect->aobject, type->animation, 0, speed);

    pthread_mutex_unlock(&container->mutex);
    return 0;
}

int
queueVisualEffect(VisualEffectContainer *container,
    VisualEffectType *type,
    void* update_args,
    SpriteFlip flip, float rot,
    float scale_x, float scale_y,
    float animation_speed,
    int start_x, int start_y,
    int end_x, int end_y)
{
    pthread_mutex_lock(&container->queue_mutex);

    if (container->num_queued >= MAX_VISUAL_EFFECTS)
    {
        DEBUG_PRINTF("Note: maximum amount (%i) of queued visual effects reached.\n",
            MAX_VISUAL_EFFECTS);
        pthread_mutex_unlock(&container->queue_mutex);
        return 1;
    }

    QueuedVisualEffect *item = &container->queue[container->num_queued];

    item->type          = type;
    item->update_args   = update_args,
    item->flip          = flip;
    item->rot           = rot;
    item->scale.x       = scale_x;
    item->scale.y       = scale_y;
    item->speed         = animation_speed;
    item->start.x       = start_x;
    item->start.y       = start_y;
    item->end.x         = end_x;
    item->end.y         = end_y;

    ++container->num_queued;

    pthread_mutex_unlock(&container->queue_mutex);

    return 0;
}

void
VisualEffectContainer_update(VisualEffectContainer *container, double dt)
{
    if (!container->active_effects)
        return;

    pthread_mutex_lock(&container->mutex);

    VisualEffect *e = container->active_effects;
    VisualEffect *e_next;

    while (e)
    {
        e_next = e->next;

        e->time_passed += (float)dt;
        AnimatedObject_update(&e->aobject, dt);

        IVec2 distance = IVec2_subs(e->end_pos, e->start_pos);

        float time_percentage = e->time_passed / e->duration;
        float x_percentage = time_percentage;
        float y_percentage = time_percentage;

        e->pos.x = e->start_pos.x + (int)(x_percentage * (float)distance.x);
        e->pos.y = e->start_pos.y + (int)(y_percentage * (float)distance.y);

        if (e->type->onUpdate)
            e->type->onUpdate(e, e->update_args);

        if (e->time_passed >= e->duration)
        {
            /* Remove from the active list */
            if (e->prev) e->prev->next = e->next;
            else container->active_effects = e->next;

            if (e->next) e->next->prev = e->prev;

            /* Add to free list */
            if (!container->free_effects)
            {
                container->free_effects = e;
                e->next = 0;
                e->prev = 0;
            }
            else
            {
                e->next = container->free_effects;
                container->free_effects->prev = e;
                e->prev = 0;
                container->free_effects = e;
            }
        }

        e = e_next;
    }

    pthread_mutex_unlock(&container->mutex);

    if (container->num_queued > 0)
    {
        pthread_mutex_lock(&container->queue_mutex);

        for (QueuedVisualEffect *qe = &container->queue[0];
            qe < &container->queue[container->num_queued];
            ++qe)
        {
            playVisualEffect(container, qe->type, qe->update_args,
                qe->flip, qe->rot,
                qe->scale.x, qe->scale.y,
                qe->speed,
                qe->start.x, qe->start.y,
                qe->end.x, qe->end.y);
        }

        container->num_queued = 0;

        pthread_mutex_unlock(&container->queue_mutex);
    }
}

void
VisualEffectContainer_render(VisualEffectContainer *container, SpriteBatch *batch)
{
    if (!container->active_effects)
        return;

    pthread_mutex_lock(&container->mutex);

    for (VisualEffect *e = container->active_effects; e; e = e->next)
    {
        AnimatedObject_draw_FlipScaleRotate(&e->aobject, batch, e->pos.x, e->pos.y,
            e->flip, e->scale.x, e->scale.y, e->rot);
    }

    pthread_mutex_unlock(&container->mutex);
}

/* For freeing SDL surfaces on the main thread */
void
freeSurfaceJob(void *args)
{
    lockGlobalMallocMutex();
    SDL_FreeSurface((SDL_Surface*)args);
    unlockGlobalMallocMutex();
}
