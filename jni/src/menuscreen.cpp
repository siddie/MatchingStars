/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#include "core.h"
#include "defs.h"

#define LOGO_CLIP_DESTINATION_Y 329

static void
MenuScreen_Button1_func(Button *button, Game *game)
{
    game->menu_screen.anim_playing = true;
    
    if (game->menu_screen.ui.state == MENU_STATE_ERROR)
    {
        game->menu_screen.ui.state = MENU_STATE_START;
    }
};

/*
static void
MenuScreen_EndButton_func(Button *button, Game *game)
{
    Game_setScreen(game, SCREEN_END_GAME);
};

static void
MenuScreen_debug_button_func(Button *button, Game *game)
{
    Game_setScreen(game, SCREEN_DEBUG);

};

static void
MenuScreen_HowToButton_func(Button *button, Game*game)
{
    Game_setScreen(game, SCREEN_HOWTO);
};
*/

//static void
//Errori_func(Button *button, Game *game)
//{
//        game->menu_screen.ui.state = MENU_STATE_START;
//}

int
MenuScreen_init(MenuScreen *screen, Game *game)
{
    screen->ui = {screen->ui.state = MENU_STATE_START};
    screen->anim_playing = false;
    screen->is_start_anim_freed = false;
    screen->ui.game = game;

    DropMenu_create(&screen->ui,
        {0, 0, RESOLUTION_WIDTH, RESOLUTION_HEIGHT},
        50, 50, 0, Assets_getTexture(&game->assets, TEX_MATLOCK),
        MENU_STATE_START);
    
    DropMenu_create(&screen->ui,
    { GRID_PIXEL_OFFSET_X, 780, RESOLUTION_WIDTH, RESOLUTION_HEIGHT - 780 },
        50, 150, Assets_getTexture(&game->assets, TEX_GAMEBOARD), Assets_getTexture(&game->assets, TEX_MATLOCK), MENU_STATE_ERROR);

    /*
    Button_create(&screen->ui,{ RESOLUTION_WIDTH - 256, 0, 256, 256},
				  {Assets_getTexture(&game->assets, TEX_END_PLACEHOLDER), {0, 0, 256, 256}},
				  {Assets_getTexture(&game->assets, TEX_END_PLACEHOLDER), {256, 0, 256, 256}},
				  0, MenuScreen_EndButton_func, 0, 0);

    Button_create(&screen->ui,{RESOLUTION_WIDTH / 2 - 256 / 2, RESOLUTION_HEIGHT / 2 + 256 / 2, 256, 256},
                  {Assets_getTexture(&game->assets, TEX_DEBUG_BUTTON), {0, 0, 256, 256}},
                  {Assets_getTexture(&game->assets, TEX_DEBUG_BUTTON), {256, 0, 256, 256}},
                  0, MenuScreen_debug_button_func, 0, 0);

	Button_create(&screen->ui,{RESOLUTION_WIDTH /2 - 464 /2, RESOLUTION_HEIGHT - 101, 464, 101},
				  {Assets_getTexture(&game->assets, TEX_HOWTO), {0, 0, 464, 101}},
				  {Assets_getTexture(&game->assets, TEX_HOWTO), {0, 0, 464, 101}},
				  0, MenuScreen_HowToButton_func, 0, 0);
    */
    Button_create(&screen->ui,{0, 0, RESOLUTION_WIDTH, RESOLUTION_HEIGHT},
                  {Assets_getTexture(&game->assets, TEX_START_SCREEN), {0, 0, RESOLUTION_WIDTH, RESOLUTION_HEIGHT}},
                  {Assets_getTexture(&game->assets, TEX_START_SCREEN), {0, 0, RESOLUTION_WIDTH, RESOLUTION_HEIGHT}},
                  0, MenuScreen_Button1_func, 0, 0);

    DropMenu_addButton(&screen->ui.menus[0], 0, 0, 0, "Tap to start");

    screen->start_animation = AnimatedObject_create(&game->assets.start_screen_animation, false, true, 1.0f);
    
    DropMenu_addButton(&screen->ui.menus[1], 0, 0, 0, "Problem with Load/Save");
    DropMenu_addButton(&screen->ui.menus[1], 0, MenuScreen_Button1_func, Assets_getTexture(&game->assets, TEX_BUTTONSHEET), 0);

#ifdef _DEBUG
    initFpsTextTexture(&game->assets.fpsText, &game->render_thread,
        Assets_getFont(&game->assets, FONT_ELECTROLIZE));
#endif

    screen->logo_clip[0] = 0;
    screen->logo_clip[1] = 0;
    screen->logo_clip[2] = 926;
    screen->logo_clip[3] = 329;

    screen->ao_explosion = AnimatedObject_create(
        &game->assets.antimatter_animation,
        0, 0, 1.5f);


    SDL_Color white = {255, 255, 255, 255};
    TTF_Font *font = Assets_getFont(&game->assets, FONT_ELECTROLIZE);

    loadTextureFromTextOnAnyThread(&game->render_thread,
        &screen->tex_credits[TEX_CREDITS_JESSE],
        "Jesse",
        font, white, 0);

    loadTextureFromTextOnAnyThread(&game->render_thread,
        &screen->tex_credits[TEX_CREDITS_ILKKA],
        "Ilkka",
        font, white, 0);

    loadTextureFromTextOnAnyThread(&game->render_thread,
        &screen->tex_credits[TEX_CREDITS_JANINA],
        "Janina",
        font, white, 0);

    loadTextureFromTextOnAnyThread(&game->render_thread,
        &screen->tex_credits[TEX_CREDITS_LASSE],
        "Lasse",
        font, white, 0);

    loadTextureFromTextOnAnyThread(&game->render_thread,
        &screen->tex_credits[TEX_CREDITS_KUVIS],
        "Kuvis",
        font, white, 0);

    loadTextureFromTextOnAnyThread(&game->render_thread,
        &screen->tex_credits[TEX_CREDITS_LOMMI],
        "Lommi",
        font, white, 0);

    loadTextureFromTextOnAnyThread(&game->render_thread,
        &screen->tex_credits[TEX_CREDITS_PYRY],
        "Weeb",
        font, white, 0);

    loadTextureFromTextOnAnyThread(&game->render_thread,
        &screen->tex_credits[TEX_CREDITS_JAMA],
        "Jama",
        font, white, 0);

    loadTextureFromTextOnAnyThread(&game->render_thread,
        &screen->tex_credits[TEX_CREDITS_KEYO],
        "Keyo",
        font, white, 0);

    return 0;
}

void
MenuScreen_onSetCurrent(MenuScreen *screen, Game *game)
{
    Mix_PlayMusic(*Assets_getMusic(&game->assets, MUS_CONNECTIONLESS_SPACE), -1);

    screen->time_in_screen = 0;
    screen->logo_clip[0] = 0;
    screen->logo_clip[1] = 0;
    screen->logo_clip[2] = 926;
    screen->logo_clip[3] = 329;

    screen->playing_true_music  = 0;
    screen->played_explosion    = 0;
    screen->credits_draw_y      = (float)RESOLUTION_HEIGHT;
}

void
MenuScreen_render(MenuScreen *screen, Game *game, int *viewport)
{
    glViewport(viewport[0], viewport[1], game->window.width, game->window.height);
    glClearColor(0.035f, 0.212f, 0.612f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    UI_draw(&screen->ui, game);

    static int frame_num = 0;
    static double frame_time_counter = 0;
    frame_time_counter += game->render_thread.clock.delta;

    float target_frame_time = game->render_thread.clock.target_fps != 0 ? (1.0f / game->render_thread.clock.target_fps) : 0.032f;

    DEBUG_PRINTF("%f\n", target_frame_time);

    if (frame_time_counter - target_frame_time > 0)
    {
        if (frame_num % (game->render_thread.clock.target_fps > 0 ? game->render_thread.clock.target_fps : 30) == 0)
        {
            ++screen->time_in_screen;
        }

        if (screen->time_in_screen >= TRUE_LOGO_SWAP_TIME)
        {
            if (screen->logo_clip[1] < LOGO_CLIP_DESTINATION_Y)
            {
                screen->logo_clip[1] = MAX(screen->logo_clip[1] + 1, LOGO_CLIP_DESTINATION_Y);
            }
        }

        ++frame_num;
        frame_time_counter = frame_time_counter - target_frame_time;
    }

    /* Draw the logo */
    Texture *tex_logo = Assets_getTexture(&game->assets, TEX_LOGO);
    SpriteBatch_drawSprite(&game->spritebatch,
        tex_logo,
        RESOLUTION_WIDTH / 2 - tex_logo->width / 2,
        200 + (int)(15.0f * sin(frame_num * 0.15 * M_PI / 3.0f)),
        screen->logo_clip);

    if (!AnimatedObject_isFinished(&screen->ao_explosion))
    {
        AnimatedObject_draw_Scale(&screen->ao_explosion, &game->spritebatch,
            RESOLUTION_WIDTH / 2 - (int)((float)game->assets.antimatter_animation.frames[0].clip[2] * 1.50f) / 2,
            175,
            1.50f, 1.50f);
    }

    if (screen->played_explosion)
    {
        float y, scale;

        for (int i = 0; i < 9; ++i)
        {
            y = (int)screen->credits_draw_y + i * 200;
            scale = 1.0f + ((float)y / (float)RESOLUTION_WIDTH);

            SpriteBatch_drawSprite_Scale(&game->spritebatch, &screen->tex_credits[i],
                RESOLUTION_WIDTH / 2 - (int)((float)screen->tex_credits[i].width * scale) / 2, y,
                0, scale, scale);
        }
    }

    AnimatedObject_draw_Scale(&screen->start_animation, &game->spritebatch, 0, 0, 2.0f, 2.0f);

#ifdef _DEBUG
    SpriteBatch_drawSprite_Scale(&game->spritebatch,
        &game->assets.fpsText, 0, 0, 0, 1.0f, 1.0f);
#endif

    SpriteBatch_flush(&game->spritebatch, viewport);

    SDL_GL_SwapWindow(game->window.sdl_window);
}

void
MenuScreen_update(MenuScreen *screen, Game *game)
{
#ifdef _DEBUG
    updateFpsTextTexture(&game->assets.fpsText,
        &game->render_thread,
        game->clock.fps,
        Assets_getFont(&game->assets, FONT_ELECTROLIZE));
#endif
    UI_update(&screen->ui, game);

    if (screen->anim_playing)
    {
        AnimatedObject_update(&screen->start_animation, game->clock.delta);
    }

    if (AnimatedObject_isFinished(&screen->start_animation) && screen->anim_playing)
    {
        screen->anim_playing = false;

        Game_setScreen(game, SCREEN_ACTIVE_GAME);
    }

    if (screen->time_in_screen > TRUE_LOGO_SWAP_TIME)
    {
        if (!screen->playing_true_music)
        {
            if (!game->assets.true_theme_song)
            {
                lockGlobalMallocMutex();
                game->assets.true_theme_song = Mix_LoadMUS("SpaceMatchBeepMix.wav");
                unlockGlobalMallocMutex();
            }

            Mix_PlayMusic(game->assets.true_theme_song, -1);
            screen->playing_true_music = 1;
        }

        if (!screen->played_explosion && screen->logo_clip[1] >= LOGO_CLIP_DESTINATION_Y - 5)
        {
            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets, SO_EXPLOSION_BIG), 0);
            AnimatedObject_replay(&screen->ao_explosion);
            screen->played_explosion = 1;
        }

        if (screen->played_explosion)
        {
            screen->credits_draw_y -= game->clock.delta * 90.0f;
        }

        AnimatedObject_update(&screen->ao_explosion, game->clock.delta);
    }
}


void
MenuScreen_handleEvent(MenuScreen *screen, Game *game, SDL_Event *event)
{
    switch (event->type)
    {
        case SDL_FINGERDOWN:
            break;
        case SDL_FINGERUP:
            break;
        case SDL_KEYDOWN:
        {
            if (event->key.keysym.sym == SDLK_AC_BACK)
                Game_shutDown(game);
        }
        break;
    }
}
