/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#include "gamesession.h"
#include <SDL_mixer.h>

inline bool32
Enemy_isBoss(Enemy *enemy, Game *game)
{
    return enemy->type == &game->game_data.enemy_types[FIRST_BOSS_INDEX]
        || enemy->type == &game->game_data.enemy_types[SECOND_BOSS_INDEX]
        || enemy->type == &game->game_data.enemy_types[THIRD_BOSS_INDEX];
}

inline bool32
Mark_getFlag(Mark *mark, MarkFlag flag)
{
    return (mark->flags & (char)flag) == (char)flag;
}

inline void
Mark_setFlag(Mark *mark, MarkFlag flag, bool32 val)
{
    mark->flags = val ? mark->flags | flag : mark->flags & ~flag;
}

inline void
Mark_draw(Mark *mark, SpriteBatch *batch)
{
    AnimatedObject_draw_Scale(&mark->aobject, batch,
        mark->rect.x, mark->rect.y,
        MARK_PIXEL_SIZE / 79.0f,
        MARK_PIXEL_SIZE / 79.0f);
}

inline void
Mark_setAnimationState(Mark *mark,
    enum MarkAnimationState state,
    Game *game)
{
    mark->animation_state = state;

    if (state == MARK_ANIM_NORMAL)
    {
        AnimatedObject_playAnimation(&mark->aobject,
            &game->assets.an_marks_normal[mark->type], 1, 1.0f);
    }
    else
    {
        if (mark->score <= 3)
        {
            AnimatedObject_playAnimation(&mark->aobject,
                &game->assets.an_mark_combo_small[mark->type], 0, 1.0f);
        }
        else
        {
            AnimatedObject_playAnimation(&mark->aobject,
                &game->assets.an_mark_combo_large[mark->type], 0, 1.0f);
        }
    }
}

inline void
Mark_update(Mark *mark, double dt, Game *game)
{
    AnimatedObject_update(&mark->aobject, dt);

    if (mark->animation_state == MARK_ANIM_COMBO)
    {
        if (AnimatedObject_isFinished(&mark->aobject))
        {
            if (game->active_game_screen.session.state != SESSION_STATE_COMBO_SHOWOFF
            &&  game->active_game_screen.session.state != SESSION_STATE_COMBO_CLEANUP)
                Mark_setAnimationState(mark, MARK_ANIM_NORMAL, game);
            else
                /* Stop the animation to not get stuck on the last frame */
                AnimatedObject_stop(&mark->aobject);
        }
    }
}

float
getAngleFromPoint(int x1, int y1, int x2, int y2)
{
    IVec2 subs = { x1 - x2, y1 - y2 };
    int len = IVec2_len(subs);
    IVec2 zero_point = { 0, len };
    float angle = IVec2_angleBetween(zero_point, subs);
    return angle;
}

void
playMissionSelectionMusic(Game *game)
{
    Mix_PlayMusic(*Assets_getMusic(&game->assets, MUS_CONNECTIONLESS_SPACE), -1);
}

int
MarkContainer_swapMarks(MarkContainer *container,
    int mark1_grid_x, int mark1_grid_y,
    int mark2_grid_x, int mark2_grid_y)
{
    if (mark1_grid_x < 0 || mark1_grid_x >= MARK_GRID_WIDTH
    ||  mark1_grid_y < 0 || mark1_grid_y >= MARK_GRID_HEIGHT
    ||  mark2_grid_x < 0 || mark2_grid_x >= MARK_GRID_WIDTH
    ||  mark2_grid_y < 0 || mark2_grid_y >= MARK_GRID_HEIGHT
    ||  (mark1_grid_x == mark2_grid_x && mark1_grid_y == mark2_grid_y))
        return -1;

    int mark1_grid_index = mark1_grid_y * MARK_GRID_WIDTH + mark1_grid_x;
    int mark2_grid_index = mark2_grid_y * MARK_GRID_WIDTH + mark2_grid_x;

    /* Indices in the mark array */
    int mark1_index = container->grid[mark1_grid_index];
    int mark2_index = container->grid[mark2_grid_index];

    container->grid[mark2_grid_index] = mark1_index;
    container->grid[mark1_grid_index] = mark2_index;

    container->marks[mark1_index].grid_index = mark2_grid_index;
    container->marks[mark2_index].grid_index = mark1_grid_index;

    return mark2_index;
}

void
MarkContainer_updateActiveMarks(MarkContainer *container, Game *game)
{
    double dt = game->clock.delta;

    switch (game->active_game_screen.session.state)
    {
        case SESSION_STATE_COMBO_SHOWOFF:
        {
            if (game->active_game_screen.session.state_timer == COMBO_SHOWOFF_DURATION)
            {
                for (Mark *mark = container->marks;
                    mark < &container->marks[NUM_MARKS];
                    ++mark)
                {
                    if (Mark_getFlag(mark, MARK_FLAG_IS_ACTIVE))
                    {
                        if (Mark_getFlag(mark, MARK_FLAG_IS_PART_OF_COMBO))
                            Mark_setAnimationState(mark, MARK_ANIM_COMBO, game);
                        Mark_update(mark, dt, game);
                    }
                }
            }
            else
            {
                for (Mark *mark = container->marks;
                    mark < &container->marks[NUM_MARKS];
                    ++mark)
                {
                    if (Mark_getFlag(mark, MARK_FLAG_IS_ACTIVE))
                        Mark_update(mark, dt, game);
                }
            }
        }
            break;
        case SESSION_STATE_REFILL_GRID:
        {
            GameSession *session = &game->active_game_screen.session;

            if (session->state_timer < GRID_REFILL_DURATION)
            {
                int destinated_y; /* Where the mark will finally end up at vertical axis wise */
                int distance;
                float percentage = 1.0f - (session->state_timer / GRID_REFILL_DURATION);
                if (percentage > 1.0f) percentage = 1.0f;

                for (int i = 0; i < NUM_MARKS; ++i)
                {
                    if (session->container.marks[i].state == MARK_STATE_FALLING)
                    {
                        destinated_y = GRID_PIXEL_OFFSET_Y + session->container.marks[i].grid_index / MARK_GRID_WIDTH * MARK_PIXEL_SIZE;
                        distance = destinated_y - session->container.marks[i].fall_start_height;
                        session->container.marks[i].rect.y = session->container.marks[i].fall_start_height + (int)(percentage * (float)distance);
                    }

                    if (Mark_getFlag(&session->container.marks[i], MARK_FLAG_IS_ACTIVE))
                    {
                        Mark_update(&session->container.marks[i], dt, game);
                    }
                }
            }
            else
            {
                for (Mark *mark = container->marks;
                    mark < &container->marks[NUM_MARKS];
                    ++mark)
                {
                    if (Mark_getFlag(mark, MARK_FLAG_IS_ACTIVE))
                    {
                        Mark_update(mark, dt, game);
                    }
                }
            }
        }
            break;
        case SESSION_STATE_COMBO_CLEANUP:
        {
            if (game->active_game_screen.session.state_timer == COMBO_CLEANUP_DURATION)
            {
                for (Mark *mark = container->marks;
                    mark < &container->marks[NUM_MARKS];
                    ++mark)
                {
                    if (Mark_getFlag(mark, MARK_FLAG_IS_PART_OF_COMBO))
                    {
                        Mark_setFlag(mark, MARK_FLAG_IS_ACTIVE, 0);
                    }

                    Mark_setFlag(mark, MARK_FLAG_IS_PART_OF_COMBO, 0);
                    Mark_update(mark, dt, game);
                }
            }
            else
            {
                for (Mark *mark = container->marks;
                    mark < &container->marks[NUM_MARKS];
                    ++mark)
                {
                    if (Mark_getFlag(mark, MARK_FLAG_IS_ACTIVE))
                    {
                        Mark_update(mark, dt, game);
                    }
                }
            }
        }
            break;
        default:
        {
            for (Mark *mark = container->marks;
                mark < &container->marks[NUM_MARKS];
                ++mark)
            {
                if (Mark_getFlag(mark, MARK_FLAG_IS_ACTIVE))
                {
                    Mark_update(mark, dt, game);
                }
            }
        }
            break;
    }
}

inline void
Mark_setAnimationByType(Mark *mark, Game *game)
{
    AnimatedObject_playAnimation(&mark->aobject,
        &game->assets.an_marks_normal[mark->type],
        1, 1.0f);
}

inline void
Mark_setFalling(Mark *mark, int start_height)
{
    mark->fall_start_height = start_height;
    mark->state             = MARK_STATE_FALLING;
    Mark_setFlag(mark, MARK_FLAG_WAS_SET_FALLING, 1);
}

inline void
Activate_enemy(Enemy *enemy, int x, int y, GameSession *session, Game *game)
{
    enemy->has_attacked = 0;
	enemy->is_active = 1;
	enemy->x = x;
	enemy->y = y;
	enemy->hp.max = enemy->type->max_health;
    enemy->hp.current = enemy->hp.max;
    enemy->attack_tracker = enemy->type->attack_interval;
    enemy->enemyturn = VisualEffectType_create(&enemy->enemyturnSprite.animation, 0);

    ProgressBar_create(&enemy->hpbar,
        &session->hpbar_bg, 0, 0,
        &session->enemyhpbar_fill, 0, 0,
        &session->hpbar_outline, 0, 0,
        enemy->x, enemy->y + enemy->type->hpbar_offset_y, 0, 0,
        enemy->hp.max, enemy->hp.max);
}

inline void
Activate_target(GameSession *session)
{
	for (int i = 0; i < MAX_NUMBER_OF_ENEMIES; ++i)
	{
		if (session->enemies[i].is_active)
		{
            if (!session->target.is_active)
                session->target.is_active = true;
            session->targeted_enemy = &session->enemies[i];
            session->target.x = session->enemies[i].x;
            session->target.y = session->enemies[i].y;
            break;
		}
	}
}

inline void
Activate_target(Enemy *enemy, GameSession *session)
{
    session->targeted_enemy = enemy;
    session->target.x = enemy->x;
    session->target.y = enemy->y;
}

inline void
Apply_damage(Enemy *enemy, int amount, GameSession *session, Game *game)
{
    if (enemy != 0)
    {
        enemy->hp.current -= amount;
        Sprite_text(session, &session->textsprite_container, game, amount, enemy->x, enemy->y);

        if (amount == (int)(BALLISTIC_BASE_DAMAGE * 8.0f))
        {
            playVisualEffect(&session->effect_container, &session->ef_explosion, session, SPRITE_FLIP_NONE,
                0, 5.0f, 5.0f, 1.0f, enemy->x - 300, enemy->y - 300, enemy->x - 300, enemy->y - 300);
            session->screen_shaking = 1;
            session->shake_frames_left = 10;
            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets, SO_EXPLOSION_BIG), 0);
        }

        if (enemy->hp.current <= 10)
        {
            playVisualEffect(&session->effect_container, &session->ef_explosion, session, SPRITE_FLIP_NONE,
                0, 1.0f, 1.0f, 1.0f, enemy->x, enemy->y, enemy->x, enemy->y);

            session->screen_shaking = 1;
            session->shake_frames_left = 2;

            enemy->is_active = false;
            Activate_target(session);

            int enemiesleft = 0;
            for (int i = 0; i < MAX_NUMBER_OF_ENEMIES; i++)
            {
                if (session->enemies[i].is_active)
                    enemiesleft++;
            }
            if (enemiesleft == 0)
                session->target.is_active = false;
        }
    }
}

inline void
Apply_damage_to_player(Player *player, int amount, GameSession *session, Game *game)
{
    Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
        SO_EXPLOSION_SMALL), 0);

    int rndnumber = (int)(rand() % 150) - 75;
    if (player->hp.current > 0)
    {
        playVisualEffect(&session->effect_container, &session->ef_explosion, session, SPRITE_FLIP_NONE,
            0, 1.0f, 1.0f, 1.0f, session->player.x + 100 + rndnumber, session->player.y - 50 + rndnumber, session->player.x + 100 + rndnumber, session->player.y - 50 + rndnumber);
        //add shield logics to here
       if (session->player.shield.shieldamount > 0)
        {
            queueVisualEffect(&session->effect_container, &session->ef_absorb, 0, SPRITE_FLIP_NONE, 0.0f,
                2.0f, 2.0f, 1.0f, player->x + 50 + rndnumber, player->y + rndnumber, player->x + 50 + rndnumber, player->y - 50 + rndnumber);
            session->player.shield.shieldamount -= 1;
            if (session->player.shield.shieldamount <= 0)
            {
                session->player.baranimation = session->player.healthbari;
                session->player.shield.shieldamount = 0;
                AnimatedObject_replay(&session->player.baranimation);
            }
        }
        else
        {
            Sprite_text(session, &session->textsprite_container, game, amount, player->x, player->y);
            player->hp.current -= amount;
        }
        if (player->hp.current < 1)
        {
            player->hp.current = 0;
            session->screen_shaking = 1;
            session->shake_frames_left = 20;
            playVisualEffect(&session->effect_container, &session->ef_explosion, session, SPRITE_FLIP_NONE,
                0, 5.0f, 5.0f, 1.0f, session->player.x - 200.0f, session->player.y - 500.0f, session->player.x - 200.0f, session->player.y - 500.0f);

        //gameover
        }
    }
}

inline void
Apply_shield(Player *player, int shieldamount)
{
        if (shieldamount <= 3)
        {
            player->shield.shieldamount += 1;
        }
        else if (shieldamount == 4)
        {
            player->shield.shieldamount += 2;
        }
        else if(shieldamount > 4)
            player->shield.shieldamount += 3;

        if (player->shield.shieldamount >= 3)
            player->shield.shieldamount = 3;
}

inline void
Sprite_text(GameSession *session, TextSpriteContainer *container, Game *game, int amount, int xpos, int ypos)
{
    char buffer[64];
    TextSprite *text_sprite = TextSprite_reserve(container, &game->render_thread,
        Convert_IntToString(buffer, 64, amount),
        Assets_getFont(&game->assets, FONT_ELECTROLIZE), { 0,77,255,0 });
    int rndnumber = (int)(rand() % 150) - 75;
    if (text_sprite != NULL)
    {
        playVisualEffect(&session->effect_container, &text_sprite->numberef, 0, SPRITE_FLIP_NONE, 0.0f,
            2.0f, 2.0f, 1.0f, xpos + 50 + rndnumber, ypos + rndnumber, xpos + 50 + rndnumber, ypos - 50 + rndnumber);
    }
}

void
SaveGame(GameSession *session, Game *game)
{
    char *base_path = SDL_GetPrefPath("Rogues", "SpaceRogues");
    char *writeposition = 0;
    if (base_path)
    {
        char buffer[128];
        strncpy(buffer, base_path, 128);
        uint size = strlen(buffer);
        writeposition = &buffer[size];
        strcpy(writeposition, "ebin.bin");
        DEBUG_PRINTF("##PATH FOUND!! %s\n", buffer);
    }
    else
    {
        game->active_game_screen.ui.state = MENU_STATE_ERROR;
        return;
    }
    SDL_RWops *rw = SDL_RWFromFile(writeposition, "w + b");
    if (rw != NULL)
    {
        void *str = (void*)&session->savefiles;
        size_t len = sizeof(SaveFiles);
        if (SDL_RWwrite(rw, str, len, len) != len)
        {
            DEBUG_PRINTF("#################Couldn't fully write string!################# %s\n", SDL_GetError());
        }
        DEBUG_PRINTF("################## %i ###################", session->savefiles.num_missions_unlocked);
        SDL_RWclose(rw);
    }
}
 
void
LoadGame(GameSession *session, Game *game)
{
    session->points_gained_this_battle = 0;
    session->score_across_missions = 0;

    char *base_path = SDL_GetPrefPath("Rogues", "SpaceRogues");
    char *writeposition = 0;
    if (base_path)
    {
        char buffer[128];
        strncpy(buffer, base_path, 128);
        uint size = strlen(buffer);
        writeposition = &buffer[size];
        strcpy(writeposition, "ebin.bin");
        DEBUG_PRINTF("##PATH FOUND!! %s\n", buffer);
    }
    else
    {
        game->menu_screen.ui.state = MENU_STATE_ERROR;
        return;
    }


    SDL_RWops *rw = SDL_RWFromFile(writeposition, "r + b");
    if (rw != NULL)
    {
        SaveFiles files;

        SDL_RWread(rw, &files, sizeof(files), 1);
        DEBUG_PRINTF("#################LOADED!################# %s\n", SDL_GetError());
        SDL_RWclose(rw);

        session->savefiles = files;
        DEBUG_PRINTF("################## %i ###################", session->savefiles.num_missions_unlocked);
    }
    else
    {
        DEBUG_PRINTF("#################notLOADED!################# %s\n", SDL_GetError());
    }
}

inline void
Mark_setRectPositionByGridIndex(Mark *mark)
{
    mark->rect.x = GRID_PIXEL_OFFSET_X + (mark->grid_index % MARK_GRID_WIDTH) * MARK_PIXEL_SIZE;
    mark->rect.y = GRID_PIXEL_OFFSET_Y + (mark->grid_index / MARK_GRID_WIDTH) * MARK_PIXEL_SIZE;
}

inline bool32
isMarkPositionSwapLegal(int grid_x, int grid_y,
    int selected_grid_x, int selected_grid_y)
{
    return grid_x - selected_grid_x >= -1
        && grid_x - selected_grid_x <=  1
        && grid_y - selected_grid_y >= -1
        && grid_y - selected_grid_y <=  1
        && (grid_x == selected_grid_x
            || grid_y == selected_grid_y)
        && (grid_x != selected_grid_x
            || grid_y != selected_grid_y);
}

inline void
Mark_randomizeType(Mark *mark, Game *game)
{
    mark->type = (enum MarkType)(rand() % NUM_MARK_TYPES);
    Mark_setAnimationByType(mark, game);
}

inline void
Mark_randomizeTypeAndForceNoCombos(Mark *mark, GameSession *session, Game *game)
{
    enum MarkType type;

    int mark_x = mark->grid_index % MARK_GRID_WIDTH;
    int mark_y = mark->grid_index / MARK_GRID_WIDTH;

    Mark *top_mark      = MarkContainer_getMarkByGridPosition(&session->container, mark_x, mark_y - 2);
    Mark *left_mark     = MarkContainer_getMarkByGridPosition(&session->container, mark_x - 2, mark_y);
    Mark *right_mark    = MarkContainer_getMarkByGridPosition(&session->container, mark_x + 2, mark_y);
    Mark *bottom_mark   = MarkContainer_getMarkByGridPosition(&session->container, mark_x, mark_y + 2);

    Mark *marks[4];
    int num_marks = 0;

    if (top_mark)       marks[num_marks++] = top_mark;
    if (left_mark)      marks[num_marks++] = left_mark;
    if (right_mark)     marks[num_marks++] = right_mark;
    if (bottom_mark)    marks[num_marks++] = bottom_mark;

    bool32 is_mark_similar_type = 1;

    while (is_mark_similar_type)
    {
        type = (enum MarkType)(rand() % NUM_MARK_TYPES);
        is_mark_similar_type = 0;

        for (Mark **m = marks; m < &marks[num_marks]; ++m)
        {
            if ((*m)->type == type)
            {
                is_mark_similar_type = 1;
                break;
            }
        }
    }

    mark->type = type;
    Mark_setAnimationByType(mark, game);
}

inline Mark *
MarkContainer_getMarkByGridPosition(MarkContainer *container, int x, int y)
{
    if (x >= 0 && x < MARK_GRID_WIDTH
    &&  y >= 0 && y < MARK_GRID_HEIGHT)
        return &container->marks[container->grid[y * MARK_GRID_WIDTH + x]];
    else
        return 0;
}

void
Effect_update(VisualEffect* effect, void* sessionptr)
{
    GameSession* session = (GameSession*)sessionptr;
    if (effect->type == &session->ef_small_laser || effect->type == &session->ef_medium_laser || effect->type == &session->ef_large_laser)
    {
        if ((effect->time_passed > ((effect->aobject.animation->total_duration - 0.51f) * (1.0f / effect->aobject.speed))) && !session->laser_visual_activated)
        {
            session->laser_visual_activated = 1;
            if (effect->type == &session->ef_large_laser)
            {
                for (uint i = 0; i < MAX_ENEMIES_PER_WAVE; ++i)
                {
                    if (session->enemies[i].is_active)
                        queueVisualEffect(&session->effect_container, &session->ef_explosion, 0, SPRITE_FLIP_NONE, 0.0f,
                            2.0f, 2.0f, 1.0f, session->enemies[i].x, session->enemies[i].y, session->enemies[i].x, session->enemies[i].y);

                        Mix_PlayChannel(-1, *Assets_getSoundClip(&session->game->assets,
                            SO_EXPLOSION_BIG), 0);
                }
            }
            else
            {
                queueVisualEffect(&session->effect_container, &session->ef_explosion, 0, SPRITE_FLIP_NONE, 0.0f,
                    2.0f, 2.0f, 1.0f, session->target.x - 50, session->target.y - 50, session->target.x - 50, session->target.y - 50);

                Mix_PlayChannel(-1, *Assets_getSoundClip(&session->game->assets,
                    SO_EXPLOSION_SMALL), 0);
            }
        }
    }
    else if (effect->time_passed > effect->aobject.animation->total_duration * (1.0f / effect->aobject.speed))
    {
        if(effect->type != &session->ef_large_ballistic)
            queueVisualEffect(&session->effect_container, &session->ef_explosion, 0, SPRITE_FLIP_NONE, 0.0f,
                2.0f, 2.0f, 1.0f, effect->pos.x - 50, effect->pos.y - 100, effect->pos.x - 50, effect->pos.y - 100);

        Mix_PlayChannel(-1, *Assets_getSoundClip(&session->game->assets,
            SO_EXPLOSION_SMALL), 0);
    }
}

void
TimedComboAction_execute(TimedComboAction *action, GameSession *session)
{
    for (uint i = 0; i < action->num_targets; ++i)
    {
        if (action->targets[i] != 0)
        {
            if (action->type == MARK_BALLISTIC)
            {
                if (action->power == (int)(BALLISTIC_BASE_DAMAGE))
                playVisualEffect(&session->effect_container, action->effect, session, SPRITE_FLIP_NONE,
                    getAngleFromPoint(session->player.x + 220, session->player.y + 100, action->targets[i]->x, action->targets[i]->y + 50) - 1.5f,
                    action->size, action->size, action->animation_speed,
                    session->player.x + 220, session->player.y + 100, action->targets[i]->x, action->targets[i]->y + 50);
                if (action->power == (int)(BALLISTIC_BASE_DAMAGE * 2.0f))
                    playVisualEffect(&session->effect_container, action->effect, session, SPRITE_FLIP_NONE,
                        getAngleFromPoint(session->player.x + 220, session->player.y + 150, action->targets[i]->x, action->targets[i]->y + 50) - 1.55f,
                        action->size, action->size, action->animation_speed,
                        session->player.x + 220, session->player.y + 150, action->targets[i]->x, action->targets[i]->y + 50);
                if (action->power == (int)(BALLISTIC_BASE_DAMAGE * 8.0f))
                    playVisualEffect(&session->effect_container, action->effect, session, SPRITE_FLIP_NONE,
                        getAngleFromPoint(session->player.x + 300, session->player.y + 150, action->targets[i]->x, action->targets[i]->y + 50) - 1.55f,
                        action->size, action->size, action->animation_speed,
                        session->player.x + 300, session->player.y + 150, action->targets[i]->x, action->targets[i]->y + 50);
            }
            else if (action->type == MARK_ROCKET)
            {
                if (action->num_targets < 4)
                    playVisualEffect(&session->effect_container, action->effect, session, SPRITE_FLIP_NONE,
                        getAngleFromPoint(session->player.x + 220, session->player.y + 100, action->targets[i]->x, action->targets[i]->y + 50) - 1.5f,
                        action->size, action->size, action->animation_speed,
                        session->player.x + 220, session->player.y + 100, action->targets[i]->x, action->targets[i]->y + 50);
                else
                    playVisualEffect(&session->effect_container, action->effect, session, SPRITE_FLIP_NONE,
                        getAngleFromPoint(session->player.x, session->player.y + 100, 700, 350) - 1.5f,
                        action->size, action->size, action->animation_speed,
                        session->player.x, session->player.y + 100, 700, 350);
            }
            else if (action->type == MARK_LASER)
            {
                if (action->power == LASER_BASE_DAMAGE)
                {
                    playVisualEffect(&session->effect_container, action->effect, session, SPRITE_FLIP_NONE,
                        getAngleFromPoint(session->player.x + 220, session->player.y - 100, action->targets[i]->x, action->targets[i]->y) - 2.0f,
                        action->size, action->size, action->animation_speed,
                        session->player.x + 220, session->player.y - 100, session->player.x + 220, session->player.y - 100);
                }
                if (action->power >= (int)(LASER_BASE_DAMAGE * 2.0f))
                {
                    if(action->num_targets == 1)
                        playVisualEffect(&session->effect_container, action->effect, session, SPRITE_FLIP_NONE,
                            getAngleFromPoint(session->player.x + 220, session->player.y - 100, action->targets[i]->x, action->targets[i]->y) -2.0f,
                            action->size, action->size, action->animation_speed,
                            session->player.x + 220, session->player.y - 100, session->player.x + 270, session->player.y - 100);
                    else
                        playVisualEffect(&session->effect_container, action->effect, session, SPRITE_FLIP_NONE,
                            getAngleFromPoint(session->player.x, session->player.y, 700, 350) - 1.5f,
                            action->size, action->size, action->animation_speed,
                            session->player.x + 300, session->player.y - 300, session->player.x + 300, session->player.y - 300);
                }
            }
        }
    }
}

bool32
TimedComboAction_update(TimedComboAction *action, Game *game)
{
    action->time_passed += game->clock.delta;
    if (action->time_passed > action->target_execution_time)
    {
        return 1;
    }
    return 0;
}

void
Player_update(GameSession *session, Game *game)
{
	AnimatedObject_update(&session->player.ao, game->clock.delta);
    char buffer[128];
    TextSprite_setText(&game->render_thread, &session->player.healthsprite,
        Convert_IntToString(buffer, 128, session->player.hp.current),
        Assets_getFont(&game->assets, FONT_ELECTROLIZE), { 255,255,255,0 });
}

void
Enemy_setType(Enemy *enemy, EnemyType *type)
{
    enemy->type         = type;
    enemy->hp.current   = type->max_health;
    enemy->hp.max       = type->max_health;
    enemy->ao           = AnimatedObject_create(type->default_animation, true, true, 1.0f);
}

void
Hpbar_update(GameSession *session, Game *game)
{
    if (session->player.shield.shieldamount > 0)
    {
        session->hpbar_fill = { Assets_getTexture(&game->assets, TEX_PROGRESS_BAR1_PH),{ 0, 981, 1080, 76 } };
        AnimatedObject_update(&session->player.baranimation, game->clock.delta);
    }
    else
    {
        session->hpbar_fill = { Assets_getTexture(&game->assets, TEX_PROGRESS_BAR1_PH),{ 0 , 165 , 1080, 76 } };
        AnimatedObject_update(&session->player.baranimation, game->clock.delta);
    }
}

void
VictoryDefeat_update(GameSession *session, Game *game, AnimatedObject *anim)
{
    if (!session->playing_anim)
    {
        AnimatedObject_replay(anim);
        session->playing_anim = true;
    }
    AnimatedObject_update(anim, game->clock.delta);
}

EnemyType
EnemyType_create(const char *name, int max_health,
    Animation *default_animation, int attack_interval,
    EnemyAttackType attack_type,
    int hpbar_offset_y, int attacks_per_turn)
{
    EnemyType ret;
    ret.name                = name;
    ret.max_health          = max_health;
    ret.default_animation   = default_animation;
    ret.attack_interval     = attack_interval;
    ret.attack_type         = attack_type;
    ret.hpbar_offset_y      = hpbar_offset_y;
    ret.attacks_per_turn    = attacks_per_turn;

    return ret;
}

void
GameSession_unlockLevel(GameSession *session, Level *level, Game *game)
{
    level->locked = 0;
    level->num_missions_unlocked = 1;
}

void
GameSession_beginMission(GameSession *session, MissionType *mission, Game *game)
{
    game->active_game_screen.ui.state = MENU_STATE_NORMAL;
    assert(session->state == SESSION_STATE_MISSION_SELECTION);

	//init player
	session->player.x           = 50;
	session->player.y           = 400;
    session->player.hp.max      = 750;
    session->player.hp.current  = 750;

    /* Basic mission inititalization stuff */
    session->selected_index = -1;
    session->axis_lock = MARK_AXIS_LOCK_TO_NONE;

    memset(session->container.marks, 0, NUM_MARKS * sizeof(Mark));
    for (Mark *mark = session->container.marks;
        mark < &session->container.marks[NUM_MARKS];
        ++mark)
    {
        /* Note: the animated object initialization must happen here,
         * after the memset */
        mark->rect.w = MARK_PIXEL_SIZE;
        mark->rect.h = MARK_PIXEL_SIZE;
        mark->aobject = AnimatedObject_create(
            &game->assets.an_marks_normal[0], 1, 1, 1.0f);
    }

    for (int i = 0; i < MARK_GRID_SIZE; ++i)
    {
        Mark_setFlag(&session->container.marks[i], MARK_FLAG_IS_ACTIVE, 1);
        session->container.marks[i].grid_index  = i;
        session->container.grid[i]              = i;
        /* The randomization of the type must happen after the index of the mark has been set! */
		Mark_randomizeTypeAndForceNoCombos(&session->container.marks[i], session, game);
        Mark_setRectPositionByGridIndex(&session->container.marks[i]);
    }

    session->current_mission = mission;
    session->current_enemy_wave = 0;
    GameSession_spawnEnemyWave(session, game, session->current_enemy_wave);

    session->player.shield.shieldamount = 0;
    /*session->player.baranimation.frame = 9;*/
    session->player.baranimation = session->player.shieldbari;
    AnimatedObject_stop(&session->player.baranimation);
    /* Begin playing the first song of the level */
    if (mission->music_tracks[0].music)
    {
        Mix_PlayMusic(*mission->music_tracks[0].music, mission->music_tracks[0].loop);
        session->playing_music_index = 0;
    }
    else
    {
        session->playing_music_index = -1;
    }

    session->saved_this_battle = 0;
    session->points_gained_this_battle = 0;
    session->state_timer = 0;
    session->state = SESSION_STATE_PLAYER_TURN;
}

void
GameSession_spawnEnemyWave(GameSession *session, Game *game, int wave_index)
{
    Enemy *enemy;
    EnemyWave *wave = &session->current_mission->enemy_waves[wave_index];
    char buffer[128];
    //session->current_mission->enemy_waves->num_enemies = wave->num_enemies;

    //enemies
    for (uint i = 0; i < MAX_NUMBER_OF_ENEMIES; ++i)
    {
        enemy = &session->enemies[i];

        if (i < wave->num_enemies)
        {
            enemy->is_active       = 1;
            enemy->is_moving       = 1;
            enemy->type            = wave->enemies[i];
            enemy->ao              = AnimatedObject_create(enemy->type->default_animation, 1, 1, 1.0f);
            enemy->attack_tracker  = enemy->type->attack_interval;
            enemy->has_attacked    = 0;
            enemy->movement_speed  = 5;

            int y_position;
            if (wave->num_enemies == 1)
                y_position = 250;
            else if (wave->num_enemies == 2)
                y_position = 100 + 300 * i;
            else if (wave->num_enemies == 3)
                y_position = 50 + 200 * i;
            else if (wave->num_enemies == 4)
                y_position = 175 * i;
            else
                y_position = 0;

            Activate_enemy(enemy, RESOLUTION_WIDTH, (y_position), session, game);

            TextSprite_setText(&game->render_thread,
                &session->enemies[i].enemyturnSprite,
                Convert_IntToString(buffer, 128,
                    session->enemies[i].attack_tracker),
                Assets_getFont(&game->assets, FONT_ELECTROLIZE),
                {255, 255, 255, 0});
        }
        else
            enemy->is_active = 0;
    }

	//init enemies
    session->targeted_enemy2    = 0;
    session->targeted_enemy3    = 0;
    session->target_all_enemies = 0;
    Activate_target(session);

    session->player_turn_timer = PLAYER_TURN_DECISION_TIME;
}

void
GameSession_initializeNew(GameSession *session, Game *game)
{
    session->game = game;

    /* Initialize levels */
    for (int i = 0; i < NUM_LEVELS; ++i)
    {
        session->levels[i].type = &game->game_data.level_types[i];
        session->levels[i].locked = 1;
        session->levels[i].num_missions_unlocked = 0;
    }
    session->levels[0].num_missions_unlocked = 1;
    session->levels[0].locked = 0;

    session->laser_visual_activated = 0;

    session->savefiles.num_missions_unlocked = 0;
    LoadGame(session, game);

    for (int i = 0; i < game->active_game_screen.session.savefiles.num_missions_unlocked; i++)
    {
        game->active_game_screen.ui.buttons[i + 2].blocked = 0;
    }

    if (session->savefiles.score != 0)
        session->score_across_missions = session->savefiles.score;
    else
        session->score_across_missions = 0;

    session->current_mission = 0;

    VisualEffectContainer_init(&session->effect_container);
    TextSpriteContainer_init(&session->textsprite_container);
    TextSprite_init(&session->player.healthsprite);
    TextSprite_init(&session->player.absorbtext);

    for (Enemy *enemy = session->enemies; enemy < &session->enemies[MAX_NUMBER_OF_ENEMIES]; ++enemy)
    {
        TextSprite_init(&enemy->enemyturnSprite);
    }

    session->hpbar_outline      = {Assets_getTexture(&game->assets, TEX_PROGRESS_BAR1_PH),
        {0, 0, 0, 0}};
    session->hpbar_bg           = {Assets_getTexture(&game->assets, TEX_PROGRESS_BAR1_PH),
        {0, 54, 1080, 77}};
    session->shieldbar_outline  = {Assets_getTexture(&game->assets, TEX_PROGRESS_BAR1_PH),
        {0, 0, 0, 0}};
    session->shieldbar_bg       = {Assets_getTexture(&game->assets, TEX_PROGRESS_BAR1_PH),
        {0, 0, 0, 0}};
    session->shieldbar_fill     = {Assets_getTexture(&game->assets, TEX_PROGRESS_BAR1_PH),
        {0, 994, 1080, 36}};

    ProgressBar_create(&session->player.hpbar, &session->hpbar_bg, 0, 0, &session->hpbar_fill, 0, 0,
        &session->hpbar_outline, 0, 0, 0, GRID_PIXEL_OFFSET_Y -102, 0, 0,
        session->player.hp.max, session->player.hp.max);

	session->player.ao = AnimatedObject_create(&game->assets.animation1, true, true, 1.0f);
	session->player.laser_angle = 0.0f;

    session->healthbaref = VisualEffectType_create(&session->player.healthsprite.animation, 0);
    session->ef_explosion = VisualEffectType_create(&game->assets.explosion_animation, 0);

    //absorb text
    session->ef_absorb = VisualEffectType_create(&session->player.absorbtext.animation, 0);
    TextSprite_setText(&game->render_thread, &session->player.absorbtext, "Absorb", Assets_getFont(&game->assets, FONT_ELECTROLIZE), { 255,242,80,0 });

    //shield animation
    session->player.shieldbari = AnimatedObject_create(&game->assets.shield_animation, false, true, 1.0f);
    session->player.healthbari = AnimatedObject_create(&game->assets.health_animation, false, true, 1.0f);
    session->player.baranimation = session->player.shieldbari;
    AnimatedObject_stop(&session->player.baranimation);

    //missile
    session->ef_small_rocket = VisualEffectType_create(&game->assets.small_rocket_animation, &Effect_update);
    session->ef_medium_rocket = VisualEffectType_create(&game->assets.medium_rocket_animation, &Effect_update);
    session->ef_large_rocket = VisualEffectType_create(&game->assets.large_rocket_animation, &Effect_update);
    session->ef_antimatter = VisualEffectType_create(&game->assets.antimatter_animation, 0);

    //bullet
    session->ef_small_ballistic = VisualEffectType_create(&game->assets.small_bullet_animation, &Effect_update);
    session->ef_medium_ballistic = VisualEffectType_create(&game->assets.medium_bullet_animation, &Effect_update);
    session->ef_large_ballistic = VisualEffectType_create(&game->assets.large_bullet_animation, &Effect_update);
    session->ef_shockwave = VisualEffectType_create(&game->assets.shockwave_animation, 0);

    //enemy hp bar
    session->enemyhpbar_fill = {Assets_getTexture(&game->assets, TEX_PROGRESS_BAR1_PH), {0, 165, 1080, 76}};

    //enemies
	for(int i = 0; i < MAX_NUMBER_OF_ENEMIES; ++i)
	{
		session->enemies[i].is_active = false;
		session->enemies[i].ao = AnimatedObject_create(&game->assets.animation2, true, true, 1.0f);
        //session->numberef = VisualEffectType_create(&session->enemies[i].enemy1.animation, 0);
	}

	session->target.animatedobject = AnimatedObject_create(&game->assets.target_animation, true, true, 1.0f);
	session->target.is_active = false;

    session->ef_small_laser = VisualEffectType_create(&game->assets.small_laser_animation, &Effect_update);
    session->ef_medium_laser = VisualEffectType_create(&game->assets.medium_laser_animation, &Effect_update);
    session->ef_large_laser = VisualEffectType_create(&game->assets.large_laser_animation, &Effect_update);

    //gameover animation
    session->gameover_ao = AnimatedObject_create(&game->assets.gameover_animation, false, false, 1.0f);
    session->victory_ao = AnimatedObject_create(&game->assets.victory_animation, false, false, 1.0f);

    //session->debug_effect = VisualEffectType_create(&game->assets.rocket_animation, 0);

    session->state = SESSION_STATE_MISSION_SELECTION;
}

uint
GameSession_numEnemiesLeft(GameSession *session)
{
    uint left = 0;

    for (Enemy *enemy = &session->enemies[0];
        enemy < &session->enemies[MAX_NUMBER_OF_ENEMIES];
        ++enemy)
    {
        if (enemy->is_active && enemy->hp.current > 0)
            ++left;
    }

    return left;
}

void
checkAdjacentMarks(GameSession *session, Mark *mark, int *score, MarkDir dir)
{
    int grid_x = mark->grid_index % MARK_GRID_WIDTH;
    int grid_y = mark->grid_index / MARK_GRID_WIDTH;
    int addition = 1;
    Mark *m;

    switch (dir)
    {
        case MARK_DIR_LEFT:
        {
            for (m = MarkContainer_getMarkByGridPosition(&session->container, grid_x + (-(addition++)), grid_y);
                m && m->type == mark->type && !Mark_getFlag(m, MARK_FLAG_CHECKED_LEFT);
                m = MarkContainer_getMarkByGridPosition(&session->container, grid_x + (-(addition++)), grid_y))
            {
                ++mark->h_sequence_count;
                m->h_sequence_count = mark->h_sequence_count;
                if (m->h_sequence_count == 3)
                    *score += 2;
                else if (m->h_sequence_count > 3)
                    *score += 1;

                Mark_setFlag(m, MARK_FLAG_CHECKED_LEFT, 1);
                checkAdjacentMarks(session, m, score, MARK_DIR_LEFT);
                m->score = *score;
            }

            if (mark->h_sequence_count >= 3)
            {
                Mark_setFlag(mark, MARK_FLAG_IS_PART_OF_COMBO, 1);
                if (!Mark_getFlag(mark, MARK_FLAG_CHECKED_UP))
                {
                    Mark_setFlag(mark, MARK_FLAG_CHECKED_UP, 1);
                    checkAdjacentMarks(session, mark, score, MARK_DIR_UP);
                }
                if(!Mark_getFlag(mark, MARK_FLAG_CHECKED_DOWN))
                {
                    Mark_setFlag(mark, MARK_FLAG_CHECKED_DOWN, 1);
                    checkAdjacentMarks(session, mark, score, MARK_DIR_DOWN);
                }
            }

            Mark_setFlag(mark, MARK_FLAG_CHECKED_LEFT, 1);
        }
            break;
        case MARK_DIR_RIGHT:
        {
            for (m = MarkContainer_getMarkByGridPosition(&session->container, grid_x + (addition++), grid_y);
                m && m->type == mark->type && !Mark_getFlag(m, MARK_FLAG_CHECKED_RIGHT);
                m = MarkContainer_getMarkByGridPosition(&session->container, grid_x + (addition++), grid_y))
            {
                ++mark->h_sequence_count;
                m->h_sequence_count = mark->h_sequence_count;
                if (m->h_sequence_count == 3)
                    *score += 2;
                else if (m->h_sequence_count > 3)
                    *score += 1;

                Mark_setFlag(m, MARK_FLAG_CHECKED_RIGHT, 1);
                checkAdjacentMarks(session, m, score, MARK_DIR_RIGHT);
                m->score = *score;
            }

            if (mark->h_sequence_count >= 3)
            {
                Mark_setFlag(mark, MARK_FLAG_IS_PART_OF_COMBO, 1);
                if (!Mark_getFlag(mark, MARK_FLAG_CHECKED_UP))
                {
                    Mark_setFlag(mark, MARK_FLAG_CHECKED_UP, 1);
                    checkAdjacentMarks(session, mark, score, MARK_DIR_UP);
                }
                if (!Mark_getFlag(mark, MARK_FLAG_CHECKED_DOWN))
                {
                    Mark_setFlag(mark, MARK_FLAG_CHECKED_DOWN, 1);
                    checkAdjacentMarks(session, mark, score, MARK_DIR_DOWN);
                }
            }

            Mark_setFlag(mark, MARK_FLAG_CHECKED_RIGHT, 1);
        }
            break;
        case MARK_DIR_UP:
        {
            for (m = MarkContainer_getMarkByGridPosition(&session->container, grid_x, grid_y + (-(addition++)));
                m && m->type == mark->type && !Mark_getFlag(m, MARK_FLAG_CHECKED_UP);
                m = MarkContainer_getMarkByGridPosition(&session->container, grid_x, grid_y + (-(addition++))))
            {
                ++mark->v_sequence_count;
                m->v_sequence_count = mark->v_sequence_count;
                if (m->v_sequence_count == 3)
                    *score += 2;
                else if (m->v_sequence_count > 3)
                    *score += 1;

                Mark_setFlag(m, MARK_FLAG_CHECKED_UP, 1);
                checkAdjacentMarks(session, m, score, MARK_DIR_UP);
                m->score = *score;
            }

            if (mark->v_sequence_count >= 3)
            {
                Mark_setFlag(mark, MARK_FLAG_IS_PART_OF_COMBO, 1);
                if (!Mark_getFlag(mark, MARK_FLAG_CHECKED_LEFT))
                {
                    Mark_setFlag(mark, MARK_FLAG_CHECKED_LEFT, 1);
                    checkAdjacentMarks(session, mark, score, MARK_DIR_LEFT);
                }
                if (!Mark_getFlag(mark, MARK_FLAG_CHECKED_RIGHT))
                {
                    Mark_setFlag(mark, MARK_FLAG_CHECKED_RIGHT, 1);
                    checkAdjacentMarks(session, mark, score, MARK_DIR_RIGHT);
                }
            }

            Mark_setFlag(mark, MARK_FLAG_CHECKED_UP, 1);
        }
            break;
        case MARK_DIR_DOWN:
        {
            for (m = MarkContainer_getMarkByGridPosition(&session->container, grid_x, grid_y + (addition++));
                m && m->type == mark->type && !Mark_getFlag(m, MARK_FLAG_CHECKED_DOWN);
                m = MarkContainer_getMarkByGridPosition(&session->container, grid_x, grid_y + (addition++)))
            {
                ++mark->v_sequence_count;
                m->v_sequence_count = mark->v_sequence_count;
                if (m->v_sequence_count == 3)
                    *score += 2;
                else if (m->v_sequence_count > 3)
                    *score += 1;

                Mark_setFlag(m, MARK_FLAG_CHECKED_DOWN, 1);
                checkAdjacentMarks(session, m, score, MARK_DIR_DOWN);
                m->score = *score;
            }

            if (mark->v_sequence_count >= 3)
            {
                Mark_setFlag(mark, MARK_FLAG_IS_PART_OF_COMBO, 1);

                if (!Mark_getFlag(mark, MARK_FLAG_CHECKED_LEFT))
                {
                    Mark_setFlag(mark, MARK_FLAG_CHECKED_LEFT, 1);
                    checkAdjacentMarks(session, mark, score, MARK_DIR_LEFT);
                }

                if (!Mark_getFlag(mark, MARK_FLAG_CHECKED_RIGHT))
                {
                    Mark_setFlag(mark, MARK_FLAG_CHECKED_RIGHT, 1);
                    checkAdjacentMarks(session, mark, score, MARK_DIR_RIGHT);
                }

                /* Special case for upside-down-T shaped patterns where the left
                 * side of the T is only one mark wide */
                int grid_x = mark->grid_index % MARK_GRID_WIDTH - 1;
                int grid_y = mark->grid_index / MARK_GRID_WIDTH;
                Mark *mark_left = MarkContainer_getMarkByGridPosition(&session->container, grid_x, grid_y);
                if (mark_left
                && mark_left->type == mark->type
                && mark_left->h_sequence_count < mark->h_sequence_count)
                {
                    mark_left->h_sequence_count = mark->h_sequence_count;
                    Mark_setFlag(mark_left, MARK_FLAG_IS_PART_OF_COMBO, 1);
                    *score += 1;
                }
            }

            Mark_setFlag(mark, MARK_FLAG_CHECKED_DOWN, 1);
        }
            break;
        case MARK_DIR_NONE:
        {
            checkAdjacentMarks(session, mark, score, MARK_DIR_LEFT);
            checkAdjacentMarks(session, mark, score, MARK_DIR_RIGHT);
            checkAdjacentMarks(session, mark, score, MARK_DIR_UP);
            checkAdjacentMarks(session, mark, score, MARK_DIR_DOWN);
            Mark_setFlag(mark, MARK_FLAG_CHECKED_LEFT, 1);
            Mark_setFlag(mark, MARK_FLAG_CHECKED_RIGHT, 1);
            Mark_setFlag(mark, MARK_FLAG_CHECKED_UP, 1);
            Mark_setFlag(mark, MARK_FLAG_CHECKED_DOWN, 1);

            mark->score = *score;

            if (*score >= 3)
                Mark_setFlag(mark, MARK_FLAG_IS_PART_OF_COMBO, 1);
        }
            break;
    }
}

void
GameSession_evaluatePoints(GameSession *session)
{
    session->num_combos = 0;

    for (Mark *mark = &session->container.marks[0];
        mark < &session->container.marks[NUM_MARKS];
        ++mark)
    {
        Mark_setFlag(mark, MARK_FLAG_CHECKED_LEFT, 0);
        Mark_setFlag(mark, MARK_FLAG_CHECKED_RIGHT, 0);
        Mark_setFlag(mark, MARK_FLAG_CHECKED_UP, 0);
        Mark_setFlag(mark, MARK_FLAG_CHECKED_DOWN, 0);
        mark->h_sequence_count = 1;
        mark->v_sequence_count = 1;
    }

    Mark *mark;
    int mark_index;
    int score;

    for (int row = 0; row < MARK_GRID_HEIGHT; ++row)
    {
        for (int col = 0; col < MARK_GRID_WIDTH; ++col)
        {
            mark_index  = session->container.grid[row * MARK_GRID_WIDTH + col];
            mark        = &session->container.marks[mark_index];
            score       = 1;

            if (!Mark_getFlag(mark, MARK_FLAG_IS_PART_OF_COMBO))
            {
                checkAdjacentMarks(session, mark, &score, MARK_DIR_NONE);
            }

            if (score >= 3)
            {
                MarkCombo *combo        = &session->combos[session->num_combos++];
                combo->type             = mark->type;
                combo->sequence_len     = score;
            }
        }
    }
}

void
GameSession_render(ActiveGameScreen *screen, GameSession *session, Game *game, int *viewport)
{
    if(!session->screen_shaking)
        glViewport(viewport[0], viewport[1], game->window.width, game->window.height);
    else
    {
        int random = - 10 + rand() % 20;
        glViewport(random, random, game->window.width + random, game->window.height + random);
    }
    //alaosa render
    if (session->state != SESSION_STATE_MISSION_SELECTION)
    {
        glClearColor(0.2, 0.2, 0.2f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        SpriteBatch_drawSprite(&game->spritebatch,
            Assets_getOnDemandTexture(&game->assets,
                session->current_mission->background_index, game),
            0, 0, 0);

        SpriteBatch_drawSprite(&game->spritebatch,
            Assets_getTexture(&game->assets, TEX_GAMEBOARD), 0, 0, 0);
    }
    else
    {
        glClearColor(0.0, 0.0, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
    }

    #define CURRENT_MARK session->container.marks[i]

    switch (session->state)
    {
        case SESSION_STATE_MISSION_SELECTION:
            break;
        case SESSION_STATE_PLAYER_TURN:
        {
            if (session->selected_index != -1)
            {
                for (int i = 0; i < MARK_GRID_WIDTH; ++i)
                {
                    SpriteBatch_drawSprite(&game->spritebatch,
                        Assets_getTexture(&game->assets, TEX_FIRST_SELECTION_INDICATOR),
                        GRID_PIXEL_OFFSET_X + i * MARK_PIXEL_SIZE,
                        GRID_PIXEL_OFFSET_Y + session->first_selected_grid_position.y * MARK_PIXEL_SIZE,
                        0);
                }
                for (int i = 0; i < MARK_GRID_HEIGHT; ++i)
                {
                    if (GRID_PIXEL_OFFSET_Y + i * MARK_PIXEL_SIZE != GRID_PIXEL_OFFSET_Y + session->first_selected_grid_position.y * MARK_PIXEL_SIZE)
                    SpriteBatch_drawSprite(&game->spritebatch,
                        Assets_getTexture(&game->assets, TEX_FIRST_SELECTION_INDICATOR),
                        GRID_PIXEL_OFFSET_X + session->first_selected_grid_position.x * MARK_PIXEL_SIZE,
                        GRID_PIXEL_OFFSET_Y + i * MARK_PIXEL_SIZE,
                        0);
                }
            }

            for (int i = 0; i < NUM_MARKS; ++i)
            {
                if (Mark_getFlag(&CURRENT_MARK, MARK_FLAG_IS_ACTIVE))
                {
                    Mark_draw(&CURRENT_MARK, &game->spritebatch);
                }
            }
        }
            break;
        case SESSION_STATE_COMBO_EVALUATION:
        case SESSION_STATE_COMBO_SHOWOFF:
        case SESSION_STATE_COMBO_CLEANUP:
        case SESSION_STATE_REFILL_GRID:
        case SESSION_STATE_ENEMY_TURN:
        {
            for (int i = 0; i < NUM_MARKS; ++i)
            {
                if (Mark_getFlag(&CURRENT_MARK, MARK_FLAG_IS_ACTIVE))
                {
                    Mark_draw(&CURRENT_MARK, &game->spritebatch);
                }
            }
        }
            break;
        case SESSION_STATE_VICTORY:
        {
            SpriteBatch_drawSprite_Scale(&game->spritebatch,
                &game->active_game_screen.tex_points_from_battle,
                RESOLUTION_WIDTH / 2 - (game->active_game_screen.tex_points_from_battle.width * 2) / 2,
                GRID_PIXEL_OFFSET_Y + (MARK_GRID_HEIGHT / 2 - 1) * MARK_PIXEL_SIZE + 20,
                0, 2.0f, 2.0f);
        }
        case SESSION_GAME_OVER:
        {
            if (session->score_across_missions > 0)
            {
                SpriteBatch_drawSprite_Scale(&game->spritebatch,
                    &game->active_game_screen.tex_total_score_text,
                    RESOLUTION_WIDTH / 2 - (game->active_game_screen.tex_total_score_text.width * 2) / 2,
                    GRID_PIXEL_OFFSET_Y + (MARK_GRID_HEIGHT / 2 - 1) * MARK_PIXEL_SIZE + game->active_game_screen.tex_points_from_battle.height * 2 + 15,
                    0, 2.0f, 2.0f);
            }
        }
            break;
    }

    SpriteBatch_flush(&game->spritebatch, game->window.viewport);

    if (session->state != SESSION_STATE_MISSION_SELECTION)
    {
        //alusrender
        if (session->player.hp.current > 0)
        {
            AnimatedObject_draw_Scale(&session->player.ao,
                &game->spritebatch, session->player.x, session->player.y, 2.0f, 2.0f);
        }

        ProgressBar_draw(&session->player.hpbar, game, 1.0f, 1.0f, 0,
            session->player.hp.max, session->player.hp.current);

        AnimatedObject_draw_ClipPercentage(&session->player.baranimation,
            &game->spritebatch, 0, GRID_PIXEL_OFFSET_Y - 116,
            (float)session->player.hp.current / (float)session->player.hp.max, 1.0f);

        int middle_point = RESOLUTION_WIDTH / 2 - ((session->player.healthsprite.texture.width * 2) / 2);
        TextSprite_draw(&game->spritebatch, &session->player.healthsprite.texture, middle_point, GRID_PIXEL_OFFSET_Y - 90, 2.0f, 2.0f);

        int mid_point = RESOLUTION_WIDTH / 2 - (screen->texar_mark_shield.clip[2] * session->player.shield.shieldamount / 2);

        for (int i = 0; i < session->player.shield.shieldamount; i++)
        {
            SpriteBatch_drawSprite(&game->spritebatch, screen->texar_mark_shield.texture,
                mid_point + (i* screen->texar_mark_shield.clip[2]),
                SHIELD_INDICATOR_Y, screen->texar_mark_shield.clip);
        }

        //vihollisrender
        for (int i = 0; i < MAX_NUMBER_OF_ENEMIES; ++i)
        {
            if (session->enemies[i].is_active)
            {
                AnimatedObject_draw_Scale(&session->enemies[i].ao, &game->spritebatch,
                    session->enemies[i].x, session->enemies[i].y, 2.0f, 2.0f);
                if (!session->enemies[i].is_moving)
                {
                    if (!Enemy_isBoss(&session->enemies[i], game))
                        TextSprite_draw(&game->spritebatch, &session->enemies[i].enemyturnSprite.texture, session->enemies[i].x + 275, session->enemies[i].y + 140, 2.0f, 2.0f);
                    else
                        TextSprite_draw(&game->spritebatch, &session->enemies[i].enemyturnSprite.texture, session->enemies[i].x + 275, session->enemies[i].y + 250, 2.0f, 2.0f);
                }
                ProgressBar_draw(&session->enemies[i].hpbar, game, 0.25f, 0.5f, 0,
                    session->enemies[i].hp.max, session->enemies[i].hp.current);
            }
        }

        //target
        if (session->target.is_active)
            AnimatedObject_draw_Scale(&session->target.animatedobject, &game->spritebatch,
                session->target.x + session->target.animatedobject.animation->frames->texture->width / 2,
                session->target.y + session->target.animatedobject.animation->frames->texture->height / 2,
                2.0f, 2.0f);

        VisualEffectContainer_render(&session->effect_container, &game->spritebatch);

        /* Note: glScissor must adhere to the current window size */
        float x_percentage = (float)game->window.width  / (float)RESOLUTION_WIDTH;
        float y_percentage = (float)game->window.height / (float)RESOLUTION_HEIGHT;

        glScissor(0,
            (int)(y_percentage * (float(RESOLUTION_HEIGHT - GRID_PIXEL_OFFSET_Y))),
            (int)(x_percentage * (float)RESOLUTION_WIDTH),
            (int)(y_percentage * (float)GRID_PIXEL_OFFSET_Y));

        if (session->state == SESSION_STATE_VICTORY)
        {
            AnimatedObject_draw(&session->victory_ao, &game->spritebatch,
                (RESOLUTION_WIDTH - 1020) / 2, (GRID_PIXEL_OFFSET_Y - 240) / 2);
        }
        else if (session->state == SESSION_GAME_OVER)
        {
            AnimatedObject_draw(&session->gameover_ao, &game->spritebatch,
                (RESOLUTION_WIDTH - 960) / 2, (GRID_PIXEL_OFFSET_Y - 440) / 2);
        }

        #ifdef _DEBUG
            SpriteBatch_drawSprite_Scale(&game->spritebatch,
                &game->assets.fpsText, 0, 0, 0, 1.0f, 1.0f);
        #endif

        SpriteBatch_flush(&game->spritebatch, game->window.viewport);

        glScissor(0, 0, (int)(x_percentage * (float)RESOLUTION_WIDTH),
            (int)(y_percentage * (float)RESOLUTION_HEIGHT));
    }
}

void
GameSession_update(GameSession *session, Game *game)
{
    if (game->active_game_screen.game_paused)
    {
        return;
    }

    if (session->state != SESSION_STATE_MISSION_SELECTION
    &&  session->playing_music_index != -1)
    {
        MissionType *mission        = session->current_mission;

        if (!Mix_PlayingMusic())
        {
            if (session->playing_music_index + 1 >= (int)mission->num_music_tracks)
            {
                session->playing_music_index = -1;
            }
            else
            {
                ++session->playing_music_index;
                MissionMusicTrack *track = &mission->music_tracks[session->playing_music_index];
                Mix_PlayMusic(*track->music, track->loop);
            }
        }
    }

    if (session->player.shield.shieldamount > 0)
        session->hpbar_fill = { Assets_getTexture(&game->assets, TEX_PROGRESS_BAR1_PH),
            { 0, 981, 1080, 76 } };
    else
        session->hpbar_fill = { Assets_getTexture(&game->assets, TEX_PROGRESS_BAR1_PH),
            { 0 , 165 , 1080, 76 } };

    if (session->screen_shaking)
    {
        session->shake_frames_left--;
        if (session->shake_frames_left <= 0)
            session->screen_shaking = 0;
    }
    //visual effects
    VisualEffectContainer_update(&session->effect_container, game->clock.delta);
    TextSprite_release(&session->textsprite_container, game->clock.delta);
    MarkContainer_updateActiveMarks(&session->container, game);

    //Animated object updates
    Player_update(session, game);

    //HPbar update
    Hpbar_update(session, game);

    //target
    AnimatedObject_update(&session->target.animatedobject, game->clock.delta);

    char buffer[128];

    //vihollisupdate
    for (int i = 0; i < MAX_NUMBER_OF_ENEMIES; ++i)
    {
        if (session->enemies[i].is_active)
        {
            AnimatedObject_update(&session->enemies[i].ao, game->clock.delta);
            TextSprite_setText(&game->render_thread, &session->enemies[i].enemyturnSprite,
                Convert_IntToString(buffer, 16, session->enemies[i].attack_tracker),
                Assets_getFont(&game->assets, FONT_ELECTROLIZE),
                { 255,255,255,0 });

            if (session->enemies[i].is_moving)
            {
                if (session->enemies[i].x > 750)
                {
                    session->enemies[i].x -= session->enemies[i].movement_speed;
                }
                else
                {
                    session->enemies[i].is_moving = 0;
                    Activate_target(&session->enemies[0], session);
                    session->enemies[i].hpbar.rect.x = session->enemies[i].x;
                    session->enemies[i].hpbar.rect.y = session->enemies[i].y + session->enemies[i].type->hpbar_offset_y;
                }
            }
        }
    }

    switch (session->state)
    {
        case SESSION_STATE_MISSION_SELECTION:
            break;
        case SESSION_STATE_PLAYER_TURN:
        {
            if (session->player_turn_timer > 0)
            {
                session->player_turn_timer -= game->clock.delta;
            }

            if (game->input.fingers[0].down_this_frame
            || (session->state_timer == 0 && game->input.fingers[0].finger_down)) /* Player held a finger down before entering this state */
            {
                //TextSprite_release(&session->textsprite_container);
                for (int i = 0; i < NUM_MARKS; ++i)
                {
                    if (Mark_getFlag(&session->container.marks[i], MARK_FLAG_IS_ACTIVE)
                    &&  IRect_containsPoint(&session->container.marks[i].rect,
                            game->input.fingers[0].position.x,
                            game->input.fingers[0].position.y))
                    {
                        Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets, SO_MARK_PRESS), 0);

                        /* TODO: ADD PIXEL OFFSETS */
                        int x_on_grid = (game->input.fingers[0].position.x - GRID_PIXEL_OFFSET_X) / MARK_PIXEL_SIZE;
                        int y_on_grid = (game->input.fingers[0].position.y - GRID_PIXEL_OFFSET_Y) / MARK_PIXEL_SIZE;
                        int grid_index = y_on_grid * MARK_GRID_WIDTH + x_on_grid;

                        if (grid_index >= 0 && grid_index < MARK_GRID_SIZE)
                        {
                            session->selected_index = session->container.grid[grid_index];

                            /* Remember where we pressed the screen first
                             * so that we can lock the x / y axis correctly */
                            session->first_selected_grid_position.x = x_on_grid;
                            session->first_selected_grid_position.y = y_on_grid;

                            /* Calculate pixel offsets for the touch position */
                            session->selection_offset.x = (game->input.fingers[0].position.x - GRID_PIXEL_OFFSET_X) - x_on_grid * MARK_PIXEL_SIZE;
                            session->selection_offset.y = (game->input.fingers[0].position.y - GRID_PIXEL_OFFSET_Y) - y_on_grid * MARK_PIXEL_SIZE;
                        }
                        break;
                    }
                }
            }
            /* We already have a selected mark being dragged by the player */
            else if (game->input.fingers[0].finger_down
                 && session->selected_index != -1)
            {
                int selected_grid_x = session->container.marks[session->selected_index].grid_index % MARK_GRID_WIDTH;
                int selected_grid_y = session->container.marks[session->selected_index].grid_index / MARK_GRID_WIDTH;

                switch (session->axis_lock)
                {
                    case MARK_AXIS_LOCK_TO_NONE:
                    {
                        int first_selected_x = session->first_selected_grid_position.x;
                        int first_selected_y = session->first_selected_grid_position.y;

                        if (selected_grid_x < first_selected_x || selected_grid_x > first_selected_x)
                        {
                            session->container.marks[session->selected_index].rect.x = CLAMP(
                                (int)game->input.fingers[0].position.x - session->selection_offset.x,
                                MARK_MIN_PIXEL_X, MARK_MAX_PIXEL_X);
                            session->container.marks[session->selected_index].rect.y = CLAMP(
                                GRID_PIXEL_OFFSET_Y + selected_grid_y * MARK_PIXEL_SIZE,
                                MARK_MIN_PIXEL_Y, MARK_MAX_PIXEL_Y);
                        }
                        else if (selected_grid_y < first_selected_y || selected_grid_y > first_selected_y)
                        {
                            session->container.marks[session->selected_index].rect.x = CLAMP(
                                GRID_PIXEL_OFFSET_X + selected_grid_x * MARK_PIXEL_SIZE,
                                MARK_MIN_PIXEL_X, MARK_MAX_PIXEL_X);
                            session->container.marks[session->selected_index].rect.y = CLAMP(
                                 game->input.fingers[0].position.y - session->selection_offset.y,
                                 MARK_MIN_PIXEL_Y, MARK_MAX_PIXEL_Y);
                        }
                        else
                        {
                            /* Because the mark may end up outside of the usual area and become "free floating",
                             * this check is here */
                            IRect first_selection_rect = {
                                GRID_PIXEL_OFFSET_X + session->first_selected_grid_position.x * MARK_PIXEL_SIZE,
                                GRID_PIXEL_OFFSET_Y + session->first_selected_grid_position.y * MARK_PIXEL_SIZE,
                                MARK_PIXEL_SIZE, MARK_PIXEL_SIZE};

                            if (IRect_inRangeOfX(&first_selection_rect, (int)game->input.fingers[0].position.x)
                            ||  IRect_inRangeOfY(&first_selection_rect, (int)game->input.fingers[0].position.y))
                            {
                                session->container.marks[session->selected_index].rect.x = CLAMP(
                                    (int)game->input.fingers[0].position.x - session->selection_offset.x,
                                    MARK_MIN_PIXEL_X, MARK_MAX_PIXEL_X);
                                session->container.marks[session->selected_index].rect.y = CLAMP(
                                     game->input.fingers[0].position.y - session->selection_offset.y,
                                     MARK_MIN_PIXEL_Y, MARK_MAX_PIXEL_Y);
                            }
                            else
                            {
                                Mark_setRectPositionByGridIndex(&session->container.marks[session->selected_index]);
                            }
                        }

                    }
                        break;
                    case MARK_AXIS_LOCK_TO_X:
                    {
                        session->container.marks[session->selected_index].rect.x = CLAMP(
                            (int)game->input.fingers[0].position.x - session->selection_offset.x,
                            MARK_MIN_PIXEL_X, MARK_MAX_PIXEL_X);
                        session->container.marks[session->selected_index].rect.y = CLAMP(
                            GRID_PIXEL_OFFSET_Y + session->first_selected_grid_position.y * MARK_PIXEL_SIZE,
                            MARK_MIN_PIXEL_Y, MARK_MAX_PIXEL_Y);
                    }
                        break;
                    case MARK_AXIS_LOCK_TO_Y:
                    {
                        session->container.marks[session->selected_index].rect.x = CLAMP(
                            GRID_PIXEL_OFFSET_X + session->first_selected_grid_position.x * MARK_PIXEL_SIZE,
                            MARK_MIN_PIXEL_X, MARK_MAX_PIXEL_X);
                        session->container.marks[session->selected_index].rect.y = CLAMP(
                            (int)game->input.fingers[0].position.y - session->selection_offset.y,
                            MARK_MIN_PIXEL_Y, MARK_MAX_PIXEL_Y);
                    }
                        break;
                }

#ifdef _DEBUG
                /* Compiler warnings... */
                volatile int grid_x;
                volatile int grid_y;
#else
                int grid_x;
                int grid_y;
#endif

                static IRect original_rect = {0, 0, MARK_PIXEL_SIZE, MARK_PIXEL_SIZE};

                switch (session->axis_lock)
                {
                    case MARK_AXIS_LOCK_TO_NONE:
                    {
                        for (int i = 0; i < NUM_MARKS; ++i)
                        {
                            if (i != session->selected_index
                            && Mark_getFlag(&session->container.marks[i], MARK_FLAG_IS_ACTIVE))
                            {
                                grid_x = session->container.marks[i].grid_index % MARK_GRID_WIDTH;
                                grid_y = session->container.marks[i].grid_index / MARK_GRID_WIDTH;

                                original_rect.x = GRID_PIXEL_OFFSET_X + grid_x * MARK_PIXEL_SIZE;
                                original_rect.y = GRID_PIXEL_OFFSET_Y + grid_y * MARK_PIXEL_SIZE;

                                if (IRect_containsPoint(&original_rect,
                                    game->input.fingers[0].position.x,
                                    game->input.fingers[0].position.y)
                                    &&
                                    isMarkPositionSwapLegal(grid_x, grid_y,
                                    selected_grid_x, selected_grid_y))
                                {
                                    MarkContainer_swapMarks(&session->container,
                                        selected_grid_x, selected_grid_y, grid_x, grid_y);

                                    if (grid_x == selected_grid_x)
                                        session->axis_lock = MARK_AXIS_LOCK_TO_Y;
                                    else
                                        session->axis_lock = MARK_AXIS_LOCK_TO_X;
                                }
                                else
                                {
                                    Mark_setRectPositionByGridIndex(&session->container.marks[i]);
                                }
                            }
                        }
                    }
                        break;
                    case MARK_AXIS_LOCK_TO_X:
                    {
                        for (int i = 0; i < NUM_MARKS; ++i)
                        {
                            if (i != session->selected_index
                            && Mark_getFlag(&session->container.marks[i], MARK_FLAG_IS_ACTIVE))
                            {
                                grid_x = session->container.marks[i].grid_index % MARK_GRID_WIDTH;
                                grid_y = session->container.marks[i].grid_index / MARK_GRID_WIDTH;

                                original_rect.x = GRID_PIXEL_OFFSET_X + grid_x * MARK_PIXEL_SIZE;
                                original_rect.y = GRID_PIXEL_OFFSET_Y + grid_y * MARK_PIXEL_SIZE;

                                if (grid_y == session->first_selected_grid_position.y
                                && IRect_inRangeOfX(&original_rect, game->input.fingers[0].position.x)
                                && isMarkPositionSwapLegal(grid_x, grid_y, selected_grid_x, selected_grid_y))
                                {
                                    MarkContainer_swapMarks(&session->container,
                                        selected_grid_x, selected_grid_y, grid_x, grid_y);
                                }
                                else
                                {
                                    Mark_setRectPositionByGridIndex(&session->container.marks[i]);
                                }
                            }
                        }
                    }
                        break;
                    case MARK_AXIS_LOCK_TO_Y:
                    {
                        for (int i = 0; i < NUM_MARKS; ++i)
                        {
                            if (i != session->selected_index
                            && Mark_getFlag(&session->container.marks[i], MARK_FLAG_IS_ACTIVE))
                            {
                                grid_x = session->container.marks[i].grid_index % MARK_GRID_WIDTH;
                                grid_y = session->container.marks[i].grid_index / MARK_GRID_WIDTH;

                                original_rect.x = GRID_PIXEL_OFFSET_X + grid_x * MARK_PIXEL_SIZE;
                                original_rect.y = GRID_PIXEL_OFFSET_Y + grid_y * MARK_PIXEL_SIZE;

                                if (grid_x == session->first_selected_grid_position.x
                                && IRect_inRangeOfY(&original_rect, game->input.fingers[0].position.y)
                                && isMarkPositionSwapLegal(grid_x, grid_y, selected_grid_x, selected_grid_y))
                                {
                                    MarkContainer_swapMarks(&session->container,
                                        selected_grid_x, selected_grid_y, grid_x, grid_y);
                                }
                                else
                                {
                                    Mark_setRectPositionByGridIndex(&session->container.marks[i]);
                                }
                            }
                        }
                    }
                }

                /* If we have an axis lock active, check if it needs to be changed */
                if (session->axis_lock != MARK_AXIS_LOCK_TO_NONE
                &&  selected_grid_x == session->first_selected_grid_position.x
                &&  selected_grid_y == session->first_selected_grid_position.y)
                {
                    int finger_x = (int)game->input.fingers[0].position.x;
                    int finger_y = (int)game->input.fingers[0].position.y;

                    /* Test middle */
                    IRect first_selection_rect = {
                        GRID_PIXEL_OFFSET_X + session->first_selected_grid_position.x * MARK_PIXEL_SIZE,
                        GRID_PIXEL_OFFSET_Y + session->first_selected_grid_position.y * MARK_PIXEL_SIZE,
                        MARK_PIXEL_SIZE, MARK_PIXEL_SIZE};

                    if (IRect_containsPoint(&first_selection_rect, finger_x, finger_y))
                    {
                        session->axis_lock = MARK_AXIS_LOCK_TO_NONE;
                        goto rect_test_end;
                    }

                    /* Test left */
                    first_selection_rect.x = GRID_PIXEL_OFFSET_X + (session->first_selected_grid_position.x - 1) * MARK_PIXEL_SIZE;

                    if (IRect_containsPoint(&first_selection_rect, finger_x, finger_y))
                    {
                        session->axis_lock = MARK_AXIS_LOCK_TO_X;
                        goto rect_test_end;
                    }

                    /* Test right */
                    first_selection_rect.x = GRID_PIXEL_OFFSET_X + (session->first_selected_grid_position.x + 1) * MARK_PIXEL_SIZE;

                    if (IRect_containsPoint(&first_selection_rect, finger_x, finger_y))
                    {
                        session->axis_lock = MARK_AXIS_LOCK_TO_X;
                        goto rect_test_end;
                    }

                    /* Test top */
                    first_selection_rect.x = GRID_PIXEL_OFFSET_X + session->first_selected_grid_position.x * MARK_PIXEL_SIZE;
                    first_selection_rect.y = GRID_PIXEL_OFFSET_Y + (session->first_selected_grid_position.y - 1) * MARK_PIXEL_SIZE;

                    if (IRect_containsPoint(&first_selection_rect, finger_x, finger_y))
                    {
                        session->axis_lock = MARK_AXIS_LOCK_TO_Y;
                        goto rect_test_end;
                    }

                    /* Test bottom */
                    first_selection_rect.y = GRID_PIXEL_OFFSET_Y + (session->first_selected_grid_position.y + 1) * MARK_PIXEL_SIZE;

                    if (IRect_containsPoint(&first_selection_rect, finger_x, finger_y))
                    {
                        session->axis_lock = MARK_AXIS_LOCK_TO_Y;
                        goto rect_test_end;
                    }

                    rect_test_end:;
                }
            }

            /* Player released a mark */
            else if (session->selected_index != -1)
            {
                int first_selected_index = session->container.grid[session->first_selected_grid_position.y * MARK_GRID_WIDTH + session->first_selected_grid_position.x];

                if (first_selected_index != session->selected_index)
                {
                    /* Enter the evaluation state */
                    session->state = SESSION_STATE_COMBO_EVALUATION;
                    session->combo_eval_iterations = 0;
                }

                session->selected_index = -1;
                session->axis_lock = MARK_AXIS_LOCK_TO_NONE;
            }
            else
            {
				IRect enemyrect;
				for (int i = 0; i < MAX_NUMBER_OF_ENEMIES; ++i) //change target manually
				{
					enemyrect = { session->enemies[i].x, session->enemies[i].y, 98 * 2, 98 * 2};
					if (session->target.is_active && session->enemies[i].is_active
						&&  IRect_containsPoint(&enemyrect,
							game->input.fingers[0].position.x,
							game->input.fingers[0].position.y))
						Activate_target(&session->enemies[i], session);
				}
                for (int i = 0; i < NUM_MARKS; ++i)
                {
                    if (Mark_getFlag(&session->container.marks[i], MARK_FLAG_IS_ACTIVE))
                    {
                        Mark_setRectPositionByGridIndex(&session->container.marks[i]);
                    }
                }
            }

            session->state_timer += (float)game->clock.delta;
        }
            break;
        /* Evaluate points and fill up the TimedComboAction array */
        case SESSION_STATE_COMBO_EVALUATION:
        {
            GameSession_evaluatePoints(session);

            /* Tag marks as members of a combo if necessary */
            TimedComboAction *action;
            session->num_timed_combo_actions = 0;

            if (session->num_combos >= 1)
                Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets, SO_COMBO), 0);

            /* Fill up our TimedComboAction array according to combos found */
            for (MarkCombo *combo = session->combos;
                combo < &session->combos[session->num_combos];
                ++combo)
            {
                action                  = &session->timed_combo_actions[session->num_timed_combo_actions++];
                action->type            = combo->type;
                action->time_passed     = 0;
                action->executed        = 0;
                action->damage_applied  = 0;
                /* Setting all types to the same duration for now - migh change to a switch case later! */
                action->target_idle_time        = TIMED_COMBO_ACTION_IDLE_TIME;
                action->target_execution_time   = BALLISTIC_DAMAGE_DELAY;
                for (uint i = 0; i < action->num_targets; ++i)
                {
                    action->targets[i] = 0;
                }

                uint num_enemies_left = GameSession_numEnemiesLeft(session);

                switch (action->type)
                {
                    case MARK_BALLISTIC:
                    {
                        action->animation_speed = 0.3f;
                        if (combo->sequence_len == 3)
                        {
                            action->effect = &session->ef_small_ballistic;
                            action->power           = BALLISTIC_BASE_DAMAGE;
                            action->num_targets     = 4;
                            action->size            = 2.0f;
                            for (uint i = 0; i < MAX_NUMBER_OF_ENEMIES; ++i)
                            {
                                if(&session->enemies[i] != 0 && session->enemies[i].is_active)
                                action->targets[i] = &session->enemies[i];
                            }
                        }
                        else if (combo->sequence_len == 4)
                        {
                            action->effect = &session->ef_medium_ballistic;
                            action->power = (int)(BALLISTIC_BASE_DAMAGE * 2.0f);
                            action->size = 2.0f;

                            if (num_enemies_left >= 2)
                            {
                                action->num_targets = 2;
                                int random_target = rand() % MAX_NUMBER_OF_ENEMIES;
                                int targeted_index = (int)(session->targeted_enemy - &session->enemies[0]);
                                while (random_target == targeted_index || !session->enemies[random_target].is_active)
                                {
                                    random_target = rand() % MAX_NUMBER_OF_ENEMIES;
                                }
                                action->targets[1] = &session->enemies[random_target];

                                if (num_enemies_left >= 3)
                                {
                                    action->num_targets = 3;
                                    int random_target2 = rand() % MAX_NUMBER_OF_ENEMIES;
                                    while (random_target2 == targeted_index || !session->enemies[random_target].is_active
                                        || random_target2 == random_target || !session->enemies[random_target2].is_active)
                                    {
                                        random_target2 = rand() % MAX_NUMBER_OF_ENEMIES;
                                    }
                                    action->targets[2] = &session->enemies[random_target2];
                                }
                            }
                            else
                            {
                                action->num_targets = 1;
                            }
                            action->targets[0] = session->targeted_enemy;
                        }

                        else if (combo->sequence_len >= 5)
                        {
                            playVisualEffect(&session->effect_container, &session->ef_shockwave, session, SPRITE_FLIP_NONE,
                                0, 1.0f, 1.0f, 10.0f, session->player.x + 350, session->player.y - 100, session->player.x + 350, session->player.y - 100);
                            action->effect      = &session->ef_large_ballistic;
                            action->power       = (int)(BALLISTIC_BASE_DAMAGE * 8.0f);
                            action->num_targets = 1;
                            action->size        = 2.0f;
                            action->targets[0]  = session->targeted_enemy;
                            action->target_idle_time += 0.2f;
                            action->target_execution_time += 0.2f;
                        }
                    }
                        break;
                    case MARK_ROCKET:
                    {
                        action->target_execution_time = BALLISTIC_DAMAGE_DELAY;
                        action->animation_speed = 1.0f;
                        if (combo->sequence_len == 3)
                        {
                            action->effect = &session->ef_small_rocket;
                            action->power = ROCKET_BASE_DAMAGE;
                            action->num_targets = 1;
                            action->size = 1.25f;
                            action->targets[0] = session->targeted_enemy;
                        }
                        else if (combo->sequence_len == 4)
                        {
                            action->effect = &session->ef_medium_rocket;
                            action->power = (int)(ROCKET_BASE_DAMAGE * 1.75f);
                            action->size = 1.5f;

                            if (num_enemies_left >= 2)
                            {
                                action->num_targets = 2;
                                int random_target = rand() % MAX_NUMBER_OF_ENEMIES;
                                int targeted_index = (int)(session->targeted_enemy - &session->enemies[0]);
                                while (random_target == targeted_index || !session->enemies[random_target].is_active)
                                {
                                    random_target = rand() % MAX_NUMBER_OF_ENEMIES;
                                }
                                action->targets[1] = &session->enemies[random_target];
                            }
                            else
                            {
                                action->num_targets = 1;
                            }
                            action->targets[0] = session->targeted_enemy;
                        }
                        else if (combo->sequence_len >= 5)
                        {
                            action->effect = &session->ef_large_rocket;
                            action->power = (int)(ROCKET_BASE_DAMAGE * 1.5f);
                            action->num_targets = 4;
                            action->size = 3.0f;
                            action->targets[0] = session->targeted_enemy;
                        }
                    }
                        break;
                    case MARK_LASER:
                    {
                        session->laser_visual_activated = 0;
                        action->target_execution_time = BALLISTIC_DAMAGE_DELAY;
                        action->animation_speed = 2.0f;

                        if (combo->sequence_len == 3)
                        {
                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                SO_LASER_SMALL), 0);
                            action->effect = &session->ef_small_laser;
                            action->power = LASER_BASE_DAMAGE;
                            action->num_targets = 1;
                            action->size = 2.0f;
                            action->targets[0] = session->targeted_enemy;
                        }
                        else if (combo->sequence_len == 4)
                        {
                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                SO_LASER_MEDIUM), 0);
                            action->effect = &session->ef_medium_laser;
                            action->power = (int)(LASER_BASE_DAMAGE * 2.0f);
                            action->num_targets = 1;
                            action->size = 2.0f;
                            action->targets[0] = session->targeted_enemy;
                        }
                        else if (combo->sequence_len >= 5)
                        {
                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                SO_LASER_BIG), 0);
                            action->effect = &session->ef_large_laser;
                            action->power = (int)(LASER_BASE_DAMAGE * 2.0f);
                            action->num_targets = 4;
                            action->size = 3.0f;
                            for (uint i = 0; i < MAX_NUMBER_OF_ENEMIES; ++i)
                            {
                                if (&session->enemies[i] != 0 && session->enemies[i].is_active)
                                    action->targets[i] = &session->enemies[i];
                            }
                        }
                    }
                        break;
                    case MARK_REPAIR:
                    {
                        if (combo->sequence_len == 3)
                        {
                            session->player.hp.current += 150;
                        }
                        else if (combo->sequence_len == 4)
                        {
                            session->player.hp.current += 350;
                        }
                        else if (combo->sequence_len >= 5)
                        {
                            session->player.hp.current = session->player.hp.max;
                        }

                        Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                            SO_HEAL1), 0);

                        if (session->player.hp.current > session->player.hp.max)
                            session->player.hp.current = session->player.hp.max;
                    }
                        break;
                    case MARK_SHIELD:
                    {
                        if (session->player.shield.shieldamount == 0)
                        {
                            session->player.baranimation = session->player.shieldbari;
                            AnimatedObject_replay(&session->player.baranimation);
                        }
                        Apply_shield(&session->player, combo->sequence_len);

                        Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                            SO_SHIELD1), 0);
                    }
                        break;
                }
            }

            //session->timed_combo_actions[0].target_execution_time = 0.0f;

            /* remember how many evaluations we've done this round */
            ++session->combo_eval_iterations;

            if (session->combo_eval_iterations == 1 || session->num_combos > 0)
            {
                /* Skip showoff if there are no combos */
                session->state          = SESSION_STATE_COMBO_SHOWOFF;
                session->state_timer    = COMBO_SHOWOFF_DURATION;
            }
            else
            {
                /* If this isn't the first evaluation iteration, check if combos exist.
                 * If not, jump ahead over showoff - else, show off the new combos, too */
                session->state_timer = ENEMY_TURN_DURATION;
                session->state       = SESSION_STATE_ENEMY_TURN;
            }
        }
            break;
        case SESSION_STATE_COMBO_SHOWOFF:
        {
            session->state_timer -= game->clock.delta;

            bool32 can_leave_state = 1;
            bool32 update_next_action = 1;

            /* Update timed combo actions */
            for (TimedComboAction *action = &session->timed_combo_actions[0];
                action < &session->timed_combo_actions[session->num_timed_combo_actions];
                ++action)
            {
                if (update_next_action)
                {
                    action->time_passed += (float)game->clock.delta;

                    if (action->time_passed < action->target_idle_time)
                    {
                        update_next_action = 0;
                    }

                    if (!action->executed)
                    {
                        TimedComboAction_execute(action, session);
                        action->executed = 1;
                        session->state_timer += 0.2f;
                    }

                    if (action->time_passed >= action->target_execution_time && !action->damage_applied)
                    {
                        if (action->num_targets > 0)
                        {
                            for (uint i = 0; i < action->num_targets; ++i)
                            {
                                if (action->targets[i] != 0)
                                {
                                    Apply_damage(action->targets[i], action->power, session, game);
                                }
                                if (action->type == MARK_ROCKET && action->num_targets == 4)
                                {
                                    for (uint i = 0; i < MAX_NUMBER_OF_ENEMIES; ++i)
                                    {
                                        if (&session->enemies[i] != 0 && session->enemies[i].is_active)
                                            action->targets[i] = &session->enemies[i];
                                    }
                                    if (action->targets[i] != 0)
                                        Apply_damage(action->targets[i], action->power, session, game);

                                    playVisualEffect(&session->effect_container, &session->ef_antimatter, session, SPRITE_FLIP_NONE,
                                        0, 3.0f, 3.0f, 2.0f, 200, -300, 200, -300);

                                        Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                            SO_EXPLOSION_BIG), 0);

                                    session->screen_shaking = 1;
                                    session->shake_frames_left = 15;
                                }
                                action->damage_applied = 1;
                            }
                        }
                        else
                            action->damage_applied = 1;
                    }
                    if (!action->damage_applied)
                    {
                        can_leave_state = 0;
                    }
                }
            }

            if (session->state_timer <= 0 && can_leave_state)
            {
                session->state          = SESSION_STATE_COMBO_CLEANUP;
                session->state_timer    = COMBO_CLEANUP_DURATION;
            }
            else
            {
                for (int i = 0; i < NUM_MARKS; ++i)
                {
                    if (Mark_getFlag(&session->container.marks[i], MARK_FLAG_IS_ACTIVE))
                    {
                        Mark_setRectPositionByGridIndex(&session->container.marks[i]);
                    }
                }

                // Skip this phase if there are no combos 
                if (session->num_combos <= 0)
                {
                    session->state = SESSION_STATE_COMBO_CLEANUP;
                    session->state_timer = COMBO_CLEANUP_DURATION;
                }
            }
        }
            break;
        case SESSION_STATE_COMBO_CLEANUP:
        {
            session->state_timer -= game->clock.delta;

            if (session->state_timer <= 0 || session->num_combos <= 0)
            {
                session->state          = SESSION_STATE_REFILL_GRID;
                session->state_timer    = GRID_REFILL_DURATION;
            }
        }
            break;
        case SESSION_STATE_REFILL_GRID:
        {
            /* Set the correct marks in motion to refill the grid */
            if (session->state_timer == GRID_REFILL_DURATION)
            {
                Mark *mark;
                Mark *mark_below;

                for (int col = 0; col < MARK_GRID_WIDTH; ++col)
                {
                    for (int row = MARK_GRID_HEIGHT - 2; row >= 0; --row)
                    {
                        mark = &session->container.marks[session->container.grid[row * MARK_GRID_WIDTH + col]];

                        for (int i = row; i < MARK_GRID_HEIGHT - 1; ++i)
                        {
                            mark_below = &session->container.marks[session->container.grid[(i + 1) * MARK_GRID_WIDTH + col]];

                            /* TODO: check this thing again t: min� */
                            if (Mark_getFlag(mark_below, MARK_FLAG_IS_ACTIVE))
                            {
                                break;
                            }
                            else
                            {
                                MarkContainer_swapMarks(&session->container, col, i, col, i + 1);

                                if (!Mark_getFlag(mark, MARK_FLAG_WAS_SET_FALLING))
                                    Mark_setFalling(mark, mark->rect.y);
                            }
                        }
                    }
                }

                /* Fill in the dead marks - this must happen after setting the proper marks
                 * in motion for randomization to work properly! */
                if (GameSession_numEnemiesLeft(session) > 0)
                {
                    Mark *mark;
                    for (int row = 0; row < MARK_GRID_HEIGHT; ++row)
                    {
                        for (int col = 0; col < MARK_GRID_WIDTH; ++col)
                        {
                            mark = &session->container.marks[session->container.grid[row * MARK_GRID_WIDTH + col]];

                            if (!Mark_getFlag(mark, MARK_FLAG_IS_ACTIVE))
                            {
                                Mark_randomizeType(mark, game);
                                mark->rect.x    = GRID_PIXEL_OFFSET_X + col * MARK_PIXEL_SIZE;
                                mark->rect.y    = -MARK_PIXEL_SIZE;
                                Mark_setFalling(mark, mark->rect.y);
                                Mark_setFlag(mark, MARK_FLAG_IS_ACTIVE, 1);
                            }
                        }
                    }
                }
                else
                {
                    Mark *mark;
                    for (int row = 0; row < MARK_GRID_HEIGHT; ++row)
                    {
                        for (int col = 0; col < MARK_GRID_WIDTH; ++col)
                        {
                            mark = &session->container.marks[session->container.grid[row * MARK_GRID_WIDTH + col]];

                            if (!Mark_getFlag(mark, MARK_FLAG_IS_ACTIVE))
                            {
                                Mark_randomizeTypeAndForceNoCombos(mark, session, game);
                                mark->rect.x    = GRID_PIXEL_OFFSET_X + col * MARK_PIXEL_SIZE;
                                mark->rect.y    = -MARK_PIXEL_SIZE;
                                Mark_setFalling(mark, mark->rect.y);
                                Mark_setFlag(mark, MARK_FLAG_IS_ACTIVE, 1);
                            }
                        }
                    }
                }
            }

            /* Used to update mark rect y's in an else clause here,
             * but moved it to MarkContainer_updateActiveMarks() */
            session->state_timer -= game->clock.delta;

            if ((session->state_timer <= 0) || session->num_combos <= 0)
            {
                for (int i = 0; i < NUM_MARKS; ++i)
                {
                    session->container.marks[i].state = MARK_STATE_NORMAL;
                    Mark_setFlag(&session->container.marks[i], MARK_FLAG_WAS_SET_FALLING, 0);
                }

                /* Spawn a new enemy wave if all of our enemies area already dead */
                if (GameSession_numEnemiesLeft(session) <= 0)
                {
                    /* Add points by the decision time multiplier */
                    MissionType *mission = session->current_mission;

                    if (mission->points > 0)
                    {
                        uint points;
                        double round_base_points = (double)mission->points / (double)mission->num_waves;

                        if (session->player_turn_timer > 0)
                        {
                            double percentage = session->player_turn_timer / PLAYER_TURN_DECISION_TIME;
                            double bonus_portion_multiplier = percentage * (MAXIMUM_BATTLE_POINT_MULTIPLIER - 1.0f);
                            double bonus = MIN(round_base_points * bonus_portion_multiplier, 0.0f);
                            points = (uint)(round_base_points + bonus);
                        }
                        else
                        {
                            points = (uint)round_base_points;
                        }

                        session->points_gained_this_battle += points;
                    }

                    if (session->current_enemy_wave + 1 < session->current_mission->num_waves)
                    {
                        ++session->current_enemy_wave;
                        GameSession_spawnEnemyWave(session, game, session->current_enemy_wave);
                        session->state = SESSION_STATE_PLAYER_TURN;
                        session->state_timer = 0;
                        break;
                    }
                    else
                    {
                        session->score_across_missions += session->points_gained_this_battle;
                        session->savefiles.score = session->score_across_missions;

                        DEBUG_PRINTF("Awarded %u credits from battle. Base from battle: %u",
                            session->points_gained_this_battle,
                            session->current_mission->points);

                        /* Update the score text that persists through missions */
                        {
                            char score_text[128];
                            const char *backup_string;

                            if (snprintf(score_text, 128, "TOTAL CREDITS: %u",
                                session->score_across_missions) <= 0)
                                backup_string = "TOTAL CREDITS: ?";
                            else
                                backup_string = score_text;

                            static SDL_Color white = {255, 255, 255, 255};
                            loadTextureFromTextOnAnyThread(&game->render_thread,
                                &game->active_game_screen.tex_total_score_text,
                                backup_string, Assets_getFont(&game->assets, FONT_ELECTROLIZE),
                                white, 0);

                            if (snprintf(score_text, 128, "GAINED: %u",
                                session->points_gained_this_battle) <= 0)
                                backup_string = "GAINED: ?";
                            else
                                backup_string = score_text;

                            loadTextureFromTextOnAnyThread(&game->render_thread,
                                &game->active_game_screen.tex_points_from_battle,
                                backup_string, Assets_getFont(&game->assets, FONT_ELECTROLIZE),
                                white, 0);

                            DEBUG_PRINTF("pts this battle: %u\n", session->points_gained_this_battle);

                        }

                        session->state = SESSION_STATE_VICTORY;
                        break;
                    }
                }

                /* Check for possible new combos if this wasn't the first iteration */
                if (session->combo_eval_iterations <= 1)
                {
                    session->state = SESSION_STATE_COMBO_EVALUATION;
                }
                else if (session->num_combos > 0)
                {
                    session->state = SESSION_STATE_COMBO_EVALUATION;
                }
                else
                {
                    session->state_timer    = ENEMY_TURN_DURATION;
                    session->state          = SESSION_STATE_ENEMY_TURN;
                }
            }
        }
            break;
        case SESSION_STATE_ENEMY_TURN:
        {
            bool32 can_leave_state = 1;

            TimedEnemyAction *action;
            if (session->state_timer == ENEMY_TURN_DURATION)
            {
                for (int i = 0; i < MAX_NUMBER_OF_ENEMIES; ++i)
                {
                    if (!session->enemies[i].is_active)
                        continue;

                    for (int j = 0; j < session->enemies[i].type->attacks_per_turn; ++j)
                    {
                        action = &session->enemies[i].actions[j];
                        action->time_passed = 0;
                        action->executed = 0;
                        action->damage_applied = 0;
                        action->damage_time = TIMED_COMBO_ACTION_EXEC_TIME + 0.5f;
                        action->target_idle_time = TIMED_COMBO_ACTION_IDLE_TIME;
                        action->target_execution_time = TIMED_COMBO_ACTION_EXEC_TIME;
                    }
                }

                /* Set marks' sprites to their correct positions */
                for (Mark *mark = &session->container.marks[0];
                    mark < &session->container.marks[NUM_MARKS];
                    ++mark)
                {
                    if (Mark_getFlag(mark, MARK_FLAG_IS_ACTIVE))
                        Mark_setRectPositionByGridIndex(mark);
                }
            }
            else
            {
                bool32 update_next_enemy  = 1;
                bool32 update_next_action = 1;
                TimedEnemyAction *action;

                for (int i = 0; i < MAX_NUMBER_OF_ENEMIES; ++i)
                {
                    if (session->enemies[i].is_active)
                    {
                        if (!session->enemies[i].has_attacked)
                        {
                            session->enemies[i].attack_tracker--;
                            session->enemies[i].has_attacked = 1;
                        }

                        if (session->enemies[i].attack_tracker <= 0)
                        {
                            for (int j = 0; j < session->enemies[i].type->attacks_per_turn; ++j)
                            {
                                action = &session->enemies[i].actions[j];
                                if (update_next_action && update_next_enemy)
                                {
                                    action->time_passed += (float)game->clock.delta;

                                    if (action->time_passed < action->target_idle_time)
                                    {
                                        update_next_enemy = 0;
                                        update_next_action = 0;
                                    }

                                    if (action->time_passed >= action->target_execution_time && !action->executed)
                                    {
                                        action->executed = 1;
                                        float angle = getAngleFromPoint(session->player.x + 100, session->player.y + 100, session->enemies[i].x + 50, session->enemies[i].y + 100) + 1.5f;

                                        if (session->enemies[i].type == &game->game_data.enemy_types[SMALL_ENEMY_INDEX])
                                        {
                                            playVisualEffect(&session->effect_container, &session->ef_small_ballistic, session, SPRITE_FLIP_NONE, angle,
                                                2.0f, 2.0f, 0.2f, session->enemies[i].x + 50, session->enemies[i].y + 100, session->player.x + 100, session->player.y + 100);
                                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                                SO_ENEMYATTACK_SMALL), 0);
                                        }
                                        if (session->enemies[i].type == &game->game_data.enemy_types[MEDIUM_ENEMY_INDEX])
                                        {
                                            playVisualEffect(&session->effect_container, &session->ef_medium_ballistic, session, SPRITE_FLIP_NONE, angle,
                                                2.0f, 2.0f, 0.2f, session->enemies[i].x + 50, session->enemies[i].y + 50, session->player.x + 100, session->player.y + 100);
                                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                                SO_ENEMYATTACK_LARGE), 0);
                                        }
                                        if (session->enemies[i].type == &game->game_data.enemy_types[LARGE_ENEMY_INDEX])
                                        {
                                            playVisualEffect(&session->effect_container, &session->ef_medium_rocket, session, SPRITE_FLIP_NONE, angle,
                                                1.5f, 1.5f, 1.0f, session->enemies[i].x + 50, session->enemies[i].y + 100, session->player.x + 100, session->player.y + 100);
                                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                                SO_ENEMYATTACK_LARGE), 0);
                                        }
                                        if (session->enemies[i].type == &game->game_data.enemy_types[TEST_ENEMY_INDEX])
                                        {
                                            playVisualEffect(&session->effect_container, &session->ef_small_rocket, session, SPRITE_FLIP_NONE, angle,
                                                3.0f, 3.0f, 0.5f, session->enemies[i].x + 50, session->enemies[i].y + 50, session->player.x + 100, session->player.y + 100);
                                        }
                                        if (session->enemies[i].type == &game->game_data.enemy_types[FIRST_BOSS_INDEX])
                                        {
                                            playVisualEffect(&session->effect_container, &session->ef_large_ballistic, session, SPRITE_FLIP_NONE, angle,
                                                1.0f, 1.0f, 0.2f, session->enemies[i].x, session->enemies[i].y + 50, session->player.x + 100, session->player.y + 100);
                                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                                SO_ENEMYATTACK_LARGE), 0);
                                        }
                                        if (session->enemies[i].type == &game->game_data.enemy_types[SECOND_BOSS_INDEX])
                                        {
                                            playVisualEffect(&session->effect_container, &session->ef_large_laser, session, SPRITE_FLIP_NONE,
                                                angle, 2.0f, 2.0f, 1.5f, session->enemies[i].x - 200, session->enemies[i].y + 100, session->enemies[i].x - 200, session->enemies[i].y + 100);
                                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                                SO_LASER_BIG), 0);
                                        }
                                        if (session->enemies[i].type == &game->game_data.enemy_types[THIRD_BOSS_INDEX])
                                        {
                                            playVisualEffect(&session->effect_container, &session->ef_medium_rocket, session, SPRITE_FLIP_NONE, angle,
                                                2.0f, 2.0f, 1.0f, session->enemies[i].x, session->enemies[i].y + 50, session->player.x + 100, session->player.y + 100);
                                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                                SO_EXPLOSION_SMALL), 0);
                                        }
                                        if (session->enemies[i].type == &game->game_data.enemy_types[SMALL_WHITE_ENEMY_INDEX])
                                        {
                                            playVisualEffect(&session->effect_container, &session->ef_small_laser, session, SPRITE_FLIP_NONE,
                                                angle, 2.0f, 2.0f, 2.0f, session->enemies[i].x - 100, session->enemies[i].y + 100, session->enemies[i].x - 100, session->enemies[i].y + 100);
                                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                                SO_LASER_SMALL), 0);
                                        }
                                        if (session->enemies[i].type == &game->game_data.enemy_types[MEDIUM_WHITE_ENEMY_INDEX])
                                        {
                                            playVisualEffect(&session->effect_container, &session->ef_medium_laser, session, SPRITE_FLIP_NONE,
                                                angle, 2.0f, 2.0f, 2.0f, session->enemies[i].x - 200, session->enemies[i].y + 100, session->enemies[i].x - 200, session->enemies[i].y + 100);
                                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                                SO_LASER_MEDIUM), 0);
                                        }
                                        if (session->enemies[i].type == &game->game_data.enemy_types[LARGE_WHITE_ENEMY_INDEX])
                                        {
                                            playVisualEffect(&session->effect_container, &session->ef_large_laser, session, SPRITE_FLIP_NONE,
                                                angle, 2.0f, 2.0f, 1.5f, session->enemies[i].x - 200, session->enemies[i].y + 100, session->enemies[i].x - 200, session->enemies[i].y + 100);
                                            Mix_PlayChannel(-1, *Assets_getSoundClip(&game->assets,
                                                SO_LASER_BIG), 0);
                                        }
                                    }

                                    if (action->time_passed >= action->damage_time && !action->damage_applied)
                                    {
                                        if (session->enemies[i].type == &game->game_data.enemy_types[SMALL_ENEMY_INDEX])
                                            Apply_damage_to_player(&session->player, 20, session, game);
                                        if (session->enemies[i].type == &game->game_data.enemy_types[MEDIUM_ENEMY_INDEX])
                                            Apply_damage_to_player(&session->player, 70, session, game);
                                        if (session->enemies[i].type == &game->game_data.enemy_types[LARGE_ENEMY_INDEX])
                                            Apply_damage_to_player(&session->player, 250, session, game);
                                        if (session->enemies[i].type == &game->game_data.enemy_types[TEST_ENEMY_INDEX])
                                            Apply_damage_to_player(&session->player, 75, session, game);
                                        if (session->enemies[i].type == &game->game_data.enemy_types[FIRST_BOSS_INDEX])
                                            Apply_damage_to_player(&session->player, 100, session, game);
                                        if (session->enemies[i].type == &game->game_data.enemy_types[SECOND_BOSS_INDEX])
                                            Apply_damage_to_player(&session->player, 750, session, game);
                                        if (session->enemies[i].type == &game->game_data.enemy_types[THIRD_BOSS_INDEX])
                                            Apply_damage_to_player(&session->player, 150, session, game);
                                        if (session->enemies[i].type == &game->game_data.enemy_types[SMALL_WHITE_ENEMY_INDEX])
                                            Apply_damage_to_player(&session->player, 60, session, game);
                                        if (session->enemies[i].type == &game->game_data.enemy_types[MEDIUM_WHITE_ENEMY_INDEX])
                                            Apply_damage_to_player(&session->player, 70, session, game);
                                        if (session->enemies[i].type == &game->game_data.enemy_types[LARGE_WHITE_ENEMY_INDEX])
                                            Apply_damage_to_player(&session->player, 500, session, game);

                                        action->damage_applied = 1;
                                    }

                                }

                                if (!action->damage_applied)
                                {
                                    can_leave_state = 0;
                                }
                            }
                        }
                    }
                }
            }

            session->state_timer -= (float)game->clock.delta;

            if (session->state_timer <= 0 && can_leave_state )
            {
                if (session->player.hp.current <= 0)
                    session->state = SESSION_GAME_OVER;
                else
                {
                    for (int i = 0; i < (int)session->current_mission->enemy_waves[session->current_enemy_wave].num_enemies; ++i)
                    {
                        if (session->enemies[i].attack_tracker <= 0)
                            session->enemies[i].attack_tracker = session->enemies[i].type->attack_interval;
                        session->enemies[i].has_attacked = 0;
                    }
                    session->state = SESSION_STATE_PLAYER_TURN;
                    session->state_timer = 0;
                }

                char buffer[128];

                for (Enemy *enemy = session->enemies;
                    enemy < &session->enemies[MAX_NUMBER_OF_ENEMIES];
                    ++enemy)
                {
                    if (enemy->is_active)
                    {
                        TextSprite_setText(&game->render_thread,
                            &enemy->enemyturnSprite,
                            Convert_IntToString(buffer, 128, enemy->attack_tracker),
                            Assets_getFont(&game->assets, FONT_ELECTROLIZE),
                            {255, 255, 255, 0});
                    }
                }
            }
        }
            break;

        case SESSION_STATE_VICTORY:
        {
            if (game->active_game_screen.ui.state != MENU_STATE_ERROR)
            {
                game->active_game_screen.ui.state = MENU_STATE_VICTORY;
            }
            VictoryDefeat_update(session, game, &session->victory_ao);
            if (game->active_game_screen.ui.victory_unlock->blocked == 1)
            {
                DEBUG_PRINTF("ASDFDASDFASFADFDASDFASDFADF##################");
                if (game->active_game_screen.ui.victory_unlock->battle_number == 0)
                {
                    session->savefiles.num_missions_unlocked += 2;
                }
                else
                {
                    session->savefiles.num_missions_unlocked += 1;
                }
            }

            if (!session->saved_this_battle)
            {
                SaveGame(session, game);
                session->saved_this_battle = 1;
            }

            game->active_game_screen.ui.victory_unlock->blocked = 0;
            if (game->input.fingers[0].up_this_frame)
            {
                session->playing_anim = false;
                break;
            }
        }
            break;
        case SESSION_GAME_OVER:
        {
            //game->active_game_screen.ui.buttons[0].blocked = 1;

            if (session->gameover_ao.frame < 6)
                VictoryDefeat_update(session, game, &session->gameover_ao);

            if (game->input.fingers[0].up_this_frame)
            {
                game->active_game_screen.ui.pressed = 0;
                game->active_game_screen.game_paused = false;
                session->playing_anim = false;
                session->state = SESSION_STATE_MISSION_SELECTION;
                playMissionSelectionMusic(game);
                game->active_game_screen.ui.state = game->active_game_screen.ui.prev_state;
                game->active_game_screen.ui.prev_state = MENU_STATE_MISSIONS;
                break;
            }
        }
            break;
    }
}
