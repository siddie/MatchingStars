/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include "render.h"

/* Forward declaration(s) */
struct Game;

typedef struct UI           UI;
typedef struct Button       Button;
typedef struct DropMenu     DropMenu;
typedef struct Button       Button;
typedef struct ProgressBar  ProgressBar;

void Button_create(UI *ui,
                   IRect rect,
                   TextureArea texar_normal,
                   TextureArea texar_press,
                   void (*onPress)(Button*, Game*),
                   void (*onRelease)(Button*, Game*),
                   const char *label,
                   int font_scale);

void
Button_update(Button *button, UI *ui, Game *game);

void
Button_draw(UI *ui, Game *game);

void
Button_press(Button *button);

void
Button_release(Button *button);

int
DropMenu_init(DropMenu *menu, Game *game);

void
DropMenu_draw(DropMenu *menu, Game *game);

void
DropMenu_update(UI *ui, Game *game);

void
LabelCheck(Button *b, Game *game);

void
DropMenu_addButton(DropMenu *menu,
                   void (*onPress)(Button*, Game*),
                   void (*onRelease)(Button*, Game*),
                   Texture *texture,
                   const char *label);

void
ProgressBar_create(ProgressBar *bar,
                   TextureArea *txa_background, int bg_offset_x, int bg_offset_y,
                   TextureArea *txa_bar, int bar_offset_x, int bar_offset_y,
                   TextureArea *txa_border, int border_offset_x, int border_offset_y,
                   int x, int y, int w, int h, int max, int cur);

void
ProgressBar_draw(ProgressBar *bar, Game *game, float scalex, float scaley, int min, int max, int cur);

void
Menu_state_check(UI *ui);

void
UI_draw(UI *ui, Game *game);

void
UI_update(UI *ui, Game *game);

enum ButtonState
{
    BUTTON_STATE_NORMAL,
    BUTTON_STATE_PRESSED
};

enum MenuState
{
    MENU_STATE_NORMAL,
    MENU_STATE_MISSIONS,
    MENU_STATE_BATTLES1,
    MENU_STATE_BATTLES2,
    MENU_STATE_BATTLES3,
    MENU_STATE_OPTIONS,
    MENU_STATE_VICTORY,
    MENU_STATE_GAMEOVER,
    MENU_STATE_PAUSE,
    MENU_STATE_QUIT_CONFIRM,
    MENU_STATE_START,
    MENU_STATE_ERROR
};

void
DropMenu_create(UI *ui,
                IRect rect,
                int button_draw_spacing,
                int button_draw_margin,
                Texture *dropmenu_texture,
                Texture *default_button_texture,
                enum MenuState state);

enum Anchor
{
    ANCHOR_TOP_LEFT,
    ANCHOR_TOP_RIGHT,
    ANCHOR_TOP_CENTER,
    ANCHOR_BOTTOM_LEFT,
    ANCHOR_BOTTOM_RIGHT,
    ANCHOR_BOTTOM_CENTER,
    ANCHOR_CENTER_LEFT,
    ANCHOR_CENTER_RIGHT,
    ANCHOR_CENTER_CENTER
};

struct Button
{
    DropMenu            *head;
    Button              *next;
    int                 touch_finger;
    int                 parent_id;
    int                 font_scale;
    int                 mission_number;
    int                 battle_number;
    enum ButtonState    state;
    enum Anchor         anchor;
    IRect               rect;
    void                (*onPress)(Button*, Game*);
    void                (*onRelease)(Button*, Game*);
    TextureArea         texar_normal;
    TextureArea         texar_press;
    TextureArea         texar_blocked;
    Texture             label;
    const char          *label_text;
    bool32              slider;
    bool32              blocked;
    int                 slider_min;
    int                 slider_max;
    int                 slider_total;
    UI                  *ui;
};

struct DropMenu
{
    enum MenuState  state;
    Button          *next;
    IRect           rect;
    bool32          pressed;
    int             id;
    int             button_amount;
    int             button_draw_spacing;
    int             button_draw_margin;
    Texture         *dropmenu_texture;
    Texture         *default_button_texture;
    UI              *ui;
};

struct ProgressBar
{
    TextureArea *txa_background;
    TextureArea *txa_bar;
    TextureArea *txa_border;
    IVec2       bg_offset;
    IVec2       border_offset;
    IVec2       bar_offset;
    IRect       rect;
};

struct UI
{
    enum MenuState  state;
    enum MenuState  prev_state;
    Button          buttons[50];
    Button          *victory_unlock;
    DropMenu        menus[20];
    bool32          pressed;
    int             button_amount;
    int             menu_amount;
    ProgressBar     volume;
    Game            *game;
};


