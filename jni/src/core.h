/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include <errno.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_opengles.h>
#include <SDL_mixer.h>
#include "render.h"
#include "UI.h"
#include "gamesession.h"
#include "assets.h"
#include "defs.h"
#include "compile_flags.h"

#define FPS_LIMIT 60
#define NUM_TOUCH_FINGERS 10
#define TEXTURE_JOB_QUEUE_INITIAL_CAPACITY 64
#define MUSIC_JOB_QUEUE_INITIAL_CAPACITY 8
#define NUM_MISSION_TYPES 5
#define NUM_LEVELS 3
#define ON_DEMAND_TEXTURE_DISPOSE_INTERVAL 5.0f /* Seconds */
#define ON_DEMAND_TEXTURE_PRESERVE_TIME 5.0f
#define TRUE_LOGO_SWAP_TIME 15

enum GameScreen
{
	SCREEN_MAIN_MENU,
	SCREEN_ACTIVE_GAME,
#ifdef _DEBUG
	SCREEN_DEBUG
#endif
};

typedef struct Task             Task;
typedef struct TaskQueue        TaskQueue;
typedef struct Window           Window;
typedef struct Clock            Clock;
typedef struct TextureJob       TextureJob;
typedef struct RenderThread     RenderThread;
typedef struct MenuScreen       MenuScreen;
#ifdef _DEBUG
    typedef struct DebugScreen      DebugScreen;
#endif
typedef struct ActiveGameScreen ActiveGameScreen;
typedef struct CreditScreen     CreditScreen;
typedef struct Assets           Assets;
typedef struct TouchFinger      TouchFinger;
typedef struct Input            Input;
typedef struct GameData         GameData;
typedef struct Game             Game;

int
TaskQueue_create(TaskQueue *queue, uint initial_capacity);

/* Return 0 on success.
 * Possible reasons of failure: maximum queue size was reached and
 * failed to allocate more space for it */
int
TaskQueue_enqueueTask(TaskQueue *queue, void (*func)(void*), void* args);

void
TaskQueue_processTasks(TaskQueue *queue);

void
Clock_tick(Clock *clock);

int
RenderThread_enqueueTextureJob(RenderThread *thread,
    Texture *texture, SDL_Surface *surface);

int
Game_init(Game *game); /* Return 0 on success */

void
Game_uninit(Game *game);

void
Game_mainLoop(Game *game);

void
Game_render(Game *game);

void *
Game_renderThreaded(void *game);

int
Game_handleAndroidOnPause(void *data, SDL_Event *event);

void
Game_setScreen(Game *game, GameScreen screen);

void
Game_shutDown(Game *game);

void
calculateWindowViewport(int *viewport, Window *window);

/* MenuScreen */

int
MenuScreen_init(MenuScreen *screen, Game *game);

void
MenuScreen_render(MenuScreen *screen, Game *game, int *viewport);

void
MenuScreen_update(MenuScreen *screen, Game *game);

void
MenuScreen_handleEvent(MenuScreen *screen, Game *game, SDL_Event *event);

/* DebugScreen */
#ifdef _DEBUG
    int
    DebugScreen_init(DebugScreen *screen, Game *game);

    void
    DebugScreen_onSetCurrent(DebugScreen *screen, Game *game);

    void
    DebugScreen_render(DebugScreen *screen, Game *game);

    void
    DebugScreen_update(DebugScreen *screen, Game *game);

    void
    DebugScreen_handelEvent(DebugScreen *screen, Game *game, SDL_Event *event);
#endif

/* ActiveGameScreen */
int
ActiveGameScreen_init(ActiveGameScreen *screen, Game *game);

void
ActiveGameScreen_onSetCurrent(ActiveGameScreen *screen, Game *game);

void
ActiveGameScreen_render(ActiveGameScreen *screen, Game *game, int *viewport);

void
ActiveGameScreen_update(ActiveGameScreen *screen, Game *game);

void
ActiveGameScreen_handleEvent(ActiveGameScreen *screen, Game *game, SDL_Event *event);

int
Assets_loadFonts(Assets *assets, RenderThread *render_thread);

/* This is run on the main thread for the bitmap loading part -
 * the texture creations themselves are queued to the render thread.
 * This means that the widths and heights of textures are immediately
 * available upon being loaded by this function */
int
Assets_loadTextures(RenderThread *render_thread, Assets *assets);

int
Assets_loadMusic(Assets *assets);

int
Assets_loadSoundClips(Assets *assets);

int
Assets_loadAnimations(Assets *assets);

int
Assets_loadOnDemandTextures(Assets *assets);

void
onDemandTextureLoadJob(void *args);

inline TTF_Font *
Assets_getFont(Assets *assets, int index);

inline Texture *
Assets_getTexture(Assets *assets, int index);

void
GameData_init(GameData *data, Game *game);

inline void
translateScreenCoordinatesByWindowViewport(uint *x, uint *y,
    int real_x, int real_y, Window *window);

#ifdef _DEBUG
void
initFpsTextTexture(Texture *texture, RenderThread *thread, TTF_Font *font);

void
updateFpsTextTexture(Texture*, RenderThread *thread, double, TTF_Font*);
#endif

void
setFps30 (Button *button, Game *game);

void
setFps60 (Button *button, Game *game);

struct Task
{
    void (*func)(void*);
    void *args;
};

struct TaskQueue
{
    Task            *tasks;
    volatile uint   num_tasks;
    volatile uint   capacity;
    pthread_mutex_t mutex;
};

inline void
TaskQueue_destroy(TaskQueue *queue)
{
    if (queue->tasks)
        free((void*)queue->tasks);
    queue->tasks     = 0;
    queue->num_tasks = 0;
    queue->capacity  = 0;
}

struct Window
{
    SDL_Window      *sdl_window;
    uint            width;
    uint            height;
    IVec2           resolution;
    int             viewport[4];
};

struct Clock
{
    double  delta;      /* Format: seconds */
    double  last_tick;
    double  fps;
    bool32  limit_fps;
    uint    target_fps;
};

struct MenuScreen
{
    UI              ui;
    AnimatedObject  start_animation;
    bool32          anim_playing;
    bool32          is_start_anim_freed;
    uint            time_in_screen;
    uint            logo_clip[4];
    bool32          playing_true_music;
    bool32          played_explosion;
    AnimatedObject  ao_explosion;

    #define TEX_CREDITS_JESSE   0
    #define TEX_CREDITS_ILKKA   1
    #define TEX_CREDITS_JANINA  2
    #define TEX_CREDITS_LASSE   3
    #define TEX_CREDITS_KUVIS   4
    #define TEX_CREDITS_LOMMI   5
    #define TEX_CREDITS_PYRY    6
    #define TEX_CREDITS_JAMA    7
    #define TEX_CREDITS_KEYO    8

    Texture         tex_credits[9];

    double          credits_draw_y;
};

struct DebugScreen
{
    UI              ui;
    DropMenu        dropmenu;
    AnimatedObject  debug_object;
};

struct ActiveGameScreen
{
    GameSession         session;

    TextureArea         texar_mark_ballistic;
    TextureArea         texar_mark_shield;
    TextureArea         texar_mark_repair;
    TextureArea         texar_mark_rocket;
    TextureArea         texar_mark_laser;
    VisualEffectType    effect_type1;
    UI                  ui;

    Texture             tex_mission_selection_header;
    Texture             tex_mission_selection_text;
    Texture             tex_total_score_text;
    Texture             tex_points_from_battle;

    AnimatedObject      ao_player_ship; /* For the menu screen */

	uint                bg_x, bg_y, scroll_value;

    bool32              game_paused;
};

struct CreditScreen
{
    Texture tex_credits;
};

struct TextureJob
{
    SDL_Surface *surface;
    Texture     *texture;
};

struct RenderThread
{
    pthread_t       native_handle;
    TaskQueue       task_queue;
	Clock           clock;

    TextureJob      *texture_jobs;
    volatile int    texture_job_queue_capacity;
    volatile int    num_texture_jobs;
    pthread_mutex_t texture_job_mutex;
    double          on_demand_texture_dispose_timer;
};

struct Assets
{
    Texture         tex_loading_text;
#ifdef _DEBUG
    Texture         fpsText;
#endif
    TTF_Font*       fonts[NUM_FONT_ASSETS];
    Texture         textures[NUM_TEXTURE_ASSETS];
    Mix_Music       *music_assets[NUM_MUSIC_ASSETS];
    Mix_Chunk       *sound_clips[NUM_SOUND_CLIPS];
    OnDemandTexture on_demand_textures[NUM_ON_DEMAND_TEXTURE_ASSETS];

    Mix_Music       *true_theme_song;

	Animation       animation1;
    Animation       animation2;
    Animation       boss1_animation;
    Animation       boss2_animation;
    Animation       finalboss_animation;
    Animation       small_laser_animation;
    Animation       medium_laser_animation;
    Animation       large_laser_animation;
    Animation       explosion_animation;
    Animation       target_animation;
    Animation       small_rocket_animation;
    Animation       medium_rocket_animation;
    Animation       large_rocket_animation;
    Animation       small_bullet_animation;
    Animation       medium_bullet_animation;
    Animation       large_bullet_animation;
    Animation       shockwave_animation;
    Animation       antimatter_animation;
    Animation       shield_animation;
    Animation       health_animation;
    Animation       gameover_animation;
    Animation       victory_animation;
    Animation       mediumenemy_animation;
    Animation       textsprite_animation;
    Animation       largeenemy_animation;
    Animation       start_screen_animation;

    Animation       an_enemy_white_small;
    Animation       an_enemy_white_medium;
    Animation       an_enemy_white_large;

    AnimationFrame  health_frames[9];

    AnimationFrame  mark_normal_frames[NUM_MARK_TYPES];
    Animation       an_marks_normal[NUM_MARK_TYPES];

    #define NUM_MARK_COMBO_SMALL_FRAMES 7
    AnimationFrame  mark_combo_small_frames[NUM_MARK_TYPES * NUM_MARK_COMBO_SMALL_FRAMES];
    Animation       an_mark_combo_small[NUM_MARK_TYPES];

    #define NUM_MARK_COMBO_LARGE_FRAMES 5
    AnimationFrame  mark_combo_large_frames[NUM_MARK_TYPES * NUM_MARK_COMBO_LARGE_FRAMES];
    Animation       an_mark_combo_large[NUM_MARK_TYPES];
};

struct TouchFinger
{
    bool32 finger_down;
    bool32 down_this_frame;
    bool32 up_this_frame;
    UIVec2 last_position;
    UIVec2 position;
    UIVec2 touch_position; /* If the finger is down, this is the position it was pressed down at. */
};

struct Input
{
    TouchFinger fingers[NUM_TOUCH_FINGERS];
};

struct GameData
{
    EnemyType   enemy_types[NUM_ENEMY_TYPES];
    MissionType mission_types[NUM_MISSION_TYPES];
    MissionType mission_types_level2[NUM_MISSION_TYPES];
    MissionType mission_types_level3[NUM_MISSION_TYPES];
    LevelType   level_types[NUM_LEVELS];
};

struct Game
{
    Window				        window;
    Clock				        clock;
    Assets				        assets;
    GameData                    game_data;
    Input				        input;
    RenderThread		        render_thread;
    volatile bool32	            running;
    SpriteBatch			        spritebatch;
    SDL_GLContext		        gl_context;
    volatile enum GameScreen    current_screen;
    TaskQueue                   task_queue; /* For actions that must be executed on the main thread */

    MenuScreen			        menu_screen;
#ifdef _DEBUG
    DebugScreen			        debug_screen;
#endif
    ActiveGameScreen            active_game_screen;
};

inline TTF_Font *
Assets_getFont(Assets *assets, int index)
{
    return assets->fonts[index];
}

inline Texture *
Assets_getTexture(Assets *assets, int index)
{
    return &assets->textures[index];
}

inline Mix_Music **
Assets_getMusic(Assets *assets, int index)
{
    return &assets->music_assets[index];
}

inline Mix_Chunk **
Assets_getSoundClip(Assets *assets, int index)
{
    return &assets->sound_clips[index];
}

inline Texture *
Assets_getOnDemandTexture(Assets *assets, int index, Game *game)
{
    assets->on_demand_textures[index].last_use_time = (uint)game->clock.last_tick;

    if (!assets->on_demand_textures[index].texture.id && !assets->on_demand_textures[index].loading)
    {
        /* Queue up */
        assets->on_demand_textures[index].loading = 1;
        TaskQueue_enqueueTask(&game->render_thread.task_queue,
            onDemandTextureLoadJob, &assets->on_demand_textures[index]);
    }

    return &assets->on_demand_textures[index].texture;
}
