APP_PLATFORM := android-19
APP_OPTIM := release
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

DEBUG_C_FLAGS = -Wall -D_USE_MATH_DEFINES -D_DEBUG -D_REENTRANT -std=c++11 -pthread -lm
OPTIMIZED_C_FLAGS = -Wall -D_USE_MATH_DEFINES -D_REENTRANT -std=c++11 -pthread -O3 -lm

LOCAL_CFLAGS += $(OPTIMIZED_C_FLAGS)

LOCAL_MODULE := main

SDL_PATH := ../SDL2

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include \
	$(LOCAL_PATH)/../SDL2_image \
	$(LOCAL_PATH)/../SDL2_ttf \
	$(LOCAL_PATH)/../SDL2_mixer

# Add your application source files here...
LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c \
	main.cpp spritebatch.cpp

LOCAL_SHARED_LIBRARIES := SDL2 SDL2_image SDL2_ttf SDL2_mixer pthread

LOCAL_LDLIBS := -lGLESv1_CM -llog -lGLESv2 -lEGL


include $(BUILD_SHARED_LIBRARY)
