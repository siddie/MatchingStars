/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#pragma once
#include "spritebatch.h"
#include "defs.h"
#include "UI.h"
#include "render.h"

/* Forward declaration(s) */
struct Game;
struct ActiveGameScreen;

#define MARK_GRID_WIDTH 6
#define MARK_GRID_HEIGHT 6
#define MARK_GRID_SIZE (MARK_GRID_WIDTH * MARK_GRID_HEIGHT)
#define NUM_MARKS (MARK_GRID_WIDTH * MARK_GRID_HEIGHT + 6)
#define GRID_PIXEL_OFFSET_X 0
#define GRID_PIXEL_OFFSET_Y (RESOLUTION_HEIGHT - MARK_GRID_HEIGHT * MARK_PIXEL_SIZE)
#define MAX_NUMBER_OF_ENEMIES 4
#define NUM_MARK_TYPES 5
#define MAX_TIMED_COMBO_ACTIONS (MARK_GRID_SIZE / 3)
#define MARK_MIN_PIXEL_X GRID_PIXEL_OFFSET_X
#define MARK_MAX_PIXEL_X (GRID_PIXEL_OFFSET_X + MARK_GRID_WIDTH * MARK_PIXEL_SIZE - MARK_PIXEL_SIZE)
#define MARK_MIN_PIXEL_Y GRID_PIXEL_OFFSET_Y
#define MARK_MAX_PIXEL_Y (GRID_PIXEL_OFFSET_Y + MARK_GRID_HEIGHT * MARK_PIXEL_SIZE - MARK_PIXEL_SIZE)

#define MAX_ENEMIES_PER_WAVE 4
#define MAX_ENEMY_WAVES_PER_MISSION 16

#define GRID_REFILL_DURATION 0.5f /* Seconds */
#define COMBO_SHOWOFF_DURATION 0.35f
#define COMBO_CLEANUP_DURATION 0.5f
#define ENEMY_TURN_DURATION 0.5f
#define BALLISTIC_BASE_DAMAGE 40
#define LASER_BASE_DAMAGE 50
#define ROCKET_BASE_DAMAGE 60

/* Enemy type indices */

enum EntityIndex
{
    SMALL_ENEMY_INDEX = 0,
    MEDIUM_ENEMY_INDEX,
    LARGE_ENEMY_INDEX,
    TEST_ENEMY_INDEX,
    FIRST_BOSS_INDEX,
    SMALL_WHITE_ENEMY_INDEX,
    MEDIUM_WHITE_ENEMY_INDEX,
    LARGE_WHITE_ENEMY_INDEX,
    SECOND_BOSS_INDEX,
    THIRD_BOSS_INDEX,

    NUM_ENEMY_TYPES
};

/* Mission type indices */
#define MISSION_1_INDEX 0
#define MISSION_2_INDEX 1
#define MISSION_3_INDEX 2
#define MISSION_4_INDEX 3
#define BOSS_BATTLE_1_INDEX 4
#define BOSS_BATTLE_2_INDEX 5
#define BOSS_BATTLE_3_INDEX 6

#define NUM_MAX_MISSION_MUSIC_TRACKS 4

#define NUM_LEVELS 3

#define TIMED_COMBO_ACTION_EXEC_TIME 0.3f
#define TIMED_COMBO_ACTION_IDLE_TIME 0.3f
#define BALLISTIC_DAMAGE_DELAY  0.5f

#define SHIELD_INDICATOR_Y (GRID_PIXEL_OFFSET_Y - 180)
#define NUM_OF_DEBUG_ENEMY_ACTIONS 3

#define PLAYER_TURN_DECISION_TIME 15.0f
#define MAXIMUM_BATTLE_POINT_MULTIPLIER 4.0f

enum MarkType
{
    MARK_BALLISTIC = 0, /* Order must not change because frame order in assets relies on it */
    MARK_SHIELD,
    MARK_REPAIR,
    MARK_ROCKET,
    MARK_LASER
};

enum EnemyAttackType
{
    ENEMY_BALLISTIC_ATTACK,
    ENEMY_MISSILE_ATTACK,
    ENEMY_LASER_ATTACK
};

enum MarkState
{
    MARK_STATE_NORMAL = 0,
    MARK_STATE_FALLING
};

enum MarkAnimationState
{
    MARK_ANIM_NORMAL = 0,
    MARK_ANIM_COMBO
};

enum MarkAxisLock
{
    MARK_AXIS_LOCK_TO_NONE,
    MARK_AXIS_LOCK_TO_X,
    MARK_AXIS_LOCK_TO_Y
};

enum SessionState
{
    SESSION_STATE_MISSION_SELECTION,
    SESSION_STATE_PLAYER_TURN,
    SESSION_STATE_COMBO_EVALUATION,
    SESSION_STATE_COMBO_SHOWOFF,
    SESSION_STATE_COMBO_CLEANUP,
    SESSION_STATE_REFILL_GRID,
    SESSION_STATE_ENEMY_TURN,
    SESSION_GAME_OVER,
    SESSION_STATE_VICTORY
};

enum MarkDir
{
    MARK_DIR_NONE,
    MARK_DIR_LEFT,
    MARK_DIR_RIGHT,
    MARK_DIR_UP,
    MARK_DIR_DOWN
};

enum MarkFlag
{
    MARK_FLAG_CHECKED_LEFT      = (1 << 0),
    MARK_FLAG_CHECKED_RIGHT     = (1 << 1),
    MARK_FLAG_CHECKED_UP        = (1 << 2),
    MARK_FLAG_CHECKED_DOWN      = (1 << 3),
    MARK_FLAG_IS_ACTIVE         = (1 << 4),
    MARK_FLAG_IS_PART_OF_COMBO  = (1 << 5),
    MARK_FLAG_WAS_SET_FALLING   = (1 << 6)
};

typedef struct Mark                 Mark;
typedef struct MarkCombo            MarkCombo;
typedef struct MarkContainer        MarkContainer;
typedef struct Health               Health;
typedef struct Player			    Player;
typedef struct Enemy			    Enemy;
typedef struct Effect			    Effect;
typedef struct Projectile		    Projectile;
typedef struct EnemyType            EnemyType;
typedef struct EnemyWave            EnemyWave;
typedef struct MissionMusicTrack    MissionMusicTrack;
typedef struct MissionType          MissionType;
typedef struct LevelType            LevelType;
typedef struct Level                Level;
typedef struct TimedComboAction     TimedComboAction;
typedef struct TimedEnemyAction     TimedEnemyAction;
typedef struct GameSession          GameSession;
typedef struct SaveFiles            SaveFiles;

void
GameSession_unlockLevel(GameSession *session, Level *level, Game *game);

void
GameSession_beginMission(GameSession *session, MissionType *mission, Game *game);

void
GameSession_initializeNew(GameSession *session, Game *game);

/* Overrides old enemies */
void
GameSession_spawnEnemyWave(GameSession *session, Game *game, int next_wave_index);

void
GameSession_render(GameSession *session, Game *game, int *viewport);

uint
GameSession_numEnemiesLeft(GameSession *session);

/* Result is returned into the MarkCombo arrays contained
 * within the session */
void
GameSession_evaluatePoints(GameSession *session);

/* This one's called by evaluatePoints() recursively */
void
checkAdjacentMarks(GameSession *session, Mark *mark, int *score, MarkDir dir);

void
GameSession_update(GameSession *session, Game *game);

void
Player_update(GameSession *session, Game *game);

void
Hpbar_update(GameSession *session, Game *game);

void
VictoryDefeat_update(GameSession *session, Game *game, AnimatedObject *anim);

void
Enemy_setType(Enemy *enemy, EnemyType *type);

float
getAngleFromPoint(int x1, int y1, int x2, int y2);

void
playMissionSelectionMusic(Game *game);

/* Return value: -1 if unsuccessful,
 * otherwise the new mark array index of mark1 */
int
MarkContainer_swapMarks(MarkContainer *container,
    int mark1_grid_x, int mark1_grid_y,
    int mark2_grid_x, int mark2_grid_y);

void
MarkContainer_updateActiveMarks(MarkContainer *container, Game *game);

void
Effect_update(Projectile *projectile, void* game);

void
TimedComboAction_execute(TimedComboAction *action, GameSession *session);

bool32
TimedComboAction_update(TimedComboAction *action, Game *game);

inline void
Activate_enemy(Enemy *enemy, int x, int y, GameSession *session, Game *game);

inline void
Activate_target(GameSession *session);

inline void
Activate_target(Enemy *enemy, GameSession *session);

inline void
Mark_draw(Mark *mark, SpriteBatch *batch);

inline void
Mark_setAnimationState(Mark *mark, enum MarkAnimationState animation);

inline void
Mark_update(Mark *mark, double dt, Game *game);

inline void
Mark_setAnimationByType(Mark *mark, Game *game);

inline void
Mark_setRectPositionByGridIndex(Mark *mark);

inline void
Mark_setFalling(Mark *mark, int start_height);

inline bool32
Mark_getFlag(Mark *mark, MarkFlag flag);

inline void
Mark_setFlag(Mark *mark, MarkFlag flag, bool32 val);

inline void
Apply_damage(Enemy *enemy, int amount, GameSession *session, Game *game);

inline void
Apply_damage_to_player(Player *player, int amount, GameSession *session, Game *game);

inline void
Apply_shield(Player *player, int shieldamount);

inline void
Sprite_text(GameSession *session, TextSpriteContainer *container, Game *game, int amount, int xpos, int ypos);

void
SaveGame(GameSession *session, Game *game);

void
LoadGame(GameSession *session, Game *game);

inline bool32
isMarkPositionSwapLegal(int grid_x, int grid_y,
    int selected_grid_x, int selected_grid_y);

inline void
Mark_randomizeType(Mark *mark, Game *game);

inline void
Mark_randomizeTypeAndForceNoCombos(Mark *mark, GameSession *session, Game *game);

inline Mark *
MarkContainer_getMarkByGridPosition(MarkContainer *container, int x, int y);

void
MarkContainer_updateActiveMarks(MarkContainer *container, Game *game);

EnemyType
EnemyType_create(char *name, int max_health, Animation *default_animation,
    int attack_interval, EnemyAttackType attack_type, int hpbar_offset_y, int attacks_per_turn);

struct Mark
{
    enum MarkType           type;
    enum MarkState          state;
    IRect                   rect;
    int                     grid_index;
    int                     fall_start_height;
    char                    flags;
    enum MarkAnimationState animation_state;
    AnimatedObject          aobject;

    /* Sequence counters for adjacent vertical and horizontal
     * marks of similar type, used in point evaluation */
    int                     h_sequence_count, v_sequence_count;

    /*Score is required to play the correct animations */
    int                     score;
};

struct MarkCombo
{
    enum MarkType   type;
    int             sequence_len;
};

struct MarkContainer
{
    int             grid[MARK_GRID_WIDTH * MARK_GRID_HEIGHT];
	Mark            marks[NUM_MARKS];
};

struct Health
{
    int current, max;
};

struct Shield
{
    int shieldamount;
};

struct Player
{
    AnimatedObject		ao, baranimation, shieldbari, healthbari;
	int				    x,y;
	Health				hp;
    ProgressBar         hpbar;
    Shield              shield;
    ProgressBar         shieldbar;
	float				laser_angle;
	float				float_timer;
    TextSprite          textSprite, healthsprite, absorbtext;
};

struct Effect
{
	AnimatedObject		animatedobject;
	int				    x,y;
	bool32				is_active;
};

struct Projectile
{
    AnimatedObject		animatedobject;
    int				    x, y;
    int                 startx, starty;
    bool32				is_active;
    float               timer, timepassed, angle;
};

struct TimedComboAction
{
    float               time_passed;
    float               target_idle_time;       /* Time until the next action is fired */
    float               target_execution_time;  /* Time until the effect of the action is applied (damage, healing, what ever) */
    float               animation_speed;
    float               size;
    enum MarkType       type;
    uint                power;
    bool32              executed;
    bool32              damage_applied;
    VisualEffectType    *effect;
    Enemy               *targets[4];
    uint                num_targets;
};

struct TimedEnemyAction
{
    float                   time_passed;
    float                   target_idle_time;       /* Time until the next action is fired */
    float                   target_execution_time;  /* Time until the effect of the action is applied (damage, healing, what ever) */
    float                   damage_time;            /*time when the Apply_damage is executed*/
    enum EnemyAttackType    type;
    uint                    power;
    bool32                  executed;
    bool32                  damage_applied;
};

struct Enemy
{
    EnemyType           *type;
    AnimatedObject		ao;
    int				    x, y;
    Health				hp;
    ProgressBar         hpbar;
    bool32				is_active, has_attacked, is_moving;
    int                 attack_tracker;
    TimedEnemyAction    actions[NUM_OF_DEBUG_ENEMY_ACTIONS];
    TextSprite          enemyturnSprite;
    VisualEffectType    enemyturn;
    int                 movement_speed;
};

struct EnemyType
{
    const char              *name;
    int                     max_health;
    Animation               *default_animation;
    enum EnemyAttackType    attack_type;
    int                     attack_interval; //how often enemy attacks
    int                     hpbar_offset_y;
    int                     attacks_per_turn;
};

struct EnemyWave
{
    EnemyType   *enemies[MAX_ENEMIES_PER_WAVE];
    uint        num_enemies;
};

struct MissionMusicTrack
{
    Mix_Music   **music;
    int         loop; /* -1 for forever, otherwise >= 0 */
};

struct MissionType
{
    uint                points;
    int                 background_index;
    const char          *name;
    EnemyWave           enemy_waves[MAX_ENEMY_WAVES_PER_MISSION];
    uint                num_waves;
    bool32              is_locked;
    MissionMusicTrack   music_tracks[NUM_MAX_MISSION_MUSIC_TRACKS];
    uint                num_music_tracks;
};

struct LevelType
{
    MissionType     *missions[5];
    ubyte           num_missions;
};

struct Level
{
    LevelType       *type;
    bool32          locked;
    uint            num_missions_unlocked;
};

struct SaveFiles
{
    int                    score;
    int                    num_missions_unlocked;
};

struct GameSession
{
    Game                    *game;

    Level                   levels[NUM_LEVELS];
    uint                    score_across_missions;
    MarkContainer           container;
    MarkCombo               combos[MARK_GRID_SIZE / 3];
    int                     num_combos;

    /* Index (in the mark array) of the mark currently selected */
    int                     selected_index;
     /* The grid position that was selected when the screen was first pressed */
    IVec2                   first_selected_grid_position;
    /* Pixel offset for the mark, calculated from the touch position */
    IVec2                   selection_offset;

    VisualEffectContainer   effect_container;
    bool32                  target_all_enemies;
    TextSpriteContainer     textsprite_container;
    Enemy*                  targeted_enemy, *targeted_enemy2, *targeted_enemy3;
    enum SessionState       state;
    enum MarkAxisLock       axis_lock;
	Player				    player;
	Effect				    effect, target;

    VisualEffectType        ef_small_ballistic, ef_medium_ballistic,
                            ef_large_ballistic, ef_small_rocket,
                            ef_medium_rocket, ef_large_rocket,
                            ef_explosion, ef_small_laser, ef_medium_laser,
                            ef_large_laser, numberef, healthbaref,
                            ef_absorb, ef_shockwave, ef_antimatter;

    TextureArea             hpbar_outline, hpbar_bg, hpbar_fill, enemyhpbar_fill;
    TextureArea             shieldbar_outline, shieldbar_bg, shieldbar_fill;
	Enemy				    enemies[MAX_NUMBER_OF_ENEMIES];
    AnimatedObject          gameover_ao;
    AnimatedObject          victory_ao;
    float                   state_timer, target_angle;

    VisualEffectType        debug_effect;

    /* How many combo evaluations we've made this round (have to know so that we can
     * break into the next state) */
    int                     combo_eval_iterations;
    TimedComboAction        timed_combo_actions[MAX_TIMED_COMBO_ACTIONS];
    uint                    num_timed_combo_actions;

    /* Mission data */
    MissionType             *current_mission;
    uint                    current_enemy_wave;

    bool32                  playing_anim;
    bool32                  laser_visual_activated;
    SaveFiles               savefiles;

    bool32                  screen_shaking;
    uint                    shake_frames_left;

    int                     playing_music_index; /* -1 if none */

    double                  player_turn_timer;
    uint                    points_gained_this_battle;

    bool32                  saved_this_battle;
};
