/*  Copyright 2016 Team Rogues
 *
 *  This file is part of Matching Stars.
 *
 *  Matching Stars is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Matching Stars is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Matching Stars. If not, see <http://www.gnu.org/licenses/>. */

#include <unistd.h>
#include "core.h"
#include "render.cpp"
#include "UI.cpp"
#include "menuscreen.cpp"
#include "gamesession.cpp"
#include "activegamescreen.cpp"

pthread_mutex_t GLOBAL_MALLOC_MUTEX = {};

#ifdef _DEBUG
    #include "debugscreen.cpp"
#endif

int
TaskQueue_create(TaskQueue *queue, uint initial_capacity)
{
    queue->num_tasks = 0;
    queue->capacity  = initial_capacity;
    queue->tasks     = (Task*)malloc(initial_capacity * sizeof(Task));
    return pthread_mutex_init(&queue->mutex, 0);
}

int
TaskQueue_enqueueTask(TaskQueue *queue, void (*func)(void*), void* args)
{
    pthread_mutex_lock(&queue->mutex);

    /* Allocate more if required */
    if (queue->capacity < queue->num_tasks + 1)
    {
        lockGlobalMallocMutex();

        uint new_size = (uint)((float)queue->capacity * 1.20f);
        if (new_size <= queue->capacity)
            new_size = queue->capacity + 1;

        void *new_tasks = malloc(new_size * sizeof(Task));
        assert(new_tasks);

        if (!new_tasks)
        {
            pthread_mutex_unlock(&queue->mutex);
            unlockGlobalMallocMutex();
            return 1;
        }

        memcpy(new_tasks, queue->tasks, queue->num_tasks * sizeof(Task));
        free(queue->tasks);

        unlockGlobalMallocMutex();

        queue->tasks    = (Task*)new_tasks;
        queue->capacity = new_size;

        DEBUG_PRINTF("Note: TaskQueue maximum capacity reached. Expanding to: %u\n", new_size);
    }

    queue->tasks[queue->num_tasks].func = func;
    queue->tasks[queue->num_tasks].args = args;
    ++queue->num_tasks;

    pthread_mutex_unlock(&queue->mutex);

    return 0;
}

void
TaskQueue_processTasks(TaskQueue *queue)
{
    if (queue->num_tasks > 0)
    {
        pthread_mutex_lock(&queue->mutex);

        for (uint i = 0; i < queue->num_tasks; ++i)
        {
            queue->tasks[i].func(queue->tasks[i].args);
        }

        queue->num_tasks = 0;

        pthread_mutex_unlock(&queue->mutex);
    }
}

void
Clock_tick(Clock *clock)
{
    double tick_time = (double)SDL_GetPerformanceCounter();
    double perf_freq = (double)SDL_GetPerformanceFrequency();
    clock->delta = (tick_time - clock->last_tick) / perf_freq;
    clock->last_tick = tick_time;
    clock->fps = 1.0f / clock->delta;

    if (clock->target_fps != 0)
    {
        double target_time = 1.0f / (double)clock->target_fps * 1000;

        if (tick_time - clock->last_tick < target_time)
        {
            uint32_t substraction = (uint32_t)tick_time - (uint32_t)clock->last_tick;
            usleep(((uint32_t)target_time - substraction) * 1000);
        }
    }
}

int
RenderThread_enqueueTextureJob(RenderThread *thread,
    Texture *texture, SDL_Surface *surface)
{
    pthread_mutex_lock(&thread->texture_job_mutex);

        if (thread->num_texture_jobs + 1 > thread->texture_job_queue_capacity)
        {
            DEBUG_PRINTF("NOTE: Maximum texture job queue reached on the render thread!\n");

            int new_size = (int)((float)thread->texture_job_queue_capacity * 1.10f);

            if (new_size <= thread->texture_job_queue_capacity)
                new_size = thread->texture_job_queue_capacity + 1;

            lockGlobalMallocMutex();

            void *new_jobs = malloc(new_size * sizeof(TextureJob));
            assert(new_jobs);

            if (!new_jobs)
            {
                DEBUG_PRINTF("Warning: failed to increase texture job queue size"
                    "- freeing bitmap.\n");
                SDL_FreeSurface(surface);
                unlockGlobalMallocMutex();
                pthread_mutex_unlock(&thread->texture_job_mutex);
                return 1;
            }

            memcpy(new_jobs, thread->texture_jobs, thread->num_texture_jobs * sizeof(TextureJob));
            free((void*)thread->texture_jobs);
            unlockGlobalMallocMutex();
            thread->texture_jobs = (TextureJob*)new_jobs;
            thread->texture_job_queue_capacity = new_size;
        }

        thread->texture_jobs[thread->num_texture_jobs].texture = texture;
        thread->texture_jobs[thread->num_texture_jobs].surface = surface;
        ++thread->num_texture_jobs;

    pthread_mutex_unlock(&thread->texture_job_mutex);
    return 0;
}

int
Game_init(Game *game)
{
    if (pthread_mutex_init(&GLOBAL_MALLOC_MUTEX, 0) != 0)
    {
        DEBUG_PRINTF("Init error: failed to initialize global malloc mutex.\n");
        return 1;
    }

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        DEBUG_PRINTF("SDL_Init() error: %s\n", SDL_GetError());
        return 2;
    }

    if (TTF_Init() == -1)
    {
        DEBUG_PRINTF("TTF_Init() error: %s\n", TTF_GetError());
        return 3;
    }

    if (IMG_Init(IMG_INIT_PNG) != (IMG_INIT_PNG))
    {
        DEBUG_PRINTF("IMG_Init error: %s\n", IMG_GetError());
        return 4;
    }

    /* Initialize audio */
	SDL_AudioInit("waveout");
	Mix_Init(0);
	Mix_VolumeMusic(20);

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) < 0)
	{
		DEBUG_PRINTF("Failed to open audio. Mix_OpenAudio() error: %s\n", Mix_GetError());
	}

    TaskQueue_create(&game->task_queue, 64);

	srand(time(0));

    game->clock.target_fps = FPS_LIMIT;

    /* Set touch finger states to 0 */
    for (int i = 0; i < 10; ++i)
    {
        game->input.fingers[i].finger_down  = 0;
        game->input.fingers[i].position     = {game->window.width / 2, game->window.height / 2};
        game->input.fingers[i].last_position= game->input.fingers[i].position;
    }

    SDL_DisplayMode display_mode;

    if (SDL_GetCurrentDisplayMode(0, &display_mode) == 0)
    {
        game->window.width  = display_mode.w;
        game->window.height = display_mode.h;
    }
    else
    {
        return 5;
    }

    game->window.resolution.x = RESOLUTION_WIDTH;
    game->window.resolution.y = RESOLUTION_HEIGHT;

    calculateWindowViewport(game->window.viewport, &game->window);

    game->window.sdl_window = SDL_CreateWindow("Rogues",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        game->window.width, game->window.height,
        SDL_WINDOW_SHOWN|SDL_WINDOW_OPENGL);

    if (!game->window.sdl_window)
    {
        DEBUG_PRINTF("SDL_CreateWindow() error: %s\n", SDL_GetError());
        return 6;
    }

    int task_queue_ret = TaskQueue_create(&game->render_thread.task_queue, 64);
    if (task_queue_ret != 0)
    {
        DEBUG_PRINTF("TaskQueue_create() error: %s\n", strerror(errno));
        return 7;
    }

    game->render_thread.num_texture_jobs = 0;
    game->render_thread.texture_jobs = (TextureJob*)malloc(TEXTURE_JOB_QUEUE_INITIAL_CAPACITY * sizeof(TextureJob));
    game->render_thread.texture_job_queue_capacity = TEXTURE_JOB_QUEUE_INITIAL_CAPACITY;

    if (pthread_mutex_init(&game->render_thread.texture_job_mutex, 0) != 0)
    {
        DEBUG_PRINTF("pthread_mutex_init() failure"
            "(for RenderThread.text_texture_job_mutex): %s\n",
            strerror(errno));
        return 8;
    }

    memset(&game->assets, 0, sizeof(Assets));

    if (Assets_loadFonts(&game->assets, &game->render_thread) != 0)
    {
        DEBUG_PRINTF("Assets_loadFonts(): There were errors loading fonts.\n");
    }

    DEBUG_PRINTF("Matching Stars: loaded fonts.");

    if (Assets_loadMusic(&game->assets) != 0)
    {
        DEBUG_PRINTF("Assets_loadMusic(): There were errors in loading music.\n");
    }

    DEBUG_PRINTF("Matching Stars: loaded music.");

    if (Assets_loadSoundClips(&game->assets) != 0)
    {
        DEBUG_PRINTF("Assets_loadSoundClips(): There were errors in loading sound clips.\n");
    }

    DEBUG_PRINTF("Matching Stars: loaded sound clips.");

    if (Assets_loadTextures(&game->render_thread, &game->assets) != 0)
    {
        DEBUG_PRINTF("Assets_loadTextures(): There were errors in loading textures.\n");
    }

    DEBUG_PRINTF("Matching Stars: loaded sound textures.");

    if (Assets_loadOnDemandTextures(&game->assets) != 0)
    {
        DEBUG_PRINTF("Assets_loadOnDemandTextures():"
            "There were errors in loading on-demand textures.\n");
    }

    DEBUG_PRINTF("Matching Stars: loaded on demand textures.");

    if (Assets_loadAnimations(&game->assets) != 0)
    {
        DEBUG_PRINTF("Assets_loadAnimations(): There were errors in loading animations.\n");
    }

    DEBUG_PRINTF("Matching Stars: loaded animations.");

    GameData_init(&game->game_data, game);

    DEBUG_PRINTF("Matching Stars: loaded game data.");

    Game_setScreen(game, SCREEN_MAIN_MENU);

    /* Initialize screens */
    if (MenuScreen_init(&game->menu_screen, game) != 0)
    {
        return 11;
    }

    #ifdef _DEBUG
        if (DebugScreen_init(&game->debug_screen, game) != 0)
        {
            return 12;
        }
    #endif

    if (ActiveGameScreen_init(&game->active_game_screen, game) != 0)
    {
        return 13;
    }

    SDL_SetEventFilter(Game_handleAndroidOnPause, game);

    return 0;
};

void
Game_uninit(Game *game)
{
    TaskQueue_destroy(&game->render_thread.task_queue);
    SDL_DestroyWindow(game->window.sdl_window);
}

void
Game_mainLoop(Game *game)
{
    game->running = 1;

#if _CF_SINGLE_THREADED
    Game_renderThreaded(game); /* Call anyway for GL initialization */
#else
    pthread_create(&game->render_thread.native_handle, 0, Game_renderThreaded, game);
#endif

    while (game->running)
    {
        Clock_tick(&game->clock);

        TaskQueue_processTasks(&game->task_queue);

        calculateWindowViewport(game->window.viewport, &game->window);

        for (int i = 0; i < NUM_TOUCH_FINGERS; ++i)
        {
            game->input.fingers[i].down_this_frame  = 0;
            game->input.fingers[i].up_this_frame    = 0;
        }

        SDL_Event event;

        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT:
                {
                    Game_shutDown(game);
                }
                    break;
                case SDL_WINDOWEVENT:
                {
                    if (event.window.event == SDL_WINDOWEVENT_MINIMIZED)
                    {
						Game_shutDown(game);
                    }
                }
                    break;
                case SDL_FINGERMOTION:
                {
                    unsigned int finger_index = event.tfinger.fingerId;

                    if (finger_index < 10)
                    {
                        game->input.fingers[finger_index].last_position = game->input.fingers[finger_index].position;
                        translateScreenCoordinatesByWindowViewport(
                            &game->input.fingers[finger_index].position.x,
                            &game->input.fingers[finger_index].position.y,
                            (int)(event.tfinger.x * (float)game->window.width),
                            (int)(event.tfinger.y * (float)game->window.height),
                            &game->window);
                    }
                }
                    break;
                case SDL_FINGERDOWN:
                {
                    unsigned int finger_index = event.tfinger.fingerId;
                    if (finger_index < 10)
                    {
                        game->input.fingers[finger_index].finger_down = 1;
                        game->input.fingers[finger_index].down_this_frame = 1;
                        game->input.fingers[finger_index].last_position = game->input.fingers[finger_index].position;
                        translateScreenCoordinatesByWindowViewport(
                            &game->input.fingers[finger_index].position.x,
                            &game->input.fingers[finger_index].position.y,
                            (int)(event.tfinger.x * (float)game->window.width),
                            (int)(event.tfinger.y * (float)game->window.height),
                            &game->window);
                        game->input.fingers[finger_index].touch_position = game->input.fingers[finger_index].position;

                    }
                }
                    break;
                case SDL_FINGERUP:
                {
                    unsigned int finger_index = event.tfinger.fingerId;

                    if (finger_index < 10)
                    {
                        game->input.fingers[finger_index].finger_down = 0;
                        game->input.fingers[finger_index].up_this_frame = 1;
                        game->input.fingers[finger_index].last_position = game->input.fingers[finger_index].position;
                        translateScreenCoordinatesByWindowViewport(&game->input.fingers[finger_index].position.x,
                            &game->input.fingers[finger_index].position.y,
                            (int)(event.tfinger.x * (float)game->window.width),
                            (int)(event.tfinger.y * (float)game->window.height),
                            &game->window);
                    }
                }
                    break;
                case SDL_KEYDOWN:
                {
                }
                   break;
            }

            switch (game->current_screen)
            {
                case SCREEN_MAIN_MENU:
                    MenuScreen_handleEvent(&game->menu_screen, game, &event);
                    break;
                #ifdef _DEBUG
                    case SCREEN_DEBUG:
                        DebugScreen_handleEvent(&game->debug_screen, game, &event);
                        break;
                #endif
                case SCREEN_ACTIVE_GAME:
                    ActiveGameScreen_handleEvent(&game->active_game_screen, game, &event);
                    break;
            }
        }

        /* update the current screen */
        switch (game->current_screen)
        {
            case SCREEN_MAIN_MENU:
                MenuScreen_update(&game->menu_screen, game);
                break;
            case SCREEN_ACTIVE_GAME:
                ActiveGameScreen_update(&game->active_game_screen, game);
				break;
            #ifdef _DEBUG
            case SCREEN_DEBUG:
                DebugScreen_update(&game->debug_screen, game);
                break;
            #endif
        }

#if _CF_SINGLE_THREADED
        Game_render(game);
#endif
    }

#if _CF_SINGLE_THREADED
#else
    pthread_detach(game->render_thread.native_handle);
#endif
}

void
Game_render(Game *game)
{
    Clock_tick(&game->render_thread.clock);

    /* Process any queued tasks for this thread */
    TaskQueue_processTasks(&game->render_thread.task_queue);

    if (game->render_thread.num_texture_jobs > 0)
    {
        pthread_mutex_lock(&game->render_thread.texture_job_mutex);

        for (int i = 0; i < game->render_thread.num_texture_jobs; ++i)
        {
            if (game->render_thread.texture_jobs[i].surface)
            {
                loadTextureAndFreePrevious(game->render_thread.texture_jobs[i].texture,
                    game->render_thread.texture_jobs[i].surface->pixels,
                    game->render_thread.texture_jobs[i].surface->w,
                    game->render_thread.texture_jobs[i].surface->h,
                    GL_RGBA, GL_REPEAT);

                if (TaskQueue_enqueueTask(&game->task_queue,
                    freeSurfaceJob,
                    game->render_thread.texture_jobs[i].surface) != 0)
                {
                    DEBUG_PRINTF("Warning: failed to enqueue free surface job to main thread."
                        "Attempting to free surface on the render thread instead...");
                    SDL_FreeSurface(game->render_thread.texture_jobs[i].surface);
                    freeTexture(game->render_thread.texture_jobs[i].texture);
                }
            }
            else
            {
                DEBUG_PRINTF("Note: surface passed to a texture to be loaded was 0.\n");
                freeTexture(game->render_thread.texture_jobs[i].texture);
            }
        }

        game->render_thread.num_texture_jobs = 0;

        pthread_mutex_unlock(&game->render_thread.texture_job_mutex);
    }

    /* Dispose of on demand textures if necessary */
    game->render_thread.on_demand_texture_dispose_timer += game->render_thread.clock.delta;

    if (game->render_thread.on_demand_texture_dispose_timer >= ON_DEMAND_TEXTURE_DISPOSE_INTERVAL)
    {
#ifdef _DEBUG
        uint num_released = 0;
#endif
        for (OnDemandTexture *odtex = &game->assets.on_demand_textures[0];
            odtex < &game->assets.on_demand_textures[NUM_ON_DEMAND_TEXTURE_ASSETS];
            ++odtex)
        {
            if ((uint)game->clock.last_tick - odtex->last_use_time >= ON_DEMAND_TEXTURE_PRESERVE_TIME
            &&  odtex->texture.id != 0)
            {
                freeTexture(&odtex->texture);

#ifdef _DEBUG
                ++num_released;
#endif
            }

        }

#ifdef _DEBUG
        if (num_released > 0)
            DEBUG_PRINTF("Freed %u on-demand textures.\n", num_released);
#endif

        game->render_thread.on_demand_texture_dispose_timer = 0;
    }

    switch (game->current_screen)
    {
        case SCREEN_MAIN_MENU:
            MenuScreen_render(&game->menu_screen, game, game->window.viewport);
            break;
        #ifdef _DEBUG
            case SCREEN_DEBUG:
                DebugScreen_render(&game->debug_screen, game, game->window.viewport);
                break;
        #endif
        case SCREEN_ACTIVE_GAME:
            ActiveGameScreen_render(&game->active_game_screen, game, game->window.viewport);
            break;
    }
}

void *
Game_renderThreaded(void *ptr)
{
    Game *game = (Game*)ptr;

	game->render_thread.clock.target_fps = FPS_LIMIT;
    game->render_thread.on_demand_texture_dispose_timer = 0.0f;

    /* Initialize OpenGL */
    game->gl_context = SDL_GL_CreateContext(game->window.sdl_window);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetSwapInterval(0);
    glEnable(GL_SCISSOR_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    /* Create the spritebatch */
    {
        GLchar log[512];
        log[0] = '\0';
        int err_num = SpriteBatch_init(&game->spritebatch, log, 512);

        if (err_num != 0)
        {
            DEBUG_PRINTF("SpriteBatch init error %i - GL error string: %s\n", err_num, log);
            Game_shutDown(game);
        }
    }

    SDL_Color white = {255, 255, 255, 255};
    loadTextureFromText(&game->assets.tex_loading_text,
        "Loading...",
        Assets_getFont(&game->assets, FONT_ELECTROLIZE),
        white, 0);

    glViewport(0, 0, game->window.width, game->window.height);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    SpriteBatch_drawSprite_Scale(&game->spritebatch,
        &game->assets.tex_loading_text,
        RESOLUTION_WIDTH  / 2 - (game->assets.tex_loading_text.width  * 2 / 2),
        RESOLUTION_HEIGHT / 2 - (game->assets.tex_loading_text.height * 2 / 2),
        0, 2.0f, 2.0f);
    SpriteBatch_flush(&game->spritebatch, game->window.viewport);

    SDL_GL_SwapWindow(game->window.sdl_window);

#if _CF_SINGLE_THREADED
#else

    while (game->running)
    {
        Game_render(game);
    }

    SpriteBatch_dispose(&game->spritebatch);
    SDL_GL_DeleteContext(game->gl_context);
#endif

    return 0;
}

int
Game_handleAndroidOnPause(void *data, SDL_Event *event)
{
#if _CF_SINGLE_THREADED
#else
    Game *game = (Game*)data;

    if (event->type == SDL_APP_WILLENTERBACKGROUND)
        pthread_kill(game->render_thread.native_handle, SIGKILL);
#endif

    return 1;
}

void
Game_setScreen(Game *game, GameScreen screen)
{
    game->current_screen = screen;

    switch (screen)
    {
        case SCREEN_MAIN_MENU:
            MenuScreen_onSetCurrent(&game->menu_screen, game);
            break;
        case SCREEN_ACTIVE_GAME:
            ActiveGameScreen_onSetCurrent(&game->active_game_screen, game);
            break;
        #ifdef _DEBUG
            case SCREEN_DEBUG:
                DebugScreen_onSetCurrent(&game->debug_screen, game);
                break;
        #endif
    }
}
void
Game_shutDown(Game* game)
{
	game->running = 0;
	pthread_kill(game->render_thread.native_handle, SIGKILL);
}

void
calculateWindowViewport(int *viewport, Window *window)
{
    viewport[0] = 0;
    viewport[1] = 1;
    viewport[2] = window->resolution.x;
    viewport[3] = window->resolution.y;
}

int
Assets_loadFonts(Assets *assets, RenderThread *thread)
{
    int num_errors = 0;

    for (uint i = 0; i < NUM_FONT_ASSETS; ++i)
    {
        assets->fonts[i] = TTF_OpenFont(FONT_ASSETS[i].path, FONT_ASSETS[i].size);

        if (!assets->fonts[i])
        {
            DEBUG_PRINTF("Failed to load a font of size %i from path %s\n", i, FONT_ASSETS[i].path);
            ++num_errors;
        }
    }

    return num_errors;
}

int
Assets_loadTextures(RenderThread *render_thread, Assets *assets)
{
    int num_errors = 0;

    for (uint i = 0; i < NUM_TEXTURE_ASSETS; ++i)
    {
        if (loadTextureFromFileOnAnyThread(render_thread, &assets->textures[i], TEXTURE_PATHS[i]) != 0)
        {
            DEBUG_PRINTF("Note: Failed to load texture from path %s\n", TEXTURE_PATHS[i]);
            loadTextureFromFileOnAnyThread(render_thread, &assets->textures[i], PLACEHOLDER_TEXTURE_PATH);
            ++num_errors;
        }
        else
        {
            DEBUG_PRINTF("Loaded texture no. %i from %s!\n", i, TEXTURE_PATHS[i]);
        }
    }

    return num_errors;
}

int
Assets_loadMusic(Assets *assets)
{
    int num_errors = 0;

    lockGlobalMallocMutex();

    for (uint i = 0; i < NUM_MUSIC_ASSETS; ++i)
    {
        assets->music_assets[i] = Mix_LoadMUS(MUSIC_PATHS[i]);

        if (!assets->music_assets[i])
        {
            DEBUG_PRINTF("Mix_LoadMUS() error (file path: %s): %s\n",
                MUSIC_PATHS[i], Mix_GetError());
            ++num_errors;
        }
    }

    unlockGlobalMallocMutex();

    assets->true_theme_song = 0;

    return num_errors;
}

int
Assets_loadSoundClips(Assets *assets)
{
    int num_errors = 0;

    lockGlobalMallocMutex();

    for (uint i = 0; i < NUM_SOUND_CLIPS; ++i)
    {
        assets->sound_clips[i] = Mix_LoadWAV(SOUND_CLIP_PATHS[i]);

        if (!assets->sound_clips[i])
        {
            DEBUG_PRINTF("Mix_LoadWAV() error (file path: %s): %s\n",
                SOUND_CLIP_PATHS[i], Mix_GetError());
            ++num_errors;
        }
    }

    unlockGlobalMallocMutex();

    Mix_VolumeChunk(*Assets_getSoundClip(assets, SO_MARK_PRESS), MIX_MAX_VOLUME);
    Mix_VolumeChunk(*Assets_getSoundClip(assets, SO_SHIELD1), MIX_MAX_VOLUME);

    return num_errors;
}

int
Assets_loadAnimations(Assets *assets)
{
    {
        AnimationFrame frames[20];
        frames->texture = Assets_getTexture(assets, TEX_CAPITAL_SHIP);
        frames->clip[0] = 0;
        frames->clip[1] = 0;
        frames->clip[2] = frames->texture->width;
        frames->clip[3] = frames->texture->height;
        frames->duration = 0.15f;
        frames->offset.x = 0;
        frames->offset.y = 0;
        frames->flip = SPRITE_FLIP_NONE;
        frames->angle = 0.0f;
        frames->scale = {0.4f, 0.4f};

        for (AnimationFrame *f = &frames[1]; f < &frames[20]; ++f)
            *f = *frames;

        for (AnimationFrame *f = &frames[1]; f < &frames[10]; ++f)
            f->duration = 0.075f;

        frames[1].offset.y = frames->offset.y + 1;
        frames[2].offset.y = frames->offset.y + 2;
        frames[3].offset.y = frames->offset.y + 3;
        frames[4].offset.y = frames->offset.y + 4;
        frames[5].offset.y = frames->offset.y + 5;
        frames[6].offset.y = frames->offset.y + 6;
        frames[7].offset.y = frames->offset.y + 7;
        frames[8].offset.y = frames->offset.y + 8;
        frames[9].offset.y = frames->offset.y + 9;
        frames[10].offset.y = frames->offset.y + 10;
        frames[11].offset.y = frames->offset.y + 9;
        frames[12].offset.y = frames->offset.y + 8;
        frames[13].offset.y = frames->offset.y + 7;
        frames[14].offset.y = frames->offset.y + 6;
        frames[15].offset.y = frames->offset.y + 5;
        frames[16].offset.y = frames->offset.y + 4;
        frames[17].offset.y = frames->offset.y + 3;
        frames[18].offset.y = frames->offset.y + 2;
        frames[19].offset.y = frames->offset.y + 1;

        Animation_create(&assets->animation1, frames, 20);
    }

    {
        AnimationFrame frames[15];
        frames->texture = Assets_getTexture(assets, TEX_FIRST_BOSS);
        frames->clip[0] = 0;
        frames->clip[1] = 0;
        frames->clip[2] = frames->texture->width;
        frames->clip[3] = frames->texture->height;
        frames->duration = 0.15f;
        frames->offset.x = -100;
        frames->offset.y = 0;
        frames->flip = SPRITE_FLIP_NONE;
        frames->angle = 0.0f;
        frames->scale = { 1.0f, 1.0f };

        for (AnimationFrame *f = &frames[1]; f < &frames[15]; ++f)
            *f = *frames;

        frames[1].offset.y = frames->offset.y + 2;
        frames[2].offset.y = frames->offset.y + 4;
        frames[3].offset.y = frames->offset.y + 6;
        frames[4].offset.y = frames->offset.y + 8;
        frames[5].offset.y = frames->offset.y + 10;
        frames[6].offset.y = frames->offset.y + 9;
        frames[7].offset.y = frames->offset.y + 8;
        frames[8].offset.y = frames->offset.y + 7;
        frames[9].offset.y = frames->offset.y + 6;
        frames[10].offset.y = frames->offset.y + 5;
        frames[11].offset.y = frames->offset.y + 4;
        frames[12].offset.y = frames->offset.y + 3;
        frames[13].offset.y = frames->offset.y + 2;
        frames[14].offset.y = frames->offset.y + 1;

        Animation_create(&assets->boss1_animation, frames, 15);
    }

    {
        AnimationFrame frames[15];
        frames->texture = Assets_getTexture(assets, TEX_SECOND_BOSS);
        frames->clip[0] = 0;
        frames->clip[1] = 0;
        frames->clip[2] = frames->texture->width;
        frames->clip[3] = frames->texture->height;
        frames->duration = 0.15f;
        frames->offset.x = -100;
        frames->offset.y = 0;
        frames->flip = SPRITE_FLIP_NONE;
        frames->angle = 0.0f;
        frames->scale = { 1.0f, 1.0f };

        for (AnimationFrame *f = &frames[1]; f < &frames[15]; ++f)
            *f = *frames;

        frames[1].offset.y = frames->offset.y + 2;
        frames[2].offset.y = frames->offset.y + 4;
        frames[3].offset.y = frames->offset.y + 6;
        frames[4].offset.y = frames->offset.y + 8;
        frames[5].offset.y = frames->offset.y + 10;
        frames[6].offset.y = frames->offset.y + 9;
        frames[7].offset.y = frames->offset.y + 8;
        frames[8].offset.y = frames->offset.y + 7;
        frames[9].offset.y = frames->offset.y + 6;
        frames[10].offset.y = frames->offset.y + 5;
        frames[11].offset.y = frames->offset.y + 4;
        frames[12].offset.y = frames->offset.y + 3;
        frames[13].offset.y = frames->offset.y + 2;
        frames[14].offset.y = frames->offset.y + 1;

        Animation_create(&assets->boss2_animation, frames, 15);
    }

    {
        AnimationFrame frames[15];
        frames->texture = Assets_getTexture(assets, TEX_FINAL_BOSS);
        frames->clip[0] = 0;
        frames->clip[1] = 0;
        frames->clip[2] = frames->texture->width;
        frames->clip[3] = frames->texture->height;
        frames->duration = 0.15f;
        frames->offset.x = -100;
        frames->offset.y = -200;
        frames->flip = SPRITE_FLIP_NONE;
        frames->angle = 0.0f;
        frames->scale = { 0.8f, 0.8f };

        for (AnimationFrame *f = &frames[1]; f < &frames[15]; ++f)
            *f = *frames;

        frames[1].offset.y = frames->offset.y + 2;
        frames[2].offset.y = frames->offset.y + 4;
        frames[3].offset.y = frames->offset.y + 6;
        frames[4].offset.y = frames->offset.y + 8;
        frames[5].offset.y = frames->offset.y + 10;
        frames[6].offset.y = frames->offset.y + 9;
        frames[7].offset.y = frames->offset.y + 8;
        frames[8].offset.y = frames->offset.y + 7;
        frames[9].offset.y = frames->offset.y + 6;
        frames[10].offset.y = frames->offset.y + 5;
        frames[11].offset.y = frames->offset.y + 4;
        frames[12].offset.y = frames->offset.y + 3;
        frames[13].offset.y = frames->offset.y + 2;
        frames[14].offset.y = frames->offset.y + 1;

        Animation_create(&assets->finalboss_animation, frames, 15);
    }

	Animation_createFromSheet(&assets->explosion_animation,
        Assets_getTexture(assets, TEX_EXPLOSION),
        164, 164, 0.05f, 0, 0, 9, 1);

    Animation_createFromSheet(&assets->shield_animation,
        Assets_getTexture(assets, TEX_PROGRESS_BAR1_PH),
        1080, 102, 0.1, 0, 152, 9, 1);

    Animation_createFromSheet(&assets->small_rocket_animation,

        Assets_getTexture(assets, TEX_ROCKET),
        110, 28, 0.1f, 0, 0, 5, 5);

    Animation_createFromSheet(&assets->medium_rocket_animation,
        Assets_getTexture(assets, TEX_ROCKET),
        110, 28, 0.1f, 0, 28, 5, 5);

    Animation_createFromSheet(&assets->large_rocket_animation,
        Assets_getTexture(assets, TEX_ROCKET),
        110, 28, 0.1f, 0, 56, 5, 5);

    Animation_createFromSinglePicture(&assets->small_bullet_animation,
        Assets_getTexture(assets, TEX_BULLET),
        70, 14, 0.1f, 0, 32, 1);

    Animation_createFromSinglePicture(&assets->medium_bullet_animation,
        Assets_getTexture(assets, TEX_BULLET),
        148, 15, 0.1f, 0, 17, 1);

    Animation_createFromSinglePicture(&assets->large_bullet_animation,
        Assets_getTexture(assets, TEX_BULLET),
        352, 17, 0.1f, 0, 0, 1);

	Animation_createFromSinglePicture(&assets->target_animation,
        Assets_getTexture(assets, TEX_TARGET),
        66, 66, 0.1f, 0, 0, 2);

	Animation_createFromSheet(&assets->small_laser_animation,
        Assets_getTexture(assets, TEX_LASER),
        400, 268, 0.1f, 0, 0, 12, 5);

    Animation_createFromSheet(&assets->medium_laser_animation,
        Assets_getTexture(assets, TEX_LASER),
        400, 268, 0.1f, 0, 804, 13, 5);

    Animation_createFromSheet(&assets->large_laser_animation,
        Assets_getTexture(assets, TEX_LASER),
        400, 268, 0.1f, 0, 1608, 15, 5);

    Animation_createFromSheet(&assets->gameover_animation,
        Assets_getTexture(assets, TEX_GAMEOVER_ANIMATION),
        960, 440, 0.2f, 0, 0, 6, 3);

    Animation_createFromSheet(&assets->victory_animation,
        Assets_getTexture(assets, TEX_VICTORY_ANIMATION),
        1020, 240, 0.1f, 0, 0, 25, 3);

    Animation_createFromSheet(&assets->start_screen_animation,
        Assets_getTexture(assets, TEX_START_SCREEN_ANIMATION),
        540, 960, 0.15f, 0, 0, 12, 4);

    Animation_createFromSheet(&assets->shockwave_animation,
        Assets_getTexture(assets, TEX_SHOCKWAVE),
        200, 620, 0.1f, 0, 0, 6, 1);

    Animation_createFromSheet(&assets->antimatter_animation,
        Assets_getTexture(assets, TEX_ANTIMATTER_EXPLOSION),
        471, 471, 0.1f, 0, 0, 18, 6);

    {
        AnimationFrame frames[15];
        frames->texture = Assets_getTexture(assets, TEX_MARAUDERS);
        frames->clip[0] = 388;
        frames->clip[1] = 0;
        frames->clip[2] = 92;
        frames->clip[3] = 32;
        frames->duration = 0.15f;
        frames->offset.x = 28;
        frames->offset.y = 48;
        frames->flip = SPRITE_FLIP_NONE;
        frames->angle = 0.0f;
        frames->scale = {1.0f, 1.0f};

        for (AnimationFrame *f = &frames[1]; f < &frames[15]; ++f)
            *f = *frames;

        frames[1].offset.y = frames->offset.y + 2;
        frames[2].offset.y = frames->offset.y + 4;
        frames[3].offset.y = frames->offset.y + 6;
        frames[4].offset.y = frames->offset.y + 8;
        frames[5].offset.y = frames->offset.y + 10;
        frames[6].offset.y = frames->offset.y + 9;
        frames[7].offset.y = frames->offset.y + 8;
        frames[8].offset.y = frames->offset.y + 7;
        frames[9].offset.y = frames->offset.y + 6;
        frames[10].offset.y = frames->offset.y + 5;
        frames[11].offset.y = frames->offset.y + 4;
        frames[12].offset.y = frames->offset.y + 3;
        frames[13].offset.y = frames->offset.y + 2;
        frames[14].offset.y = frames->offset.y + 1;

        Animation_create(&assets->animation2, frames, 15);
    }

    {
        AnimationFrame frames[1];
        frames->texture = Assets_getTexture(assets, TEX_MARAUDERS);
        frames->clip[0] = 230;
        frames->clip[1] = 0;
        frames->clip[2] = 158;
        frames->clip[3] = 60;
        frames->duration = 1.0f;
        frames->offset.x = -40;
        frames->offset.y = 16;
        frames->flip = SPRITE_FLIP_NONE;
        frames->angle = 0.0f;
        frames->scale = {1.0f, 1.0f};

        Animation_create(&assets->mediumenemy_animation, frames, 1);
    }

    {
        AnimationFrame frames[15];
        frames->texture = Assets_getTexture(assets, TEX_MARAUDERS);
        frames->clip[0] = 0;
        frames->clip[1] = 0;
        frames->clip[2] = 230;
        frames->clip[3] = 82;
        frames->duration = 1.0f;
        frames->offset.x = -40;
        frames->offset.y = 16;
        frames->flip = SPRITE_FLIP_NONE;
        frames->angle = 0.0f;
        frames->scale = {1.0f, 1.0f};

        for (AnimationFrame *f = &frames[1]; f < &frames[15]; ++f)
            *f = *frames;

        frames[1].offset.y = frames->offset.y + 2;
        frames[2].offset.y = frames->offset.y + 4;
        frames[3].offset.y = frames->offset.y + 6;
        frames[4].offset.y = frames->offset.y + 8;
        frames[5].offset.y = frames->offset.y + 10;
        frames[6].offset.y = frames->offset.y + 9;
        frames[7].offset.y = frames->offset.y + 8;
        frames[8].offset.y = frames->offset.y + 7;
        frames[9].offset.y = frames->offset.y + 6;
        frames[10].offset.y = frames->offset.y + 5;
        frames[11].offset.y = frames->offset.y + 4;
        frames[12].offset.y = frames->offset.y + 3;
        frames[13].offset.y = frames->offset.y + 2;
        frames[14].offset.y = frames->offset.y + 1;

        Animation_create(&assets->largeenemy_animation, frames, 15);
    }


    /* Large white enemy */
    {
        static AnimationFrame frames[14] = {};
        frames[0].texture = Assets_getTexture(assets, TEX_ENEMIES2);
        frames[0].clip[0] = 0;
        frames[0].clip[1] = 0;
        frames[0].clip[2] = 213;
        frames[0].clip[3] = 92;
        frames[0].duration = 0.18f;
        frames[0].flip = SPRITE_FLIP_NONE;
        frames[0].scale.x = 0.8f;
        frames[0].scale.y = 0.8f;
        frames[0].offset.x = -52;
        frames[0].offset.y = 0;

        for (AnimationFrame *f = &frames[1]; f < &frames[14]; ++f)
            *f = *frames;

        frames[1].offset.y = frames->offset.y + 1;
        frames[2].offset.y = frames->offset.y + 2;
        frames[3].offset.y = frames->offset.y + 3;
        frames[4].offset.y = frames->offset.y + 4;
        frames[5].offset.y = frames->offset.y + 5;
        frames[6].offset.y = frames->offset.y + 6;
        frames[7].offset.y = frames->offset.y + 7;
        frames[8].offset.y = frames->offset.y + 6;
        frames[9].offset.y = frames->offset.y + 5;
        frames[10].offset.y = frames->offset.y + 4;
        frames[11].offset.y = frames->offset.y + 3;
        frames[12].offset.y = frames->offset.y + 2;
        frames[13].offset.y = frames->offset.y + 1;

        assets->an_enemy_white_large.frames = frames;
        assets->an_enemy_white_large.num_frames = 14;
        assets->an_enemy_white_large.total_duration = frames[0].duration * 14.0f;
    }

    /* Medium white enemy */
    {
        static AnimationFrame frames[15] = {};
        frames[0].texture = Assets_getTexture(assets, TEX_ENEMIES2);
        frames[0].clip[0] = 213;
        frames[0].clip[1] = 14;
        frames[0].clip[2] = 165;
        frames[0].clip[3] = 64;
        frames[0].duration = 0.17f;
        frames[0].flip = SPRITE_FLIP_NONE;
        frames[0].scale.x = 1.0f;
        frames[0].scale.y = 1.0f;
        frames[0].offset.x = -45;
        frames[0].offset.y = 18;

        for (AnimationFrame *f = &frames[1]; f < &frames[15]; ++f)
            *f = *frames;

        frames[1].offset.y = frames->offset.y + 2;
        frames[2].offset.y = frames->offset.y + 4;
        frames[3].offset.y = frames->offset.y + 6;
        frames[4].offset.y = frames->offset.y + 8;
        frames[5].offset.y = frames->offset.y + 10;
        frames[6].offset.y = frames->offset.y + 9;
        frames[7].offset.y = frames->offset.y + 8;
        frames[8].offset.y = frames->offset.y + 7;
        frames[9].offset.y = frames->offset.y + 6;
        frames[10].offset.y = frames->offset.y + 5;
        frames[11].offset.y = frames->offset.y + 4;
        frames[12].offset.y = frames->offset.y + 3;
        frames[13].offset.y = frames->offset.y + 2;
        frames[14].offset.y = frames->offset.y + 1;

        assets->an_enemy_white_medium.frames = frames;
        assets->an_enemy_white_medium.num_frames = 15;
        assets->an_enemy_white_medium.total_duration = frames->duration * 15.0f;
    }

    /* Small white enemy */
    {
        static AnimationFrame frames[15] = {};
        frames->texture = Assets_getTexture(assets, TEX_ENEMIES2);
        frames->clip[0] = 378;
        frames->clip[1] = 20;
        frames->clip[2] = 100;
        frames->clip[3] = 50;
        frames->duration = 0.17f;
        frames->flip = SPRITE_FLIP_NONE;
        frames->scale.x = 1.0f;
        frames->scale.y = 1.0f;
        frames->offset.y = 28;

        assets->an_enemy_white_small.frames = frames;
        assets->an_enemy_white_small.num_frames = 10;
        assets->an_enemy_white_small.total_duration = 15.0f * frames->duration;

        for (AnimationFrame *f = &frames[1]; f < &frames[10]; ++f)
            *f = *frames;

        frames[1].offset.y = frames->offset.y + 1;
        frames[2].offset.y = frames->offset.y + 2;
        frames[3].offset.y = frames->offset.y + 3;
        frames[4].offset.y = frames->offset.y + 4;
        frames[5].offset.y = frames->offset.y + 5;
        frames[6].offset.y = frames->offset.y + 4;
        frames[7].offset.y = frames->offset.y + 3;
        frames[8].offset.y = frames->offset.y + 2;
        frames[9].offset.y = frames->offset.y + 1;
    }

    /* Mark animations */
    {
        /* Normal single frame animations' frames */
        for (AnimationFrame *frame = assets->mark_normal_frames;
            frame < &assets->mark_normal_frames[NUM_MARK_TYPES];
            ++frame)
        {
            frame->texture  = Assets_getTexture(assets, TEX_MARKS);
            frame->duration = 1;
            frame->offset.x = 0;
            frame->offset.y = 0;
            frame->flip     = SPRITE_FLIP_NONE;
            frame->angle    = 0.0f;
            frame->scale.x  = 1.0f;
            frame->scale.y  = 1.0f;
        }

        assets->mark_normal_frames[MARK_BALLISTIC].clip[0] = 0;
        assets->mark_normal_frames[MARK_BALLISTIC].clip[1] = 0;
        assets->mark_normal_frames[MARK_BALLISTIC].clip[2] = 79;
        assets->mark_normal_frames[MARK_BALLISTIC].clip[3] = 79;

        assets->mark_normal_frames[MARK_SHIELD].clip[0] = 0;
        assets->mark_normal_frames[MARK_SHIELD].clip[1] = 4*79;
        assets->mark_normal_frames[MARK_SHIELD].clip[2] = 79;
        assets->mark_normal_frames[MARK_SHIELD].clip[3] = 79;

        assets->mark_normal_frames[MARK_REPAIR].clip[0] = 0;
        assets->mark_normal_frames[MARK_REPAIR].clip[1] = 3*79;
        assets->mark_normal_frames[MARK_REPAIR].clip[2] = 79;
        assets->mark_normal_frames[MARK_REPAIR].clip[3] = 79;

        assets->mark_normal_frames[MARK_ROCKET].clip[0] = 0;
        assets->mark_normal_frames[MARK_ROCKET].clip[1] = 2*79;
        assets->mark_normal_frames[MARK_ROCKET].clip[2] = 79;
        assets->mark_normal_frames[MARK_ROCKET].clip[3] = 79;

        assets->mark_normal_frames[MARK_LASER].clip[0] = 0;
        assets->mark_normal_frames[MARK_LASER].clip[1] = 79;
        assets->mark_normal_frames[MARK_LASER].clip[2] = 79;
        assets->mark_normal_frames[MARK_LASER].clip[3] = 79;

        {
            Animation *a;
            for (int i = 0; i < NUM_MARK_TYPES; ++i)
            {
                a = &assets->an_marks_normal[i];
                a->frames = &assets->mark_normal_frames[i];
                a->num_frames = 1;
                a->total_duration = assets->mark_normal_frames[i].duration;
            }
        }

        /* Small combo animations */
        AnimationFrame *frames = assets->mark_combo_small_frames;
        #define MARK_COMBO_SMALL_ANIM_FRAME_DUR ((float)COMBO_SHOWOFF_DURATION / (float)NUM_MARK_COMBO_SMALL_FRAMES)

        uint clip_x;
        for (int i = 0; i < NUM_MARK_COMBO_SMALL_FRAMES; ++i)
        {
            clip_x = i * 79;

            AnimationFrame_create(&frames[MARK_BALLISTIC * NUM_MARK_COMBO_SMALL_FRAMES + i],
                Assets_getTexture(assets, TEX_MARKS),
                clip_x, 0, 79, 79,
                MARK_COMBO_SMALL_ANIM_FRAME_DUR,
                0, 0,
                SPRITE_FLIP_NONE,
                0.0f,
                1.0f, 1.0f);

            AnimationFrame_create(&frames[MARK_SHIELD * NUM_MARK_COMBO_SMALL_FRAMES + i],
                Assets_getTexture(assets, TEX_MARKS),
                clip_x, 4*79, 79, 79,
                MARK_COMBO_SMALL_ANIM_FRAME_DUR,
                0, 0,
                SPRITE_FLIP_NONE,
                0.0f,
                1.0f, 1.0f);

            AnimationFrame_create(&frames[MARK_REPAIR * NUM_MARK_COMBO_SMALL_FRAMES + i],
                Assets_getTexture(assets, TEX_MARKS),
                clip_x, 3*79, 79, 79,
                MARK_COMBO_SMALL_ANIM_FRAME_DUR,
                0, 0,
                SPRITE_FLIP_NONE,
                0.0f,
                1.0f, 1.0f);

            AnimationFrame_create(&frames[MARK_ROCKET * NUM_MARK_COMBO_SMALL_FRAMES + i],
                Assets_getTexture(assets, TEX_MARKS),
                clip_x, 2*79, 79, 79,
                MARK_COMBO_SMALL_ANIM_FRAME_DUR,
                0, 0,
                SPRITE_FLIP_NONE,
                0.0f,
                1.0f, 1.0f);

            AnimationFrame_create(&frames[MARK_LASER * NUM_MARK_COMBO_SMALL_FRAMES + i],
                Assets_getTexture(assets, TEX_MARKS),
                clip_x, 79, 79, 79,
                MARK_COMBO_SMALL_ANIM_FRAME_DUR,
                0, 0,
                SPRITE_FLIP_NONE,
                0.0f,
                1.0f, 1.0f);
        }

        Animation *a;
        for (int i = 0; i < NUM_MARK_TYPES; ++i)
        {
            a = &assets->an_mark_combo_small[i];
            a->frames = &assets->mark_combo_small_frames[i * NUM_MARK_COMBO_SMALL_FRAMES];
            a->total_duration = (float)NUM_MARK_COMBO_SMALL_FRAMES * MARK_COMBO_SMALL_ANIM_FRAME_DUR;
            a->num_frames = NUM_MARK_COMBO_SMALL_FRAMES;
        }

        /* Large combo animations */
        #define MARK_COMBO_LARGE_ANIM_FRAME_DUR ((float)COMBO_SHOWOFF_DURATION / (float)NUM_MARK_COMBO_LARGE_FRAMES)

        frames = assets->mark_combo_large_frames;
        clip_x = 0;

        /* First create the index 0 frames since they're separate on the sheet */
        AnimationFrame_create(&frames[NUM_MARK_COMBO_LARGE_FRAMES * MARK_BALLISTIC],
            Assets_getTexture(assets, TEX_MARKS),
            clip_x, 0, 79, 79,
            MARK_COMBO_LARGE_ANIM_FRAME_DUR,
            0, 0,
            SPRITE_FLIP_NONE,
            0.0f,
            1.0f, 1.0f);

        AnimationFrame_create(&frames[NUM_MARK_COMBO_LARGE_FRAMES * MARK_SHIELD],
            Assets_getTexture(assets, TEX_MARKS),
            clip_x, 4*79, 79, 79,
            MARK_COMBO_LARGE_ANIM_FRAME_DUR,
            0, 0,
            SPRITE_FLIP_NONE,
            0.0f,
            1.0f, 1.0f);

        AnimationFrame_create(&frames[NUM_MARK_COMBO_LARGE_FRAMES * MARK_REPAIR],
            Assets_getTexture(assets, TEX_MARKS),
            clip_x, 3*79, 79, 79,
            MARK_COMBO_LARGE_ANIM_FRAME_DUR,
            0, 0,
            SPRITE_FLIP_NONE,
            0.0f,
            1.0f, 1.0f);

        AnimationFrame_create(&frames[NUM_MARK_COMBO_LARGE_FRAMES * MARK_ROCKET],
            Assets_getTexture(assets, TEX_MARKS),
            clip_x, 2*79, 79, 79,
            MARK_COMBO_LARGE_ANIM_FRAME_DUR,
            0, 0,
            SPRITE_FLIP_NONE,
            0.0f,
            1.0f, 1.0f);

        AnimationFrame_create(&frames[NUM_MARK_COMBO_LARGE_FRAMES * MARK_LASER],
            Assets_getTexture(assets, TEX_MARKS),
            clip_x, 79, 79, 79,
            MARK_COMBO_LARGE_ANIM_FRAME_DUR,
            0, 0,
            SPRITE_FLIP_NONE,
            0.0f,
            1.0f, 1.0f);

        for (int i = 1; i < NUM_MARK_COMBO_LARGE_FRAMES; ++i)
        {
            clip_x = 7 * 79 + (i - 1) * 79;

            AnimationFrame_create(&frames[NUM_MARK_COMBO_LARGE_FRAMES * MARK_BALLISTIC + i],
                Assets_getTexture(assets, TEX_MARKS),
                clip_x, 0, 79, 79,
                MARK_COMBO_LARGE_ANIM_FRAME_DUR,
                0, 0,
                SPRITE_FLIP_NONE,
                0.0f,
                1.0f, 1.0f);

            AnimationFrame_create(&frames[NUM_MARK_COMBO_LARGE_FRAMES * MARK_SHIELD + i],
                Assets_getTexture(assets, TEX_MARKS),
                clip_x, 4*79, 79, 79,
                MARK_COMBO_LARGE_ANIM_FRAME_DUR,
                0, 0,
                SPRITE_FLIP_NONE,
                0.0f,
                1.0f, 1.0f);

            AnimationFrame_create(&frames[NUM_MARK_COMBO_LARGE_FRAMES * MARK_REPAIR + i],
                Assets_getTexture(assets, TEX_MARKS),
                clip_x, 3*79, 79, 79,
                MARK_COMBO_LARGE_ANIM_FRAME_DUR,
                0, 0,
                SPRITE_FLIP_NONE,
                0.0f,
                1.0f, 1.0f);

            AnimationFrame_create(&frames[NUM_MARK_COMBO_LARGE_FRAMES * MARK_ROCKET + i],
                Assets_getTexture(assets, TEX_MARKS),
                clip_x, 2*79, 79, 79,
                MARK_COMBO_LARGE_ANIM_FRAME_DUR,
                0, 0,
                SPRITE_FLIP_NONE,
                0.0f,
                1.0f, 1.0f);

            AnimationFrame_create(&frames[NUM_MARK_COMBO_LARGE_FRAMES * MARK_LASER + i],
                Assets_getTexture(assets, TEX_MARKS),
                clip_x, 79, 79, 79,
                MARK_COMBO_LARGE_ANIM_FRAME_DUR,
                0, 0,
                SPRITE_FLIP_NONE,
                0.0f,
                1.0f, 1.0f);
        }

        for (int i = 0; i < NUM_MARK_TYPES; ++i)
        {
            a = &assets->an_mark_combo_large[i];
            a->frames = &assets->mark_combo_large_frames[i * NUM_MARK_COMBO_LARGE_FRAMES];
            a->total_duration = (float)NUM_MARK_COMBO_LARGE_FRAMES * MARK_COMBO_LARGE_ANIM_FRAME_DUR;
            a->num_frames = NUM_MARK_COMBO_LARGE_FRAMES;
        }
    }

    assets->health_animation = assets->shield_animation;

    assets->health_animation.frames = assets->health_frames;

    for (int i = assets->shield_animation.num_frames; i > 0; --i)
    {
        assets->health_frames[assets->shield_animation.num_frames - i] = assets->shield_animation.frames[i-1];
    }

    return 0;
}

int
Assets_loadOnDemandTextures(Assets *assets)
{
    int num_errors = 0;

    lockGlobalMallocMutex();
    for (uint i = 0; i < NUM_ON_DEMAND_TEXTURE_ASSETS; ++i)
    {
        assets->on_demand_textures[i].bitmap = IMG_Load(ON_DEMAND_TEXTURE_PATHS[i]);

        if (!assets->on_demand_textures[i].bitmap)
        {
            DEBUG_PRINTF("Failed to load OnDemandTexture bitmap from path %s: %s. Loading placeholder instead.\n",
                ON_DEMAND_TEXTURE_PATHS[i], IMG_GetError());
            assets->on_demand_textures[i].bitmap = IMG_Load(PLACEHOLDER_TEXTURE_PATH);
            ++num_errors;
        }

        assets->on_demand_textures[i].texture.id = 0;
        assets->on_demand_textures[i].texture.width  = assets->on_demand_textures[i].bitmap->w;
        assets->on_demand_textures[i].texture.height = assets->on_demand_textures[i].bitmap->h;
    }
    unlockGlobalMallocMutex();

    return num_errors;
}

void
onDemandTextureLoadJob(void *args)
{
    OnDemandTexture *od_tex = (OnDemandTexture*)args;

    loadTextureAndFreePrevious(&od_tex->texture,
        od_tex->bitmap->pixels,
        od_tex->bitmap->w,
        od_tex->bitmap->h,
        GL_RGBA, GL_REPEAT);

    od_tex->loading = 0;
}

void
GameData_init(GameData *data, Game *game)
{
    memset(&game->game_data, 0, sizeof(GameData));

    data->enemy_types[SMALL_ENEMY_INDEX] = EnemyType_create("Small enemy", 140,
        &game->assets.animation2, 1, ENEMY_BALLISTIC_ATTACK, 150, 1);

    data->enemy_types[MEDIUM_ENEMY_INDEX] = EnemyType_create("Medium enemy", 290,
        &game->assets.mediumenemy_animation, 2, ENEMY_BALLISTIC_ATTACK, 150, 2);

    data->enemy_types[LARGE_ENEMY_INDEX] = EnemyType_create("Large enemy", 390,
        &game->assets.largeenemy_animation, 3, ENEMY_MISSILE_ATTACK, 150, 1);

    data->enemy_types[TEST_ENEMY_INDEX] = EnemyType_create("Test enemy", 310,
        &game->assets.largeenemy_animation, 1, ENEMY_MISSILE_ATTACK, 150, 2);

    data->enemy_types[FIRST_BOSS_INDEX] = EnemyType_create("First Boss", 500,
        &game->assets.boss1_animation, 2, ENEMY_BALLISTIC_ATTACK, 250, 3);

    data->enemy_types[SECOND_BOSS_INDEX] = EnemyType_create("Second Boss", 750,
        &game->assets.boss2_animation, 2, ENEMY_BALLISTIC_ATTACK, 250, 2);

    data->enemy_types[THIRD_BOSS_INDEX] = EnemyType_create("Third Boss", 1000,
        &game->assets.finalboss_animation, 1, ENEMY_BALLISTIC_ATTACK, 250, 3);

    /* Second mission */
    data->enemy_types[SMALL_WHITE_ENEMY_INDEX] = EnemyType_create("Small White Enemy", 160,
        &game->assets.an_enemy_white_small, 1, ENEMY_LASER_ATTACK, 150, 1);

    data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX] = EnemyType_create("Medium White Enemy", 320,
        &game->assets.an_enemy_white_medium , 3, ENEMY_LASER_ATTACK, 150, 3);

    data->enemy_types[LARGE_WHITE_ENEMY_INDEX] = EnemyType_create("Large White Enemy", 410,
        &game->assets.an_enemy_white_large, 3, ENEMY_LASER_ATTACK, 150, 1);

    MissionType *mission;

    /* Mission 1 */
    {
        mission = &data->mission_types[MISSION_1_INDEX];

        mission->points             = 10;
        mission->num_waves          = 3;

        mission->background_index   = ODTEX_SPACE_BG_FROZEN;
        mission->name               = "Mission 1";

        mission->enemy_waves[0].num_enemies = 1;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[SMALL_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 1;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[MEDIUM_ENEMY_INDEX];

        mission->enemy_waves[2].num_enemies = 2;
        mission->enemy_waves[2].enemies[0] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[1] = &data->enemy_types[MEDIUM_ENEMY_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets,
            MUS_COMBINATED_SPACE1);
        mission->music_tracks[0].loop  = -1;

        mission->num_music_tracks = 1;
    }

    /* Mission 2 */
    {
        mission = &data->mission_types[MISSION_2_INDEX];

        mission->points             = 10;
        mission->num_waves          = 3;
        mission->background_index   = ODTEX_SPACE_BG_FROZEN;
        mission->name               = "Mission 2";

        mission->enemy_waves[0].num_enemies = 2;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[1] = &data->enemy_types[SMALL_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 2;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[MEDIUM_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[1] = &data->enemy_types[MEDIUM_ENEMY_INDEX];

        mission->enemy_waves[2].num_enemies = 1;
        mission->enemy_waves[2].enemies[0] = &data->enemy_types[LARGE_ENEMY_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets,
            MUS_COMBINATED_SPACE1);
        mission->music_tracks[0].loop  = -1;
    }

    /* Mission 3 */
    {
        mission = &data->mission_types[MISSION_3_INDEX];

        mission->points             = 10;
        mission->num_waves          = 2;
        mission->background_index   = ODTEX_SPACE_BG_FROZEN;
        mission->name               = "Mission 3";

        mission->enemy_waves[0].num_enemies = 3;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[1] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[2] = &data->enemy_types[SMALL_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 3;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[MEDIUM_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[1] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[2] = &data->enemy_types[MEDIUM_ENEMY_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets,
            MUS_COMBINATED_SPACE1);
        mission->music_tracks[0].loop  = -1;
    }
    /* Mission 4 */
    {
        mission = &data->mission_types[MISSION_4_INDEX];

        mission->points = 10;
        mission->num_waves = 3;
        mission->background_index = ODTEX_SPACE_BG_FROZEN;
        mission->name = "Mission 4";

        mission->enemy_waves[0].num_enemies = 2;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[MEDIUM_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[1] = &data->enemy_types[SMALL_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 3;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[1] = &data->enemy_types[MEDIUM_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[2] = &data->enemy_types[LARGE_ENEMY_INDEX];

        mission->enemy_waves[2].num_enemies = 4;
        mission->enemy_waves[2].enemies[0] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[1] = &data->enemy_types[MEDIUM_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[2] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[3] = &data->enemy_types[SMALL_ENEMY_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets,
            MUS_COMBINATED_SPACE1);
        mission->music_tracks[0].loop  = -1;
    }

    /* Boss 1 */
    {
        mission = &data->mission_types[BOSS_BATTLE_1_INDEX];

        mission->points = 10;
        mission->num_waves = 1;
        mission->background_index = ODTEX_SPACE_BG_FROZEN;
        mission->name = "Boss 1";

        mission->enemy_waves[0].num_enemies = 1;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[FIRST_BOSS_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets,
            MUS_LEVEL_1_BOSS_INTRO);
        mission->music_tracks[0].loop  = 0;

        mission->music_tracks[1].music = Assets_getMusic(&game->assets,
            MUS_LEVEL_1_BOSS);
        mission->music_tracks[1].loop  = -1;

        mission->num_music_tracks = 2;
    }

        /* Mission 2 Battle 1*/
    {
        mission = &data->mission_types_level2[MISSION_1_INDEX];

        mission->points = 10;
        mission->num_waves = 2;
        mission->background_index = ODTEX_SPACE_BG_GREEN;
        mission->name = "Mission 1";

        mission->enemy_waves[0].num_enemies = 3;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[1] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[2] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 3;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[1] = &data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[2] = &data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets, MUS_COMBINATED_SPACE2);
        mission->music_tracks[0].loop = -1;
        mission->num_music_tracks = 1;
    }

    /*Mission 2 BATTLE 2*/
    {
        mission = &data->mission_types_level2[MISSION_2_INDEX];

        mission->points = 10;
        mission->num_waves = 3;
        mission->background_index = ODTEX_SPACE_BG_GREEN;
        mission->name = "Mission 2";

        mission->enemy_waves[0].num_enemies = 2;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[1] = &data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 4;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[1] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[2] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[3] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];

        mission->enemy_waves[2].num_enemies = 2;
        mission->enemy_waves[2].enemies[0] = &data->enemy_types[LARGE_WHITE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[1] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets, MUS_COMBINATED_SPACE2);
        mission->music_tracks[0].loop = -1;
        mission->num_music_tracks = 1;
    }
    /*Mission 2 BATTLE 3*/
    {
        mission = &data->mission_types_level2[MISSION_3_INDEX];

        mission->points = 10;
        mission->num_waves = 4;
        mission->background_index = ODTEX_SPACE_BG_GREEN;
        mission->name = "Mission 3";

        mission->enemy_waves[0].num_enemies = 2;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[1] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 3;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[MEDIUM_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[1] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[2] = &data->enemy_types[MEDIUM_ENEMY_INDEX];

        mission->enemy_waves[2].num_enemies = 4;
        mission->enemy_waves[2].enemies[0] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[1] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[2] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[3] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];

        mission->enemy_waves[3].num_enemies = 2;
        mission->enemy_waves[3].enemies[0] = &data->enemy_types[LARGE_WHITE_ENEMY_INDEX];
        mission->enemy_waves[3].enemies[1] = &data->enemy_types[LARGE_WHITE_ENEMY_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets, MUS_COMBINATED_SPACE2);
        mission->music_tracks[0].loop = -1;
        mission->num_music_tracks = 1;
    }

    /*Mission 2 BATTLE 4*/
    {
        mission = &data->mission_types_level2[MISSION_4_INDEX];

        mission->points = 10;
        mission->num_waves = 2;
        mission->background_index = ODTEX_SPACE_BG_GREEN;
        mission->name = "Mission 4";

        mission->enemy_waves[0].num_enemies = 2;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[1] = &data->enemy_types[MEDIUM_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 4;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[1] = &data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[2] = &data->enemy_types[LARGE_WHITE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[3] = &data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets, MUS_COMBINATED_SPACE2);
        mission->music_tracks[0].loop = -1;
        mission->num_music_tracks = 1;
    }

    /*MISSION 2 Boss 1 */
    {
        mission = &data->mission_types_level2[BOSS_BATTLE_1_INDEX];

        mission->points = 10;
        mission->num_waves = 1;
        mission->background_index = ODTEX_SPACE_BG_GREEN;
        mission->name = "Boss 2";

        mission->enemy_waves[0].num_enemies = 1;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[SECOND_BOSS_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets,
            MUS_LEVEL_1_BOSS_INTRO);
        mission->music_tracks[0].loop = 0;

        mission->music_tracks[1].music = Assets_getMusic(&game->assets,
            MUS_LEVEL_1_BOSS);
        mission->music_tracks[1].loop = -1;

        mission->num_music_tracks = 2;
    }

    /* Mission 3 Battle 1*/
    {
        mission = &data->mission_types_level3[MISSION_1_INDEX];

        mission->points = 10;
        mission->num_waves = 4;
        mission->background_index = ODTEX_SPACE_BG_TORN;
        mission->name = "Mission 1";

        mission->enemy_waves[0].num_enemies = 4;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[MEDIUM_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[1] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[2] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[3] = &data->enemy_types[SMALL_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 3;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[1] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[2] = &data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX];

        mission->enemy_waves[2].num_enemies = 2;
        mission->enemy_waves[2].enemies[0] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[1] = &data->enemy_types[LARGE_WHITE_ENEMY_INDEX];

        mission->enemy_waves[3].num_enemies = 4;
        mission->enemy_waves[3].enemies[0] = &data->enemy_types[MEDIUM_ENEMY_INDEX];
        mission->enemy_waves[3].enemies[1] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[3].enemies[2] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[3].enemies[3] = &data->enemy_types[LARGE_ENEMY_INDEX];


        mission->music_tracks[0].music = Assets_getMusic(&game->assets, MUS_LEVEL_2);
        mission->music_tracks[0].loop = -1;
        mission->num_music_tracks = 1;
    }

    /*Mission 3 BATTLE 2*/
    {

        mission = &data->mission_types_level3[MISSION_2_INDEX];

        mission->points = 10;
        mission->num_waves = 3;
        mission->background_index = ODTEX_SPACE_BG_TORN;
        mission->name = "Mission 2";

        mission->enemy_waves[0].num_enemies = 4;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[MEDIUM_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[1] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[2] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[3] = &data->enemy_types[LARGE_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 2;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[LARGE_WHITE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[1] = &data->enemy_types[LARGE_WHITE_ENEMY_INDEX];

        mission->enemy_waves[2].num_enemies = 4;
        mission->enemy_waves[2].enemies[0] = &data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[1] = &data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[2] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[3] = &data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX];


        mission->music_tracks[0].music = Assets_getMusic(&game->assets, MUS_LEVEL_2);
        mission->music_tracks[0].loop = -1;
        mission->num_music_tracks = 1;
    }
    /*Mission 3 BATTLE 3*/
    {
        mission = &data->mission_types_level3[MISSION_3_INDEX];

        mission->points = 10;
        mission->num_waves = 3;
        mission->background_index = ODTEX_SPACE_BG_TORN;
        mission->name = "Mission 3";

        mission->enemy_waves[0].num_enemies = 2;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[SMALL_WHITE_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[1] = &data->enemy_types[SMALL_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 4;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[MEDIUM_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[1] = &data->enemy_types[LARGE_WHITE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[2] = &data->enemy_types[LARGE_WHITE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[3] = &data->enemy_types[LARGE_WHITE_ENEMY_INDEX];

        mission->enemy_waves[2].num_enemies = 4;
        mission->enemy_waves[2].enemies[0] = &data->enemy_types[MEDIUM_WHITE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[1] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[2] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[3] = &data->enemy_types[LARGE_ENEMY_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets, MUS_LEVEL_2);
        mission->music_tracks[0].loop = -1;
        mission->num_music_tracks = 1;
    }

    /*Mission 3 BATTLE 4*/
    {
        mission = &data->mission_types_level3[MISSION_4_INDEX];

        mission->points = 10;
        mission->num_waves = 4;
        mission->background_index = ODTEX_SPACE_BG_TORN;
        mission->name = "Mission 4";

        mission->enemy_waves[0].num_enemies = 3;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[MEDIUM_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[1] = &data->enemy_types[SMALL_ENEMY_INDEX];
        mission->enemy_waves[0].enemies[2] = &data->enemy_types[MEDIUM_ENEMY_INDEX];

        mission->enemy_waves[1].num_enemies = 2;
        mission->enemy_waves[1].enemies[0] = &data->enemy_types[LARGE_WHITE_ENEMY_INDEX];
        mission->enemy_waves[1].enemies[1] = &data->enemy_types[SMALL_ENEMY_INDEX];

        mission->enemy_waves[2].num_enemies = 4;
        mission->enemy_waves[2].enemies[0] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[1] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[2] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[2].enemies[3] = &data->enemy_types[LARGE_ENEMY_INDEX];

        mission->enemy_waves[3].num_enemies = 4;
        mission->enemy_waves[3].enemies[0] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[3].enemies[1] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[3].enemies[2] = &data->enemy_types[LARGE_ENEMY_INDEX];
        mission->enemy_waves[3].enemies[3] = &data->enemy_types[LARGE_ENEMY_INDEX];


        mission->music_tracks[0].music = Assets_getMusic(&game->assets, MUS_LEVEL_2);
        mission->music_tracks[0].loop = -1;
        mission->num_music_tracks = 1;
    }

    /*MISSION 3 Boss 1 */
    {
        mission = &data->mission_types_level3[BOSS_BATTLE_1_INDEX];

        mission->points = 10;
        mission->num_waves = 1;
        mission->background_index = ODTEX_SPACE_BG_TORN;
        mission->name = "Boss 3";

        mission->enemy_waves[0].num_enemies = 1;
        mission->enemy_waves[0].enemies[0] = &data->enemy_types[THIRD_BOSS_INDEX];

        mission->music_tracks[0].music = Assets_getMusic(&game->assets,
            MUS_LEVEL_1_BOSS_INTRO);
        mission->music_tracks[0].loop = 0;

        mission->music_tracks[1].music = Assets_getMusic(&game->assets,
            MUS_LEVEL_1_BOSS);
        mission->music_tracks[1].loop = -1;

        mission->num_music_tracks = 2;
    }

    /* Create levels */

    /* Level 1 */
    {
        LevelType *level = &data->level_types[0];

        for (int i = 0; i < 5; ++i)
            level->missions[i] = &data->mission_types[i];

        level->num_missions = 5;
    }

    /* Level 2 */
    {
        LevelType *level = &data->level_types[1];
        for (int i = 0; i < 5; ++i)
            level->missions[i] = &data->mission_types_level2[i];
        level->num_missions = 5;
    }

    /* Level 3 */
    {
        LevelType *level = &data->level_types[2];
        for (int i = 0; i < 5; ++i)
            level->missions[i] = &data->mission_types_level3[i];
        level->num_missions = 5;
    }
}

inline void
translateScreenCoordinatesByWindowViewport(uint *x, uint *y,
    int real_x, int real_y, Window *window)
{
    float x_multip = (float)window->viewport[2] / (float)window->width;
    float y_multip = (float)window->viewport[3] / (float)window->height;
    *x = (int)(x_multip * (float)(real_x - window->viewport[0]));
    *y = (int)(y_multip * (float)(real_y - window->viewport[1]));
}

#ifdef _DEBUG
void
initFpsTextTexture(Texture *texture, RenderThread *thread, TTF_Font *font)
{
    loadTextureFromTextOnAnyThread(thread, texture,
        "fps: 0", font, {255, 255, 255, 0}, 0);
}

void
updateFpsTextTexture(Texture* texture, RenderThread *thread, double fps, TTF_Font* font)
{
    char buffer[64];
    static const char *alt_text = "fps: 0";
    const char *text;

    if (fps == 0.0f)
        text = alt_text;
    else
    {
        snprintf(buffer, 64, "fps: %f", (float)fps);
        text = buffer;
    }

    loadTextureFromTextOnAnyThread(thread, texture, text, font, {255, 255, 255, 0}, 0);
}
#endif

void
setFps30 (Button *button, Game *game)
{
    game->clock.target_fps = 30;
    game->render_thread.clock.target_fps = 30;
}

void
setFps60 (Button *button, Game *game)
{
    game->clock.target_fps = 60;
    game->render_thread.clock.target_fps = 60;
}

Game GAME;

int main(int argc, char **argv)
{
    if (Game_init(&GAME) != 0)
        return -1;

    Game_mainLoop(&GAME);
    Game_uninit(&GAME);

    return 0;
}
